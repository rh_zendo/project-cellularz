# Setting current dir
set(projectName "UnitTest")
set(srcdir "${CellularZ_SOURCE_DIR}/src/unittest")
set(com_defs "")

message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] Creating executable build")

# Includeing sub folders
include_directories(.)
include_directories(client)
include_directories(server)
include_directories(shared)
include_directories(shared/ecs)
include_directories(shared/ecs/component)
include_directories(shared/util)
include_directories(shared/network)

# Getting file lists
file(GLOB_RECURSE src 	*.h		*.cpp		*.hpp)

# Creating executable
add_executable(${projectName} ${src})

# Adding links and definitions
target_link_libraries(${projectName} Shared)
target_link_libraries(${projectName} ${SDL2_LIBRARY} ${SDL2_NET_LIBRARIES} ${SDL2_MIXER_LIBRARIES} ${SDL2_IMAGE_LIBRARY})

# Check if client build
if(BUILD_CLIENT)
	message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] executable build as a client")
	set(com_defs "${com_defs};_BUILD_CLIENT")
	target_link_libraries(${projectName} Client)
endif()

# Check if server build
if(BUILD_SERVER)
	message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] executable build as a server")
	set(com_defs "${com_defs};_BUILD_SERVER")
	target_link_libraries(${projectName} Server)
endif()

# Checks if the unittest shuld hang for debug
if(UNITEST_DEBUG)
	message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] Hanges for debug")
	set(com_defs "${com_defs};_UNITTEST_DEBUG")
endif()

# Adding Compiler Definitions
message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] Adding compiler definitions: ${com_defs}")
set_target_properties(${projectName} PROPERTIES COMPILE_DEFINITIONS "${com_defs}")

# (Win Only) Making to that when building dll files are copied to the build dir
if(WIN32)
	message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] Copying dll files to build directory")

	# SDL2
	message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] ${SDL2_LIBRARY_DLL}")
	add_custom_command(TARGET ${projectName} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different "${SDL2_LIBRARY_DLL}" $<TARGET_FILE_DIR:${projectName}>)

	# SDL2_net
	message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] ${SDL2_NET_LIBRARY_DLL}")
	add_custom_command(TARGET ${projectName} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different "${SDL2_NET_LIBRARY_DLL}" $<TARGET_FILE_DIR:${projectName}>)

	# Client Libs
	if(BUILD_CLIENT)
		# SDL2_image
		message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] ${SDL2_IMAGE_LIBRARY_DLL}")
		add_custom_command(TARGET ${projectName} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different "${SDL2_IMAGE_LIBRARY_DLL}" $<TARGET_FILE_DIR:${projectName}>)

		message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] ${SDL2_IMAGE_LIBRARY_DLL_LIBPNG}")
		add_custom_command(TARGET ${projectName} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different "${SDL2_IMAGE_LIBRARY_DLL_LIBPNG}" $<TARGET_FILE_DIR:${projectName}>)

		message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] ${SDL2_IMAGE_LIBRARY_DLL_ZLIB}")
		add_custom_command(TARGET ${projectName} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different "${SDL2_IMAGE_LIBRARY_DLL_ZLIB}" $<TARGET_FILE_DIR:${projectName}>)

		# SDL2_mixer
		message(STATUS "[${CMAKE_PROJECT_NAME}][${projectName}] ${SDL2_MIXER_LIBRARY_DLL}")
		add_custom_command(TARGET ${projectName} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different "${SDL2_MIXER_LIBRARY_DLL}" $<TARGET_FILE_DIR:${projectName}>)
	endif()
endif()

# Making source filters
foreach(source IN LISTS src)
    get_filename_component(source_path "${source}" PATH)
    string(REPLACE "${srcdir}" "" source_path "${source_path}")
    string(REPLACE "/" "\\" source_path_msvc "${source_path}")
    source_group("${source_path_msvc}" FILES "${source}")
endforeach()