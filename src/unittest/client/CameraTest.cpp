#ifdef _BUILD_CLIENT
#include "catch.hpp"

#include "Camera.h"
using namespace cellularz;

TEST_CASE("[Camera]", "[Client]")
{
	// Test Data
	int testWidth = 1366;
	int testHeight = 768;
	Camera testCamera(testWidth, testHeight);

	SECTION("Get / Set Methods")
	{
		SECTION("Camera(int width, int height)")
		{
			Camera camera(testWidth, testHeight);

			REQUIRE(camera.getSize().x == testWidth);
			REQUIRE(camera.getSize().y == testHeight);
		}

		#pragma region Size
		SECTION("glm::vec2 getSize()")
		{
			REQUIRE(testCamera.getSize().x == testWidth);
			REQUIRE(testCamera.getSize().y == testHeight);
		}
		#pragma endregion

		#pragma region Position
		SECTION("glm::vec2 getPosition() / glm::vec2 getCenterPosition() / setPosition(float x, float y)")
		{
			float testX = 10.0f;
			float testY = 10.0f;

			testCamera.setPosition(testX, testY);

			REQUIRE(testCamera.getPosition().x == testX);
			REQUIRE(testCamera.getPosition().y == testY);

			REQUIRE(testCamera.getCenterPosition().x == testX + (testWidth / 2.0f));
			REQUIRE(testCamera.getCenterPosition().y == testY + (testHeight / 2.0f));
		}


		SECTION("glm::vec2 getPosition() / glm::vec2 getCenterPosition() / setPosition(glm::vec2 pos)")
		{
			glm::vec2 testPos(300.0f, 10.0f);

			testCamera.setPosition(testPos);

			REQUIRE(testCamera.getPosition().x == testPos.x);
			REQUIRE(testCamera.getPosition().y == testPos.y);

			REQUIRE(testCamera.getCenterPosition().x == testPos.x + (testWidth / 2.0f));
			REQUIRE(testCamera.getCenterPosition().y == testPos.y + (testHeight / 2.0f));
		}

		SECTION("glm::vec2 getPosition() / glm::vec2 getCenterPosition() / addPosition(float x, float y)")
		{
			float testX = 10.0f;
			float testY = 10.0f;
			float testAddX = 30.0f;
			float testAddY = 20.0f;

			testCamera.setPosition(testX, testY);
			testCamera.addPosition(testAddX, testAddY);

			REQUIRE(testCamera.getPosition().x == testX + testAddX);
			REQUIRE(testCamera.getPosition().y == testY + testAddY);

			REQUIRE(testCamera.getCenterPosition().x == testX + testAddX + (testWidth / 2.0f));
			REQUIRE(testCamera.getCenterPosition().y == testY + testAddY + (testHeight / 2.0f));
		}

		SECTION("glm::vec2 getPosition() / glm::vec2 getCenterPosition() / addPosition(glm::vec2 pos)")
		{
			glm::vec2 testPos(300.0f, 10.0f);
			glm::vec2 testAddPos(10.0f, 10.0f);

			testCamera.setPosition(testPos);
			testCamera.addPosition(testAddPos);

			REQUIRE(testCamera.getPosition().x == testPos.x + testAddPos.x);
			REQUIRE(testCamera.getPosition().y == testPos.y + testAddPos.y);

			REQUIRE(testCamera.getCenterPosition().x == testPos.x + testAddPos.x + (testWidth / 2.0f));
			REQUIRE(testCamera.getCenterPosition().y == testPos.y + testAddPos.y + (testHeight / 2.0f));
		}

		SECTION("glm::vec2 getPosition() / glm::vec2 getCenterPosition() / setCenterPosition(glm::vec2 centerPos)")
		{
			glm::vec2 testCenterPos(400.0f, 200.0f);

			testCamera.setCenterPosition(testCenterPos);

			REQUIRE(testCamera.getPosition().x == (testWidth / 2.0f) - testCenterPos.x);
			REQUIRE(testCamera.getPosition().y == (testHeight / 2.0f) - testCenterPos.y);

			REQUIRE(testCamera.getCenterPosition().x == testCenterPos.x);
			REQUIRE(testCamera.getCenterPosition().y == testCenterPos.y);
		}
		#pragma endregion

		#pragma region Scale
		SECTION("float getScale() / setScale(float scale)")
		{
			float testScale = 2.0f;

			testCamera.setScale(testScale);

			REQUIRE(testCamera.getScale() == testScale);
		}
		#pragma endregion

	}
}
#endif