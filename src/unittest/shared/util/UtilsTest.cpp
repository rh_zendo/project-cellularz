#include "catch.hpp"

#include "Utils.h"

using namespace cellularz;

TEST_CASE("[Utils]", "[Shared]")
{
	#pragma region Collision Detection
	SECTION("Collision Detection")
	{
		SECTION("collisionAABB() - Should collide")
		{
			REQUIRE(Utils::collisionAABB(glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), glm::vec2(glm::vec2(0.5f, 0.5f)), glm::vec2(glm::vec2(1.5f, 1.5f))));
		}

		SECTION("collisionAABB() - Shouldnt collide")
		{
			REQUIRE_FALSE(Utils::collisionAABB(glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 1.0f), glm::vec2(glm::vec2(1.0f, 1.0f)), glm::vec2(glm::vec2(2.0f, 2.0f))));
		}
	}
	#pragma endregion
}