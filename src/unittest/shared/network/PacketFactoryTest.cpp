#include "catch.hpp"

#include <bitset>

#include "Settings.h"

#include "PacketUtils.h"
#include "PacketFactory.h"

#include "ComponentID.h"

using namespace cellularz;

TEST_CASE("[PacketFactory]", "[Shared]")
{
	#pragma region Methods
	SECTION("Methods") {
		// Test Data
		PacketProtocalID protocalId = PacketProtocalID::UNKNOWN;
		int size = 4;
		 #pragma once
		SECTION("createPacket(PacketProtocalID protocalId)")
		{
			UDPpacket* packet = PacketFactory::createPacket(protocalId);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == protocalId);
			REQUIRE(PacketUtils::decodeHeaderAckId(packet) == ACKMANAGER_DISABLE_ACKID); // Should be disabled by default
			REQUIRE(packet->channel == -1);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS);
		}

		SECTION("createPacket(PacketProtocalID protocalId, int size)") {
			UDPpacket* packet = PacketFactory::createPacket(protocalId, size);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == protocalId);
			REQUIRE(PacketUtils::decodeHeaderAckId(packet) == ACKMANAGER_DISABLE_ACKID); // Should be disabled by default
			REQUIRE(packet->channel == -1);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS + size);
		}
	}
	#pragma endregion

	#pragma region Ack Packets
	SECTION("Ack Packets")
	{
		// Test Data
		Uint16 ackId = 14;

		SECTION("ack(Uint16 ackId)")
		{
			UDPpacket* packet = PacketFactory::ack(ackId);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::ACK);
			REQUIRE(PacketUtils::decodeAckAckId(packet) == ackId);
			REQUIRE(packet->channel == -1);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS + PACKET_ACK_SIZE);
		}

		SECTION("ack(Uint16 ackId, int channelId)")
		{
			int channelId = 0;
			UDPpacket* packet = PacketFactory::ack(ackId, channelId);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::ACK);
			REQUIRE(PacketUtils::decodeAckAckId(packet) == ackId);
			REQUIRE(packet->channel == channelId);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS + PACKET_ACK_SIZE);
		}
	}
	#pragma endregion

	#pragma region Connection Packets
	SECTION("Connection Packets")
	{
		SECTION("connect(std::string username)")
		{
			std::string username = "Some Name";
			UDPpacket* packet = PacketFactory::PacketFactory::connect(username);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::CONNECT);
			REQUIRE(PacketUtils::decodeHeaderAckId(packet) == ACKMANAGER_ENABLED_ACKID); // Should be enabled !!!
			REQUIRE(PacketUtils::decodeConnectUsername(packet) == username);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS + username.size());
		}

		SECTION("connect_failed(IPaddress ipAddress)")
		{
			IPaddress ipAddress;
			ipAddress.host = 0x7F000001; // Same as 127.0.0.1
			ipAddress.port = 25025;

			UDPpacket* packet = PacketFactory::PacketFactory::connect_failed(ipAddress);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::CONNECT_FAILED);
			REQUIRE(packet->channel == -1);	// Has to be -1 otherwise it wont use the ip address
			REQUIRE(packet->len == PACKET_SIZE_HEADERS);
			REQUIRE(packet->address.host == ipAddress.host);
			REQUIRE(packet->address.port == ipAddress.port);
		}

		SECTION("disconnect()")
		{
			UDPpacket* packet = PacketFactory::PacketFactory::disconnect();

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::DISCONNECT);
			REQUIRE(packet->channel == -1);	// Has to be -1
			REQUIRE(packet->len == PACKET_SIZE_HEADERS);
		}
		SECTION("disconnect(int channelId)")
		{
			int channelId = 24;
			UDPpacket* packet = PacketFactory::PacketFactory::disconnect(channelId);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::DISCONNECT);
			REQUIRE(packet->channel == channelId);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS);
		}

		SECTION("unresponsive()")
		{
			UDPpacket* packet = PacketFactory::PacketFactory::unresponsive();

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::UNRESPONSIVE);
			REQUIRE(packet->channel == -1);	// Has to be -1
			REQUIRE(packet->len == PACKET_SIZE_HEADERS);
		}
		SECTION("unresponsive(int channelId)")
		{
			int channelId = 2;
			UDPpacket* packet = PacketFactory::PacketFactory::unresponsive(channelId);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::UNRESPONSIVE);
			REQUIRE(packet->channel == channelId);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS);
		}

		SECTION("reconnected()")
		{
			UDPpacket* packet = PacketFactory::PacketFactory::reconnected();

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::RECONNECTED);
			REQUIRE(packet->channel == -1);	// Has to be -1
			REQUIRE(packet->len == PACKET_SIZE_HEADERS);
		}
		SECTION("reconnected(int channelId)")
		{
			int channelId = 3;
			UDPpacket* packet = PacketFactory::PacketFactory::reconnected(channelId);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::RECONNECTED);
			REQUIRE(packet->channel == channelId);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS);
		}
	}
	#pragma endregion

	#pragma region Game Object Packets
	SECTION("Game Object Packets")
	{
		// Test Data
		Uint32 objectId = 10;
		Uint32 objectOwnerId = 20;
		Uint16 objectClassId = 5;
		using ComponentBitMask = std::bitset<ECS_POOL_SIZE_COMPONENT_SYSTEM>;
		ComponentBitMask objectCompMask = { false };
		objectCompMask[ComponentID::CTRANSFORM] = true;
		objectCompMask[ComponentID::CPHYSICS] = true;
		objectCompMask[ComponentID::CCOLLISON] = true;

		SECTION("gameobject_update(Uint32 objectId, Uint32 objectOwnerId, Uint16 objectClassId, Uint32 objectCompMask)")
		{
			UDPpacket* packet = PacketFactory::gameobject_update(objectId, objectOwnerId, objectClassId, objectCompMask.to_ulong());

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::GAMEOBJECT_UPDATE);
			REQUIRE(packet->channel == CHANNELMANAGER_BROADCAST_CHANNEL);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS + PACKET_GAMEOBJECT_UPDATE_SIZE);
			REQUIRE(PacketUtils::decodeGameObjectPacketGetId(packet) == objectId);
			REQUIRE(PacketUtils::decodeGameObjectPacketGetOwnerId(packet) == objectOwnerId);
			REQUIRE(PacketUtils::decodeGameObjectPacketGetClassId(packet) == objectClassId);
			REQUIRE(PacketUtils::decodeGameObjectPacketGetCompMask(packet) == objectCompMask.to_ulong());
		}

		SECTION("gameobject_kill(Uint32 objectId)")
		{
			UDPpacket* packet = PacketFactory::gameobject_kill(objectId);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::GAMEOBJECT_KILL);
			REQUIRE(PacketUtils::decodeHeaderAckId(packet) == ACKMANAGER_ENABLED_ACKID);
			REQUIRE(packet->channel == CHANNELMANAGER_BROADCAST_CHANNEL);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS + PACKET_GAMEOBJECT_KILL_SIZE);
			REQUIRE(PacketUtils::decodeGameObjectPacketGetId(packet) == objectId);
		}
	}
	#pragma endregion

	#pragma region Client Packets
	SECTION("Client Packages")
	{
		Uint16 eventId = 0x00FF;
		int size = 4;

		SECTION("client_inputevent(Uint16 eventId)")
		{
			UDPpacket* packet = PacketFactory::client_inputevent(eventId);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::CLIENT_INPUTEVENT);
			REQUIRE(packet->channel == -1);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS + PACKET_CLIENT_INPUTEVENT_SIZE);
			REQUIRE(PacketUtils::decodeClientInputEventId(packet) == eventId);
		}
		SECTION("client_inputevent(Uint16 eventId, int size)")
		{
			UDPpacket* packet = PacketFactory::client_inputevent(eventId, size);

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::CLIENT_INPUTEVENT);
			REQUIRE(packet->channel == -1);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS + PACKET_CLIENT_INPUTEVENT_SIZE + size);
			REQUIRE(PacketUtils::decodeClientInputEventId(packet) == eventId);
		}

		SECTION("client_keepalive()")
		{
			UDPpacket* packet = PacketFactory::client_keepalive();

			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::CLIENT_KEEPALIVE);
			REQUIRE(packet->len == PACKET_SIZE_HEADERS);
		}
	}
	#pragma endregion
}