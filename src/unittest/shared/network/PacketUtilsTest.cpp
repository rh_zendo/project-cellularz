#include "catch.hpp"

#include "PacketUtils.h"
#include "PacketFactory.h"

using namespace cellularz;

TEST_CASE("[PacketUtils]", "[Shared]")
{
	#pragma region Packet Header Methods
	SECTION("Packet Header Methods")
	{
		// Test data
		Uint16 ackId = 0x0013;
		UDPpacket* packet = PacketFactory::createPacket(PacketProtocalID::UNKNOWN);

		SECTION("encodeHeaderAckId(UDPpacket* packet, Uint16 ackId)")
		{
			PacketUtils::encodeHeaderAckId(packet, ackId);
			REQUIRE(PacketUtils::decodeHeaderProtocalId(packet) == PacketProtocalID::UNKNOWN);
			REQUIRE(PacketUtils::decodeHeaderAckId(packet) == ackId);
		}

		SECTION("decodeHeaderAckId(UDPpacket* packet)")
		{
			PacketUtils::encodeHeaderAckId(packet, ackId);
			REQUIRE(PacketUtils::decodeHeaderAckId(packet) == ackId);
		}
	}
	#pragma endregion
}