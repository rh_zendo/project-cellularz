#include "catch.hpp"

#include "World.h"

#ifdef _BUILD_CLIENT
#include "ClientSDK.h"
#endif

#ifdef _BUILD_SERVER
#include "ServerSDK.h"
#endif

#include "SystemCollision.h"
#include "SystemPhysics.h"
#include "ComponentID.h"

#include <glm/glm.hpp>

using namespace cellularz;

TEST_CASE("[World]", "[Shared]")
{
#pragma region Methods
	SECTION("Methods")
	{
		// Test variables
		#ifdef _BUILD_SERVER
				ServerSDK* testSDK = new ServerSDK();
		#endif // _BUILD_SERVER
		#ifdef _BUILD_CLIENT
				ClientSDK* testSDK = new ClientSDK();
		#endif // _BUILD_CLIENT

		World* testWorld = new World(testSDK);

		float testWorldXSize = 500;
		float testWorldYSize = 400;

		glm::vec2 testWorldSize(testWorldXSize, testWorldYSize);

		SECTION("setSize(float& width, float& height)")
		{
			testWorld->setSize(testWorldXSize, testWorldYSize);
			glm::vec2 worldSize = testWorld->getSize();

			REQUIRE(worldSize.x == testWorldXSize);
			REQUIRE(worldSize.y == testWorldYSize);
		}
		SECTION("void setSize(glm::vec2& size)")
		{
			testWorld->setSize(testWorldSize);
			glm::vec2 worldSize = testWorld->getSize();

			REQUIRE(worldSize.x == testWorldXSize);
			REQUIRE(worldSize.y == testWorldYSize);
		}
		#ifdef _BUILD_SERVER
		SECTION("ServerSDK* getServer()")
		{
			ServerSDK* server = testWorld->getServer();

			REQUIRE(server == testSDK);
		}
		#endif // _BUILD_SERVER
		#ifdef _BUILD_CLIENT
		SECTION("ServerSDK* getServer()")
		{
			ClientSDK* client = testWorld->getClient();

			REQUIRE(client == testSDK);
		}
		#endif // _BUILD_SERVER
		
		SECTION("GameObject* createGameObject()")
		{
			World* testWorldLocal = new World(testSDK);

			GameObject* gameObject1 = testWorld->createGameObject();
			GameObject* gameObject2 = testWorld->createGameObject();

			REQUIRE(gameObject1->getId() == 0);
			REQUIRE(gameObject2->getId() == 1);

			delete gameObject1;
			delete gameObject2;

			delete testWorldLocal;
		}
		SECTION("GameObject* createGameObject(Uint32 objectId)")
		{
			World* testWorldLocal = new World(testSDK);

			Uint32 objectId(255);

			GameObject* gameObject = testWorld->createGameObject(objectId);

			REQUIRE(gameObject->getId() == objectId);

			delete gameObject;
			delete testWorldLocal;
		}
		SECTION("std::size_t getGameObjectCount()")
		{
			World* testWorldLocal = new World(testSDK);

			GameObject* gameObject1 = testWorld->createGameObject();
			GameObject* gameObject2 = testWorld->createGameObject();

			REQUIRE(testWorld->getGameObjectCount() == 2);

			delete gameObject1;
			delete gameObject2;

			delete testWorldLocal;
		}
		SECTION("std::map<Uint32, GameObject*>& getGameObjects()")
		{
			World* testWorldLocal = new World(testSDK);

			GameObject* gameObject = testWorld->createGameObject();
			std::map<Uint32, GameObject*> worldsGameObjects;

			worldsGameObjects = testWorldLocal->getGameObjects();

			for (auto iterator = worldsGameObjects.begin(); iterator != worldsGameObjects.end(); iterator++) {
				// iterator->first = key
				// iterator->second = value
				// Repeat if you also want to iterate through the second map.
				REQUIRE(iterator->second == gameObject);
			}

			delete gameObject;
			delete testWorldLocal;
		}
		/*SECTION("addSystem(TArgs&&... args)")
		{
			testWorld->addSystem<SystemCollision>();

			auto test1 = typeid(testWorld->getSystem<SystemCollision>(ComponentID::CCOLLISON)).name();
			auto test2 = typeid(SystemCollision).name();

			REQUIRE(typeid(testWorld->getSystem<SystemCollision>(ComponentID::CCOLLISON)).name() == typeid(SystemCollision).name());
		}*/
		SECTION("removeSystem(const Uint8& id);")
		{
			testWorld->removeSystem(ComponentID::CCOLLISON);
			REQUIRE(testWorld->getSystem(ComponentID::CCOLLISON) == nullptr);
		}

		delete testWorld;
		delete testSDK;
	}
#pragma endregion
}