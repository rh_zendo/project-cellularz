#include "catch.hpp"

#include "CSprite.h"
#include "ComponentID.h"

using namespace cellularz;

TEST_CASE("[CSprite]", "[Shared]")
{
	// Test Data
	CSprite* cSprite = new CSprite();

	SECTION("Get / Set Methods")
	{
		SECTION("ComponentID Component::getId()")
		{
			REQUIRE(cSprite->getId() == ComponentID::CSPRITE);
		}
	}

	// CleanUp
	delete cSprite;
}