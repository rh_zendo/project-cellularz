#include "catch.hpp"

#include "CCollision.h"
#include "ComponentID.h"

using namespace cellularz;

TEST_CASE("[CCollision]", "[Shared]")
{
	// Test Data
	CCollision* cCollision = new CCollision();

	SECTION("Get / Set Methods")
	{
		CollisionGroupID groupId = CollisionGroupID::MAP;

		SECTION("ComponentID getId()")
		{
			REQUIRE(cCollision->getId() == ComponentID::CCOLLISON);
		}

		#pragma region Collision Group
		SECTION("enableCollisionGroup(CollisionGroupID groupId)")
		{
			cCollision->enableCollisionGroup(groupId);
			REQUIRE(cCollision->hasCollisionGroup(groupId));
		}

		SECTION("disableCollisionGroup(CollisionGroupID groupId)")
		{
			cCollision->disableCollisionGroup(groupId);
			REQUIRE_FALSE(cCollision->hasCollisionGroup(groupId));
		}

		SECTION("disableCollisionGroup(CollisionGroupID groupId)")
		{
			cCollision->disableCollisionGroup(groupId);
			REQUIRE_FALSE(cCollision->hasCollisionGroup(groupId));
		}
		#pragma endregion

	}

	// CleanUp
	delete cCollision;
}