#include "catch.hpp"

#include "CPlayer.h"
#include "ComponentID.h"

#ifdef _BUILD_CLIENT
#include "Camera.h"
#endif

using namespace cellularz;

TEST_CASE("[CPlayer]", "[Shared]")
{
	// Test Data
	CPlayer* cPlayer = new CPlayer();

	SECTION("Get / Set Methods")
	{
		SECTION("ComponentID Component::getId()")
		{
			REQUIRE(cPlayer->getId() == ComponentID::CPLAYER);
		}

		#pragma region Camera
		SECTION("Camera")
		{
			SECTION("enableCameraLock()")
			{
				cPlayer->enableCameraLock();

				REQUIRE(cPlayer->isCameraLocked());
			}

			SECTION("disableCameraLock()")
			{
				cPlayer->disableCameraLock();

				REQUIRE_FALSE(cPlayer->isCameraLocked());
			}

			#ifdef _BUILD_CLIENT
			Camera* testCamera = new Camera(100, 100);

			SECTION("Camera* getCamera() / setCamera(Camera* camera)")
			{
				cPlayer->setCamera(testCamera);

				REQUIRE(cPlayer->getCamera() == testCamera);
			}

			SECTION("Camera* getCamera() / setCamera(nullptr)")
			{
				cPlayer->enableCameraLock();
				cPlayer->setCamera(nullptr);

				REQUIRE(cPlayer->getCamera() == nullptr);
				REQUIRE_FALSE(cPlayer->isCameraLocked());
			}

			// CleanUp
			delete testCamera;
			#endif
		}
		#pragma endregion
	}

	// CleanUp
	delete cPlayer;
}