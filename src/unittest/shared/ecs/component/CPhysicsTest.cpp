#include "catch.hpp"

#include "CPhysics.h"
#include "ComponentID.h"

using namespace cellularz;

TEST_CASE("[CPhysics]", "[Shared]")
{
	// Test Data
	CPhysics* cPhysics = new CPhysics();

	SECTION("Get / Set Methods")
	{
		float testWeight(30.0f);
		glm::vec2 testVelocity(30.0f, 25.0f);
		glm::vec2 testAddVelocity(10.0f, 10.0f);

		SECTION("ComponentID Component::getId()")
		{
			REQUIRE(cPhysics->getId() == ComponentID::CPHYSICS);
		}

		#pragma region Weight
		SECTION("getWeight() / setWeight(float weigth)")
		{
			cPhysics->setWeight(testWeight);

			REQUIRE(cPhysics->getWeight() == testWeight);
		}
		#pragma endregion

		#pragma region Velocity
		SECTION("getVelocity() / setVelocity(float x, float y)")
		{
			cPhysics->setVelocity(testVelocity.x, testVelocity.y);

			REQUIRE(cPhysics->getVelocity().x == testVelocity.x);
			REQUIRE(cPhysics->getVelocity().y == testVelocity.y);
		}

		SECTION("getVelocity() / setVelocity(glm::vec2 velocity)")
		{
			cPhysics->setVelocity(testVelocity);

			REQUIRE(cPhysics->getVelocity().x == testVelocity.x);
			REQUIRE(cPhysics->getVelocity().y == testVelocity.y);
		}

		SECTION("getVelocity() / addVelocity(float x, float y)")
		{
			cPhysics->setVelocity(testVelocity.x, testVelocity.y);
			cPhysics->addVelocity(testAddVelocity.x, testAddVelocity.y);

			REQUIRE(cPhysics->getVelocity().x == testVelocity.x + testAddVelocity.x);
			REQUIRE(cPhysics->getVelocity().y == testVelocity.y + testAddVelocity.y);
		}

		SECTION("getVelocity() / addVelocity(glm::vec2 velocity)")
		{
			cPhysics->setVelocity(testVelocity);
			cPhysics->addVelocity(testAddVelocity);

			REQUIRE(cPhysics->getVelocity().x == testVelocity.x + testAddVelocity.x);
			REQUIRE(cPhysics->getVelocity().y == testVelocity.y + testAddVelocity.y);
		}
		#pragma endregion

		#pragma region Gravity
		SECTION("bool hasGravity() / enableGravity()")
		{
			cPhysics->enableGravity();

			REQUIRE(cPhysics->hasGravity());
		}

		SECTION("bool hasGravity() / disableGravity()")
		{
			cPhysics->disableGravity();

			REQUIRE_FALSE(cPhysics->hasGravity());
		}
		#pragma endregion
	}

	// CleanUp
	delete cPhysics;
}