#include "catch.hpp"

#include "CTransform.h"
#include "ComponentID.h"

using namespace cellularz;

TEST_CASE("[CTransform]", "[Shared]")
{
	SECTION("Get / Set Methods")
	{
		// Test Data
		CTransform* cTransform = new CTransform();

		SECTION("ComponentID Component::getId()")
		{
			REQUIRE(cTransform->getId() == ComponentID::CTRANSFORM);
		}

		SECTION("glm::vec2 getPosition()")
		{
			glm::vec2 pos = cTransform->getPosition();
			REQUIRE(pos.x == 0.0f);
			REQUIRE(pos.y == 0.0f);
		}

		SECTION("glm::vec2 getCenterPosition()")
		{
			glm::vec2 testPos(100.0f, 100.0f);
			glm::vec2 testSize(50.0f, 40.0f);

			glm::vec2 testCenterPos(125.0f, 120.0f);

			cTransform->setPosition(testPos);
			cTransform->setSize(testSize);

			glm::vec2 centerPos = cTransform->getCenterPosition();
			REQUIRE(centerPos.x == testCenterPos.x);
			REQUIRE(centerPos.y == testCenterPos.y);
		}

		SECTION("setPosition(float x, float y)")
		{
			float x = 10.0f;
			float y = 20.0f;

			cTransform->setPosition(x, y);
			glm::vec2 pos = cTransform->getPosition();
			REQUIRE(pos.x == x);
			REQUIRE(pos.y == y);
		}

		SECTION("setPosition(glm::vec2 position)")
		{
			glm::vec2 testPos(10.0f, 30.0f);

			cTransform->setPosition(testPos);

			glm::vec2 pos = cTransform->getPosition();
			REQUIRE(pos.x == testPos.x);
			REQUIRE(pos.y == testPos.y);
		}

		SECTION("glm::vec2 getSize()")
		{
			glm::vec2 size = cTransform->getSize();
			REQUIRE(size.x == 0.0f);
			REQUIRE(size.y == 0.0f);
		}

		SECTION("setSize(float w, float h)")
		{
			float w = 25.0f;
			float h = 200.0f;

			cTransform->setSize(w, h);

			glm::vec2 size = cTransform->getSize();
			REQUIRE(size.x == w);
			REQUIRE(size.y == h);
		}

		SECTION("setSize(glm::vec2 size)")
		{
			glm::vec2 testSize(50.0f, 50.0f);

			cTransform->setSize(testSize);

			glm::vec2 size = cTransform->getSize();
			REQUIRE(size.x == testSize.x);
			REQUIRE(size.y == testSize.y);
		}

		SECTION("setSize(glm::vec2 size)")
		{
			glm::vec2 testSize(50.0f, 50.0f);

			cTransform->setSize(testSize);

			glm::vec2 size = cTransform->getSize();
			REQUIRE(size.x == testSize.x);
			REQUIRE(size.y == testSize.y);
		}

		SECTION("float getRotation()")
		{
			REQUIRE(cTransform->getRotation() == 0.0f);
		}

		SECTION("setRotation(float rotation)")
		{
			float testRotation = 60.0f;

			cTransform->setRotation(testRotation);

			float rotation = cTransform->getRotation();
			REQUIRE(rotation == testRotation);
		}

		// CleanUp
		delete cTransform;
	}
}