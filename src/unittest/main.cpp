//#define CATCH_CONFIG_MAIN
//#include "catch.hpp"

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include <iostream>

int main(int argc, char* const argv[])
{
	// global setup...
	int result = Catch::Session().run(argc, argv);

	// Hang if debug mode
	#ifdef _UNITTEST_DEBUG
	std::cout << "Press anykey to exit: ";
	char tmp;
	std::cin >> tmp;
	#endif

	// global clean-up...
	return result;
}
