#pragma once

#include <SDL.h>
#include <SDL_net.h>

#include <string>

#include "TickLimiter.h"

#include "World.h"
#include "Console.h"
#include "ServerNetwork.h"
#include "ServerPacketHandler.h"

#include "AxisID.h"

namespace cellularz
{
	// Forward declaration
	class Console;
	class ServerNetwork;
	class ServerPacketHandler;
	class World;
	class GameObject;

	class ServerSDK
	{
	public:
		ServerSDK();
		~ServerSDK();

		// Get / Set Methods
		bool isRunning();

		ServerNetwork* getServerNetwork();
		World* getWorld();

		// Initialiation Methods
		void initialize(const Uint16& port);
		void startServer();
		void stopServer();

		// Methods
		void fatalError(const std::string& msg);

		// Initialiation Hooks
		virtual void onInitializeWorld(World* world) {}

		// Hooks
		virtual void onTick(const double& deltaTime) {}
		virtual void onGameObjectCollision(GameObject* objectA, GameObject* objectB) {}
		virtual void onGameObjectCollisionResponse(GameObject* object, AxisID axis) {}
		virtual void onGameObjectOutOfWorld(GameObject* object) {}


		// Connnection Hooks
		virtual void onClientConnected(const Uint8& channelId, const IPaddress& clientIp) {}
		virtual void onClientDisconnect(const Uint8& channelId) {}
		virtual void onClientUnresponsive(const Uint8& channelId) {}
		virtual void onClientReconnected(const Uint8& channelId) {}

		// Client Hooks
		virtual void onClientInputEvent(const Uint8& channelId, const Uint16& eventId, Uint8* dataPointer) {}

	private:
		// Members
		bool m_isRunning;
		TickLimiter m_tickLimiter;

		ServerNetwork* m_serverNetwork;
		ServerPacketHandler* m_serverPacketHandler;

		World* m_world;

		// Methods
		void loopInitialize();
		void loop();

		// Thread
		SDL_Thread *m_thread;
		static int runner(void* ptr);
	};
}