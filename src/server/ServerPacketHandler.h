#pragma once

#include "ServerSDK.h"
#include "ServerNetwork.h"

namespace cellularz
{
	// Forward declaration
	class ServerSDK;
	class ServerNetwork;

	class ServerPacketHandler
	{
	public:
		ServerPacketHandler(ServerSDK* server, ServerNetwork* serverNetwork);
		~ServerPacketHandler();

		// Methods
		/// Handels inputBuffer from the packet queue
		void handelInputPackets();

		/// Adds all need packets to the outputBuffer in the packet queue, packets like gameobject updates.
		void handelOutputPackets();

	private:
		// Members
		ServerSDK* m_server;
		ServerNetwork* m_serverNetwork;

	};
}