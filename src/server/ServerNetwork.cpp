#include "ServerNetwork.h"

#include "PacketUtils.h"
#include "PacketFactory.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	ServerNetwork::ServerNetwork(ServerSDK* server) :
		m_server(server),
		m_tickLimiter(SERVERNETWORK_TICKRATE),
		m_channelManager(nullptr),
		m_ackManager(nullptr)
	{}

	ServerNetwork::~ServerNetwork()
	{
		m_channelManager->disconnectAllChannels();

		// Frees channel pool
		delete m_channelManager;
		delete m_ackManager;

		// Closing socket
		SDLNet_UDP_Close(m_socket);

		// Frees packet memory
		SDLNet_FreePacket(m_packetIn);
	}
	#pragma endregion

	#pragma region Get / Set Methods
	UDPsocket* ServerNetwork::getUDPsocket() { return &m_socket; }
	AckManager* ServerNetwork::getAckManager() { return m_ackManager; }
	ChannelManager* ServerNetwork::getChannelManager() { return m_channelManager; }
	#pragma endregion

	#pragma region Initialization Methods
	void ServerNetwork::initialize(const Uint16& port)
	{
		Console::print("[Network] initializing");

		// Creating UDP Socket
		m_socket = SDLNet_UDP_Open(port);
		if (!m_socket) {
			m_server->fatalError("[Network] Failed to create udp socket\n\r" + std::string(SDLNet_GetError()));
		}

		// Allocating Packet buffers
		if (!(m_packetIn = SDLNet_AllocPacket(PACKET_SIZE))) {
			m_server->fatalError("[Network] Failed to allocating input buffer\n\r" + std::string(SDLNet_GetError()));
		}

		// Initializing Ack Manager
		m_ackManager = new AckManager(this);

		// Initializing Channel Manager
		m_channelManager = new ChannelManager(this);

		Console::print("[Network] initialization complete");
	}

	void ServerNetwork::startNetworking()
	{
		// Creating thread
		m_thread = SDL_CreateThread(runner, "ServerNetwork", this);
	}
	#pragma endregion

	#pragma region Methods
	std::vector<UDPpacket*> ServerNetwork::getInputPacketQueue() { return m_packetQueue.getInputBuffer(); }
	std::vector<UDPpacket*> ServerNetwork::getOutputPacketQueue() { return m_packetQueue.getOutputBuffer(); }

	void ServerNetwork::addInputPacketToQueue(UDPpacket* packet) { m_packetQueue.addInputPacket(packet); }
	void ServerNetwork::addOutputPacketToQueue(UDPpacket* packet) {	m_packetQueue.addOutputPacket(packet); }
	#pragma endregion

	#pragma region Private Methods
	void ServerNetwork::loop()
	{
		while (m_server->isRunning()) {
			// Begin TickLimiter
			m_tickLimiter.begin();

			// Process received packets
			while (SDLNet_UDP_Recv(m_socket, m_packetIn)) {
				receivedPacket(m_packetIn);
			}

			// Process send packets
			std::vector<UDPpacket*> buffer = m_packetQueue.getOutputBuffer();
			for (std::size_t i = 0; i < buffer.size(); ++i) {
				sendPacket(buffer[i]);
			}

			// Updates Managers
			m_channelManager->update();
			m_ackManager->update();

			// End TickLimiter
			m_tickLimiter.end();
		}
	}

	void ServerNetwork::receivedPacket(UDPpacket* packet)
	{
		// Checks if header are valid
		if (packet->len < PACKET_SIZE_HEADERS) {
			Console::print("[Network] Packet is invalid");
			return;
		}

		// Checks if the packet should be handeld in the serverNetwork thread
		switch (PacketUtils::decodeHeaderProtocalId(packet)) {
			// Connection Packets
			case PacketProtocalID::CONNECT:
				// Checks if received packet already has a channel
				if (packet->channel != -1) { return; }

				// Tries to connect to the a channel
				packet->channel = m_channelManager->connectWithIPaddress(&packet->address);

				// Send a packet back to the client
				if (packet->channel == -1) {
					m_packetQueue.addOutputPacket(PacketFactory::connect_failed(packet->address));
					return;
				}
			break;

			case PacketProtocalID::DISCONNECT:
				// Checks if received packet has a channel
				if (packet->channel != -1) { m_channelManager->disconnectChannel(packet->channel); }
				return;
			break;

			// If its a ack packet send it to the ack manager, and ends
			case PacketProtocalID::ACK:
				m_ackManager->acknowledgePacketByAckId(PacketUtils::decodeAckAckId(packet), packet->channel);
				return;
			break;
		}

		// Checks if packet should be acknowledge
		Uint16 ackId = PacketUtils::decodeHeaderAckId(packet);
		if (ackId != ACKMANAGER_DISABLE_ACKID) {
			m_packetQueue.addOutputPacket(PacketFactory::ack(ackId, packet->channel));
		}

		// Checks if channel is valid
		if (packet->channel == -1) { return; }

		// Updates last response for the channel
		m_channelManager->updateChannelLastResponse(packet->channel);

		// Clones and adds the packet to the Queue
		m_packetQueue.addInputPacket(PacketUtils::clonePacket(packet));
	}

	void ServerNetwork::sendPacket(UDPpacket* packet)
	{
		// Checks if packet should be managed by ack manager
		bool ackHandeld = false;
		if(PacketUtils::decodeHeaderAckId(packet) == ACKMANAGER_ENABLED_ACKID) {
			ackHandeld = m_ackManager->encodeAckId(packet);
		}

		// Checks if it's a broadcast packet
		if (packet->channel != CHANNELMANAGER_BROADCAST_CHANNEL) {
			SDLNet_UDP_Send(m_socket, packet->channel, packet);
		} else {
			m_channelManager->broadcastUDPPacket(packet);
		}

		// Checks if the packet is handeld by the ack manager, otherwise frees the heap memory
		if(!ackHandeld) {
			SDLNet_FreePacket(packet);
		}
	}
	#pragma endregion

	#pragma region Thread
	int ServerNetwork::runner(void* ptr)
	{
		ServerNetwork *serverNetworkThread = (ServerNetwork*)ptr;

		// Starts network loop
		serverNetworkThread->loop();

		return 0;
	}
	#pragma endregion
}