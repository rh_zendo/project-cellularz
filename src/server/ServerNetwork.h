#pragma once

#include <SDL_net.h>

#include "ServerSDK.h"
#include "TickLimiter.h"

#include "AckManager.h"
#include "ChannelManager.h"

#include "PacketQueue.h"


namespace cellularz
{
	// Forward declaration
	class ServerSDK;
	class AckManager;
	class ChannelManager;

	class ServerNetwork
	{
	public:
		ServerNetwork(ServerSDK* server);
		~ServerNetwork();

		// Get / Set Methods
		/// Returns a pointer to the UDPsocket
		UDPsocket* getUDPsocket();
		/// Returns a pointer to the AckManager
		AckManager* getAckManager();
		/// Returns a pointer to the ChannelManager 
		ChannelManager* getChannelManager();

		// Initialization Methods
		/// Will initialize the server networking on the given port
		void initialize(const Uint16& port);
		/// This will start the network thread 
		void startNetworking();

		// Methods
		std::vector<UDPpacket*> getInputPacketQueue();
		std::vector<UDPpacket*> getOutputPacketQueue();

		void addInputPacketToQueue(UDPpacket* packet);
		void addOutputPacketToQueue(UDPpacket* packet);

	private:
		// Members
		ServerSDK* m_server;
		TickLimiter m_tickLimiter;

		// Sockets
		UDPsocket m_socket;
		AckManager* m_ackManager;
		ChannelManager* m_channelManager;

		// Packet Buffers
		UDPpacket* m_packetIn;
		PacketQueue m_packetQueue;

		// Methods
		void loop();
		void receivedPacket(UDPpacket* packet);
		void sendPacket(UDPpacket* packet);

		// Thread
		SDL_Thread *m_thread;
		static int runner(void* ptr);
	};
}