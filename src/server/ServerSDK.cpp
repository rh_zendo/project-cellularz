#include "ServerSDK.h"

//#include "Utils.h"

#include "Settings.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	ServerSDK::ServerSDK() :
		m_isRunning(false),
		m_tickLimiter(SERVER_TICKRATE),
		m_serverNetwork(nullptr),
		m_serverPacketHandler(nullptr),
		m_world(nullptr)
	{}

	ServerSDK::~ServerSDK()
	{
		SDLNet_Quit();
		SDL_Quit();
	}
	#pragma endregion

	#pragma region Get / Set Methods
	bool ServerSDK::isRunning() { return m_isRunning; }

	ServerNetwork* ServerSDK::getServerNetwork() { return m_serverNetwork; }

	World* ServerSDK::getWorld() { return m_world; }
	#pragma endregion

	#pragma region Initialiation Methods
	void ServerSDK::initialize(const Uint16& port)
	{
		Console::print("[Server] initializing");

		// Inits SDL
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
			fatalError("Failed to initialize SDL\n\r" + std::string(SDL_GetError()));
		}

		// Inits SDL_net
		if (SDLNet_Init() == -1) {
			fatalError("Failed to initialize SDL_net\n\r" + std::string(SDLNet_GetError()));
		}

		// Initializing Networking
		m_serverNetwork = new ServerNetwork(this);
		m_serverNetwork->initialize(port);

		// Initializing Packet Handeler
		m_serverPacketHandler = new ServerPacketHandler(this, m_serverNetwork);

		// Initializing World
		m_world = new World(this);

		// Trigger Hook
		onInitializeWorld(m_world);

		Console::print("[Server] initialization complete");
	}

	void ServerSDK::startServer()
	{
		// Sets the server to running 
		m_isRunning = true;

		// Making thread
		m_thread = SDL_CreateThread(runner, "Server", this);
	}

	void ServerSDK::stopServer()
	{
		Console::print("[Server] Shutting down");
		m_isRunning = false;

		exit(0);
	}
	#pragma endregion

	#pragma region Methods
	void ServerSDK::fatalError(const std::string& msg) {
		Console::print("[FatalError]" + msg);

		// Wait for key press
		std::cout << "Press anykey to exit: ";
		char tmp;
		std::cin >> tmp;

		exit(-1);
	}
	#pragma endregion

	#pragma region Private Methods
	void ServerSDK::loopInitialize()
	{
		// Start networking
		m_serverNetwork->startNetworking();
	}

	void ServerSDK::loop()
	{
		// Delta Time vars
		unsigned long long lastFrame = Utils::getCurrentMicro();
		double deltaTime = 0.0f;

		while (m_isRunning) {
			// Begin TickLimiter
			m_tickLimiter.begin();

			// Calcs delta time
			deltaTime = (Utils::getCurrentMicro() - lastFrame) / 1000000.0f;
			lastFrame = Utils::getCurrentMicro();

			// Handel input packets
			m_serverPacketHandler->handelInputPackets();

			// Logic
			onTick(deltaTime);
			m_world->update(deltaTime);

			// Handel output packets
			m_serverPacketHandler->handelOutputPackets();

			// End TickLimiter
			m_tickLimiter.end();
		}
	}
	#pragma endregion

	#pragma region Thread
	int ServerSDK::runner(void* ptr)
	{
		ServerSDK* server = (ServerSDK*)ptr;

		// Starting Server
		server->loopInitialize();
		server->loop();

		Console::print("[Server] thread has ended for an unknown reason");

		return 0;
	}
	#pragma endregion
}