#include "ServerPacketHandler.h"

#include "PacketUtils.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	ServerPacketHandler::ServerPacketHandler(ServerSDK* server, ServerNetwork* serverNetwork) :
		m_server(server),
		m_serverNetwork(serverNetwork)
	{}

	ServerPacketHandler::~ServerPacketHandler()
	{}
	#pragma endregion

	#pragma region Methods
	void ServerPacketHandler::handelInputPackets()
	{
		UDPpacket* packet;

		// Gets input buffer
		std::vector<UDPpacket*> buffer = m_serverNetwork->getInputPacketQueue();

		// Loop through the buffer
		for (std::size_t i = 0; i < buffer.size(); ++i) {
			packet = buffer[i];

			switch (PacketUtils::decodeHeaderProtocalId(packet)) {
				// Connection
				case PacketProtocalID::CONNECT: m_server->onClientConnected(packet->channel, packet->address); break;
				case PacketProtocalID::DISCONNECT: m_server->onClientDisconnect(packet->channel); break;
				case PacketProtocalID::UNRESPONSIVE: m_server->onClientUnresponsive(packet->channel); break;
				case PacketProtocalID::RECONNECTED: m_server->onClientReconnected(packet->channel); break;

				// Client
				case PacketProtocalID::CLIENT_INPUTEVENT: 
					m_server->onClientInputEvent(
					packet->channel,
					PacketUtils::decodeClientInputEventId(packet),
					packet->data + PACKET_CLIENT_INPUTEVENT_POINTER_DATA); 
				break;
			}

			// Removes packet from heap
			SDLNet_FreePacket(packet);
		}
	}

	void ServerPacketHandler::handelOutputPackets()
	{
		// Transmits World State
		m_server->getWorld()->transmitWorldState(m_serverNetwork);
	}
	#pragma endregion
}