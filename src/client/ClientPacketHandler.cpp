#include "ClientPacketHandler.h"

#include "PacketUtils.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	ClientPacketHandler::ClientPacketHandler(ClientSDK* client, ClientNetwork* clientNetwork) :
		m_client(client),
		m_clientNetwork(clientNetwork)
	{}

	ClientPacketHandler::~ClientPacketHandler()
	{}
	#pragma endregion

	#pragma region Methods
	void ClientPacketHandler::handelInputPackets()
	{
		UDPpacket* packet;

		// Gets input buffer
		std::vector<UDPpacket*> buffer = m_clientNetwork->getInputPacketQueue();
		for (std::size_t i = 0; i < buffer.size(); ++i) {
			packet = buffer[i];

			switch (PacketUtils::decodeHeaderProtocalId(packet)) {
				// Connection Packets
				case PacketProtocalID::CONNECT: m_client->onServerConnect(); break;
				case PacketProtocalID::CONNECT_FAILED: m_client->onServerConnectFailed(); break;

				// Game Objects Packets
				case PacketProtocalID::GAMEOBJECT_UPDATE: m_client->getWorld()->handelUpdatePacket(packet); break;
				case PacketProtocalID::GAMEOBJECT_KILL: m_client->getWorld()->killGameObject(PacketUtils::decodeGameObjectPacketGetId(packet)); break;

				// Client
				case PacketProtocalID::CLIENT_SERVERCHANNEL: m_client->setServerChannelId(PacketUtils::decodeClientServerChannelId(packet)); break;
			}

			// Removes packet from heap
			SDLNet_FreePacket(packet);
		}
	}

	void ClientPacketHandler::handelOutputPackets()
	{
	}
	#pragma endregion
}