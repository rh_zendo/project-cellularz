#include "ClientSDK.h"

#include <SDL_image.h>
#include <SDL_net.h>
#include <iostream>

#include "Utils.h"

// Components
#include "CCollision.h"
#include "CPhysics.h"
#include "CSprite.h"
#include "CTransform.h"

// Systems
#include "SystemCollision.h"
#include "SystemPhysics.h"
#include "SystemSprite.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	ClientSDK::ClientSDK() :
		m_isRunning(false),
		m_serverChannelId(-1),
		m_tickLimiter(CLIENT_TICKRATE),
		m_window(nullptr),
		m_camera(WINDOW_WIDTH, WINDOW_HEIGHT),
		m_audioManager(nullptr),
		m_inputManager(nullptr),
		m_textureManager(nullptr),
		m_clientNetwork(nullptr),
		m_clientPacketHandler(nullptr),
		m_world(nullptr),
		m_config(nullptr)
	{}

	ClientSDK::~ClientSDK()
	{
		delete m_window;

		// Cleaning up SDL
		SDLNet_Quit();
		IMG_Quit();
		SDL_Quit();
	}
	#pragma endregion

	#pragma region Get / Set
	bool ClientSDK::isRunning() { return m_isRunning; }

	const int& ClientSDK::getServerChannelId() { return m_serverChannelId; }
	void ClientSDK::setServerChannelId(const int& channelId)
	{
		m_serverChannelId = channelId;
		onServerConnectChannelId(channelId);
	}

	Window* ClientSDK::getWindow() { return m_window; }
	Camera* ClientSDK::getCamara() { return &m_camera; }
	RenderEngine* ClientSDK::getRenderEngine() { return m_renderEngine; }

	AudioManager* ClientSDK::getAudioManager() { return m_audioManager; }
	InputManager* ClientSDK::getInputManager() { return m_inputManager; }
	TextureManager* ClientSDK::getTextureManager() { return m_textureManager; }
	KeyBindingManager* ClientSDK::getKeyBindingmanager() { return m_keyBindingManager; }

	Config* ClientSDK::getConfig() { return m_config; }
	ClientNetwork* ClientSDK::getNetwork() { return m_clientNetwork; }

	World* ClientSDK::getWorld() { return m_world; }
	#pragma endregion

	#pragma region Initialiation Methods
	void ClientSDK::initialize()
	{
		Console::print("[Client] initializing");

		// Inits SDL
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
			fatalError("Failed to initialize SDL\n\r" + std::string(SDL_GetError()));
		}

		// Inits SDL_image
		int imgFlags = IMG_INIT_PNG;
		if (!(IMG_Init(imgFlags) & imgFlags)) {
			fatalError("Failed to initialize SDL_Image\n\r" + std::string(IMG_GetError()));
		}

		// Inits SDL_mixer
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
			fatalError("Failed to initialize SDL_mixer\n\r" + std::string(Mix_GetError()));
		}

		// Inits SDL_net
		if (SDLNet_Init() == -1) {
			fatalError("Failed to initialize SDL_net\n\r" + std::string(SDLNet_GetError()));
		}

		// Initializing Networking
		m_clientNetwork = new ClientNetwork(this);
		m_clientNetwork->initialize();

		// Initializing Packet Handeler
		m_clientPacketHandler = new ClientPacketHandler(this, m_clientNetwork);

		Console::print("[Client] initialized");
	}

	void ClientSDK::startClient()
	{
		// Sets the client to running 
		m_isRunning = true;
		
		// Making thread
		m_thread = SDL_CreateThread(runner, "Client", this);
	}

	void ClientSDK::stopClient()
	{
		// Sets the client to stopped
		m_isRunning = false;
		exit(0);
	}
	#pragma endregion

	#pragma region Methods
	void ClientSDK::fatalError(const std::string& msg)
	{
		Console::print("[FatalError]" + msg);

		// Wait for key press
		std::cout << "Press anykey to exit: ";
		char tmp;
		std::cin >> tmp;

		exit(-1);
	}
	#pragma endregion

	#pragma region Private Methods
	void ClientSDK::loopInitialize()
	{
		// Initializing Window
		m_window = new Window(this);
		m_window->initialize("CellularZ Client", 1366, 768, 0);

		// Initializing RenderEngine
		m_renderEngine = new RenderEngine(this, &m_camera);
		m_renderEngine->initialize(m_window->getSDLWindow());
		m_renderEngine->setBackgroundColor(255, 255, 255, 255);

		// Creating Managers
		m_audioManager = new AudioManager(this);
		m_inputManager = new InputManager(this);
		m_textureManager = new TextureManager(this);

		// Starting network
		m_clientNetwork->startNetworking();
		m_clientNetwork->connect("localhost", 25025);

		// Initializing World
		m_world = new World(this);

		// Trigger Hooks
		onInitializeResources(m_audioManager, m_textureManager);
		onInitializeWorld(m_world, m_config);
	}

	void ClientSDK::loop()
	{
		// Delta Time vars
		unsigned long long lastFrame = Utils::getCurrentMicro();
		double deltaTime = 0.0f;

		long long frameStartTime, frameTime;

		while (m_isRunning) {
			frameStartTime = Utils::getCurrentMicro();

			// Begin TickLimiter
			m_tickLimiter.begin();

			// Calcs delta time
			deltaTime = (Utils::getCurrentMicro() - lastFrame) / 1000000.0f;
			lastFrame = Utils::getCurrentMicro();

			// Input
			m_inputManager->update();

			// Handel output packets
			m_clientPacketHandler->handelOutputPackets();

			// Logic
			onTick(deltaTime); // Calls Tick Hook
			m_world->update(deltaTime); // Runs world logic

			// Rendering
			m_renderEngine->renderUpdate();

			frameTime = Utils::getCurrentMicro() - frameStartTime;

			// Handel input packets
			m_clientPacketHandler->handelInputPackets();

			// End TickLimiter
			m_tickLimiter.end();
			m_window->setTitle("FPS: " + std::to_string(m_tickLimiter.getCurrentTPS()) + " FrameTime: " + std::to_string(frameTime) + " Objects: " + std::to_string(m_world->getGameObjectCount()));

		}
	}
	#pragma endregion

	#pragma region Thread
	int ClientSDK::runner(void* ptr)
	{
		ClientSDK* client = (ClientSDK*)ptr;

		// Starting loop
		client->loopInitialize();
		client->loop();

		Console::print("[Client] thread has ended for an unknown reason");

		return 0;
	}
	#pragma endregion
}