#pragma once

#include <SDL.h>

namespace cellularz
{
	struct Color
	{
		/// Red channel
		Uint8 r = 0;
		/// Green channel
		Uint8 g = 0;
		/// Blue channel
		Uint8 b = 0;
		/// Alpha channel
		Uint8 a = 255;
	};
}