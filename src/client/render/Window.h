#pragma once

#include <SDL.h>

#include <string>

#include "ClientSDK.h"

namespace cellularz
{
	// Forward declaration
	class ClientSDK;

	enum WindowFlags {
		WINDOWFLAG_HIDDEN = 0x1,
		WINDOWFLAG_FULLSCREEN = 0x2,
		WINDOWFLAG_BORDERLESS = 0x4
	};

	class Window
	{
	public:
		Window(ClientSDK* client);
		~Window();

		// Get / Set Methods
		const int& getWidth();
		const int& getHeight();

		void setTitle(const std::string& title);

		SDL_Window* getSDLWindow();

		// Methods
		void initialize(const std::string& title, const int& width, const int& height, const Uint32& currentFlags);

	private:
		// Members
		int m_width;
		int m_height;

		ClientSDK* m_client;
		SDL_Window* m_window;

	};
}