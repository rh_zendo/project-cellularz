#pragma once

#include <glm/glm.hpp>

namespace cellularz
{
	class Camera
	{
	public:
		Camera(const int& width, const int& height);
		~Camera();

		// Get / Set
		const glm::vec2& getSize();

		const glm::vec2& getPosition();
		void setPosition(const float& x, const float& y);
		void setPosition(const glm::vec2& position);
		void addPosition(const float& x, const float& y);
		void addPosition(const glm::vec2& position);
		const glm::vec2& getCenterPosition();
		void setCenterPosition(const glm::vec2& centerPosition);

		const float& getScale();
		void setScale(const float& scale);

		// Methods
		const glm::vec2 convertScreenToWorld(const glm::vec2& mousePosition);
		bool inView(const glm::vec2& pos, const glm::vec2& size);

	private:
		// Members
		glm::vec2 m_size;
		glm::vec2 m_sizeOffset;

		glm::vec2 m_position;
		glm::vec2 m_positionCenter;
		
		float m_scale;

		// Private Methods
		void updateOffset();
		void updateCenter();
	};
}