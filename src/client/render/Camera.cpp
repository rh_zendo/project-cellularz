#include "Camera.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	Camera::Camera(const int& width, const int& height) :
		m_size(width, height),
		m_sizeOffset(0.0f, 0.0f),
		m_position(0.0f, 0.0f),
		m_positionCenter(0.0f, 0.0f),
		m_scale(1.0f)
	{
		updateOffset();
		updateCenter();
	}

	Camera::~Camera()
	{}
	#pragma endregion


	#pragma region Get / Set Methods
	const glm::vec2& Camera::getSize() { return m_size; }

	const glm::vec2& Camera::getPosition() { return m_position; }
	void Camera::setPosition(const float& x, const float& y)
	{
		m_position.x = x;
		m_position.y = y;
		updateCenter();
	}
	void Camera::setPosition(const glm::vec2& position)
	{
		m_position = position;
		updateCenter();
	}

	void Camera::addPosition(const float& x, const float& y)
	{
		m_position.x += x;
		m_position.y += y;
		updateCenter();
	}
	void Camera::addPosition(const glm::vec2& position)
	{
		m_position += position;
		updateCenter();
	}

	const glm::vec2& Camera::getCenterPosition() { return m_positionCenter; }
	void Camera::setCenterPosition(const glm::vec2& centerPosition)
	{
		m_positionCenter = centerPosition;
		m_position.x = m_sizeOffset.x - centerPosition.x;
		m_position.y = m_sizeOffset.y - centerPosition.y;
	}

	const float& Camera::getScale() { return m_scale; }
	void Camera::setScale(const float& scale) { m_scale = scale; }
	#pragma endregion

	#pragma region Methods
	const glm::vec2 Camera::convertScreenToWorld(const glm::vec2& mousePosition) { return mousePosition - m_position; }

	bool Camera::inView(const glm::vec2& pos, const glm::vec2& size)
	{
		const float MIN_DIST_X = (size.x / 2.0f) + m_sizeOffset.x;
		const float MIN_DIST_Y = (size.y / 2.0f) + m_sizeOffset.y;

		// Gets the distance between to to center points
		glm::vec2 distCenter = pos - m_positionCenter;

		// Finde the diffrence
		float xDiff = MIN_DIST_X - std::abs(distCenter.x);
		float yDiff = MIN_DIST_Y - std::abs(distCenter.y);

		if (xDiff > 0 && yDiff > 0) { return true; }

		return false;
	}
	#pragma endregion

	#pragma region Private Methods
	void Camera::updateOffset() { m_sizeOffset = (m_size / 2.0f); }
	void Camera::updateCenter() { m_positionCenter = m_position + m_sizeOffset; }
	#pragma endregion
}