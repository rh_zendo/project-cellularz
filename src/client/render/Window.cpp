#include "Window.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	Window::Window(ClientSDK* client) :
		m_client(client),
		m_window(nullptr)
	{}

	Window::~Window()
	{
		SDL_DestroyWindow(m_window);
		m_window = nullptr;
	}
	#pragma endregion

	#pragma region Get / Set Methods
	const int& Window::getWidth() { return m_width; }
	const int& Window::getHeight() { return m_height; }

	void Window::setTitle(const std::string& title) { SDL_SetWindowTitle(m_window, title.c_str()); }

	SDL_Window* Window::getSDLWindow() { return m_window; }
	#pragma endregion

	#pragma region Methods
	void Window::initialize(const std::string& title, const int& width, const int& height, const Uint32& currentFlags)
	{
		Console::print("[Window] initializing");

		// Convert Flags
		Uint32 flags = 0;
		if (currentFlags & WINDOWFLAG_HIDDEN) {	flags |= SDL_WINDOW_HIDDEN;	}
		if (currentFlags & WINDOWFLAG_FULLSCREEN) { flags |= SDL_WINDOW_FULLSCREEN_DESKTOP; }
		if (currentFlags & WINDOWFLAG_BORDERLESS) { flags |= SDL_WINDOW_BORDERLESS; }

		// Creating SDL_Window
		m_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, flags);
		if (m_window == nullptr) {
			m_client->fatalError("[Window] Failed to create SDL_Window\n\r" + std::string(SDL_GetError()));
		}

		// Sets width and height
		SDL_GetWindowSize(m_window, &m_width, &m_height);

		Console::print("[Window] initialized");
	}
	#pragma endregion
}