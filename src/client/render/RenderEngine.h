#pragma once

#include "ClientSDK.h"
#include "Camera.h"

#include "Color.h"

namespace cellularz
{
	// Forward declaration
	class ClientSDK;

	class RenderEngine
	{
	public:
		RenderEngine(ClientSDK* client, Camera* camera);
		~RenderEngine();

		// Get / Set Methods
		SDL_Renderer* getSDLRenderer();

		void setBackgroundColor(const Color& color);
		void setBackgroundColor(const Uint8& r, const Uint8& g, const Uint8& b, const Uint8& a);

		void setDrawColor(const Color& color);
		void setDrawColor(const Uint8& r, const Uint8& g, const Uint8& b, const Uint8& a);

		// Initialiation Methods
		void initialize(SDL_Window* sdlWindow);

		// Line Methods
		void drawLine(const glm::vec2& pointA, const glm::vec2& pointB);

		// Rectangle Methods
		void drawRect(const glm::vec2& pos, const glm::vec2& size);

		// Sprite Methods
		void drawSprite(SDL_Texture* texture, const glm::vec2& pos, const glm::vec2& size);
		void drawSprite(SDL_Texture* texture, const glm::vec2& pos, const glm::vec2& size, const float& angel);

		// Update Methods
		void renderUpdate();

	private:
		ClientSDK* m_client;
		Camera* m_camera;

		SDL_Renderer* m_renderer;

		Color m_colorBackground;
	};	
}