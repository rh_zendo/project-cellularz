#include "RenderEngine.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	RenderEngine::RenderEngine(ClientSDK* client, Camera* camera) :
		m_client(client),
		m_camera(camera),
		m_renderer(nullptr)
	{}

	RenderEngine::~RenderEngine()
	{
		SDL_DestroyRenderer(m_renderer);
	}
	#pragma endregion

	#pragma region Get / Set Methods
	SDL_Renderer* RenderEngine::getSDLRenderer() { return m_renderer; }

	void RenderEngine::setBackgroundColor(const Color& color) { m_colorBackground = color; }
	void RenderEngine::setBackgroundColor(const Uint8& r, const Uint8& g, const Uint8& b, const Uint8& a)
	{
		m_colorBackground.r = r;
		m_colorBackground.g = g;
		m_colorBackground.b = b;
		m_colorBackground.a = a;
	}

	void RenderEngine::setDrawColor(const Color& color) { SDL_SetRenderDrawColor(m_renderer, color.r, color.g, color.b, color.a); }
	void RenderEngine::setDrawColor(const Uint8& r, const Uint8& g, const Uint8& b, const Uint8& a) { SDL_SetRenderDrawColor(m_renderer, r, g, b, a); }
	#pragma endregion

	#pragma region Initialiation Methods
	void RenderEngine::initialize(SDL_Window* sdlWindow)
	{
		Console::print("[RenderEngine] initializing");

		// Creating SDL_Renderer
		m_renderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED);
		if (m_renderer == nullptr) {
			m_client->fatalError("[RenderEngine] Failed to create SDL_Renderer\n\r" + std::string(SDL_GetError()));
		}

		// Enables linear filtering on texturers
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
			Console::print("[Warning][RenderEngine] Failed to enable linear filtering on textures\n\r" + std::string(SDL_GetError()));
		}

		// Enables blendmode on renderer
		if (SDL_SetRenderDrawBlendMode(m_renderer, SDL_BLENDMODE_NONE) != 0) {
			Console::print("[Warning][RenderEngine] Failed to enable blendmode\n\r" + std::string(SDL_GetError()));
		}

		Console::print("[RenderEngine] initialized");
	}
	#pragma endregion

	#pragma region Line Methods
	void RenderEngine::drawLine(const glm::vec2& pointA, const glm::vec2& pointB)
	{
		glm::vec2 cameraPosition(m_camera->getPosition());

		int posAx = (int)(pointA.x + cameraPosition.x);
		int posAy = (int)(pointA.y + cameraPosition.y);
		int posBx = (int)(pointB.x + cameraPosition.x);
		int posBy = (int)(pointB.y + cameraPosition.y);

		// TODO: Make camera math right (Scaling)
		SDL_RenderDrawLine(m_renderer, posAx, posAy, posBx, posBy);
	}
	#pragma endregion

	#pragma region Rectangle Methods
	void RenderEngine::drawRect(const glm::vec2& pos, const glm::vec2& size)
	{
		SDL_Rect renderQuad;
		glm::vec2 cameraPosition(m_camera->getPosition());

		// TODO: Make camera math right (Scaling)
		renderQuad.x = (int)(pos.x + cameraPosition.x);
		renderQuad.y = (int)(pos.y + cameraPosition.y);
		renderQuad.w = (int)(size.x);
		renderQuad.h = (int)(size.y);

		SDL_RenderDrawRect(m_renderer, &renderQuad);
	}
	#pragma endregion
	
	#pragma region Sprite Methods
	void RenderEngine::drawSprite(SDL_Texture* texture, const glm::vec2& pos, const glm::vec2& size)
	{
		SDL_Rect renderQuad;
		glm::vec2 cameraPosition(m_camera->getPosition());

		// TODO: Make camera math right (Scaling)
		renderQuad.x = (int)(pos.x + cameraPosition.x);
		renderQuad.y = (int)(pos.y + cameraPosition.y);
		renderQuad.w = (int)(size.x);
		renderQuad.h = (int)(size.y);

		// Draws texture
		SDL_RenderCopy(m_renderer, texture, NULL, &renderQuad);
	}

	void RenderEngine::drawSprite(SDL_Texture* texture, const glm::vec2& pos, const glm::vec2& size, const float& angel)
	{
		SDL_Rect renderQuad;
		glm::vec2 cameraPosition(m_camera->getPosition());

		// TODO: Make camera math right (Scaling)
		renderQuad.x = (int)(pos.x + cameraPosition.x);
		renderQuad.y = (int)(pos.y + cameraPosition.y);
		renderQuad.w = (int)(size.x);
		renderQuad.h = (int)(size.y);

		// Draws texture
		SDL_RenderCopyEx(m_renderer, texture, NULL, &renderQuad, (float)angel, NULL, SDL_FLIP_NONE);
	}
	#pragma endregion

	#pragma region Update Methods
	void RenderEngine::renderUpdate()
	{
		// Renders the worlds game objects
		m_client->getWorld()->render(this);

		// Sets background color
		SDL_SetRenderDrawColor(m_renderer, m_colorBackground.r, m_colorBackground.g, m_colorBackground.b, m_colorBackground.a);

		// Renders every thing to the screen
		SDL_RenderPresent(m_renderer);

		// Cleans the render buffer
		SDL_RenderClear(m_renderer);
	}
	#pragma endregion
}