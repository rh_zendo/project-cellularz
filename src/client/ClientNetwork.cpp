#include "ClientNetwork.h"
#include "ClientNetwork.h"

#include "PacketUtils.h"
#include "PacketFactory.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	ClientNetwork::ClientNetwork(ClientSDK* client) :
		m_client(client),
		m_tickLimiter(CLIENTNETWORK_TICKRATE)
	{}

	ClientNetwork::~ClientNetwork()
	{
		// Closing socket
		SDLNet_UDP_Close(m_socket);

		// Freeing packet memory
		SDLNet_FreePacket(m_packetIn);
	}
	#pragma endregion

	#pragma region Get / Set Methods
	UDPsocket* ClientNetwork::getUDPsocket() { return &m_socket; }
	AckManager* ClientNetwork::getAckManager() { return m_ackManager; }
	#pragma endregion

	#pragma region Methods
	void ClientNetwork::initialize()
	{
		Console::print("[Network] initializing");

		// Creating UDP Socket
		m_socket = SDLNet_UDP_Open(0);
		if (!m_socket) {
			m_client->fatalError("[Network] Failed to create udp socket\n\r" + std::string(SDLNet_GetError()));
		}

		// Allocating Packet buffers
		if (!(m_packetIn = SDLNet_AllocPacket(PACKET_SIZE))) {
			m_client->fatalError("[Network] Failed to allocating input buffer\n\r" + std::string(SDLNet_GetError()));
		}

		// Initializing Ack Manager
		m_ackManager = new AckManager(this);

		Console::print("[Network] initialization complete");
	}

	void ClientNetwork::startNetworking()
	{
		// Creating thread
		m_thread = SDL_CreateThread(runner, "ClientNetwork", this);
	}
	#pragma endregion

	#pragma region Methods
	void ClientNetwork::connect(const std::string& hostname, const Uint16& hostport)
	{
		IPaddress ipAddress;
		if (SDLNet_ResolveHost(&ipAddress, hostname.c_str(), hostport) == -1) {
			Console::print("[Network] Failed to resolve " + hostname + ":" + std::to_string(hostport));
			return;
		}

		if (SDLNet_UDP_Bind(m_socket, 0, &ipAddress) == -1) {
			Console::print("[Network] Failed to bind to " + hostname + ":" + std::to_string(hostport));
			return;
		}

		// Adds connection packet to the output buffer
		m_packetQueue.addOutputPacket(PacketFactory::connect("Billy"));
	}

	std::vector<UDPpacket*> ClientNetwork::getInputPacketQueue() { return m_packetQueue.getInputBuffer(); }
	std::vector<UDPpacket*> ClientNetwork::getOutputPacketQueue() { return m_packetQueue.getOutputBuffer(); }

	void ClientNetwork::addInputPacketToQueue(UDPpacket* packet) { m_packetQueue.addInputPacket(packet); }
	void ClientNetwork::addOutputPacketToQueue(UDPpacket* packet) { m_packetQueue.addOutputPacket(packet); }
	#pragma endregion

	#pragma region Private Methods
	void ClientNetwork::loop()
	{
		while (m_client->isRunning()) {
			// Begin TickLimiter
			m_tickLimiter.begin();

			// Process received packets
			while (SDLNet_UDP_Recv(m_socket, m_packetIn)) {
				receivedPacket(m_packetIn);
			}

			// Adds keep alive packet to output buffer
			m_packetQueue.addOutputPacket(PacketFactory::client_keepalive());

			// Process send packets
			std::vector<UDPpacket*> buffer = m_packetQueue.getOutputBuffer();
			for (std::size_t i = 0; i < buffer.size(); ++i) {
				sendPacket(buffer[i]);
			}

			// Updates Managers
			m_ackManager->update();

			// End TickLimiter
			m_tickLimiter.end();
		}
	}

	void ClientNetwork::receivedPacket(UDPpacket* packet)
	{
		// Checks if header are valid
		if (packet->len < PACKET_SIZE_HEADERS) {
			Console::print("[Network] Packet is invalid");
			return;
		}

		// Checks if the packet should be handeld in the serverNetwork thread
		switch (PacketUtils::decodeHeaderProtocalId(packet)) {
			// If its a ack packet send it to the ack manager, and ends
			case PacketProtocalID::ACK:
				m_ackManager->acknowledgePacketByAckId(PacketUtils::decodeAckAckId(packet), packet->channel);
				return;
			break;
		}

		// Checks if packet should be acknowledge
		Uint16 ackId = PacketUtils::decodeHeaderAckId(packet);
		if (ackId != ACKMANAGER_DISABLE_ACKID) {
			m_packetQueue.addOutputPacket(PacketFactory::ack(ackId));
		}

		// Clones and adds the packet to the Input Buffer
		m_packetQueue.addInputPacket(PacketUtils::clonePacket(packet));
	}

	void ClientNetwork::sendPacket(UDPpacket* packet)
	{
		// Checks if packet should be managed by ack manager
		bool ackHandeld = false;
		if (PacketUtils::decodeHeaderAckId(packet) == ACKMANAGER_ENABLED_ACKID) {
			ackHandeld = m_ackManager->encodeAckId(packet);
		}

		// Sends packet to server
		SDLNet_UDP_Send(m_socket, 0, packet);

		// Checks if the packet is handeld by the ack manager, otherwise frees heap memory
		if (!ackHandeld) {
			SDLNet_FreePacket(packet);
		}
	}
	#pragma endregion

	#pragma region Thread
	int ClientNetwork::runner(void* ptr)
	{
		ClientNetwork* clientNetwork = (ClientNetwork*)ptr;

		// Starting loop
		clientNetwork->loop();

		return 0;
	}
	#pragma endregion
}