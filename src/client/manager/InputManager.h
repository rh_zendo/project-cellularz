#pragma once

#include <map>
#include <SDL.h>
#include <glm/glm.hpp>

#include "ClientSDK.h"

namespace cellularz
{
	// Forward declaration
	class ClientSDK;

	class InputManager
	{
	public:
		InputManager(ClientSDK* client);
		~InputManager();

		// Get / Set Methods
		bool const isKeyDown(SDL_Keycode keycode);

		glm::ivec2 const getMousePos();
		bool const isMouseDown(Uint8 mouseCode);

		// Methods
		void update();

	private:
		// Members
		ClientSDK* m_client;

		std::map<SDL_Keycode, bool> m_keys;
		glm::ivec2 m_mousePos;
		std::map<Uint8, bool> m_mouseKeys;

		// Methods
		void keyDown(SDL_Keycode keycode);
		void keyUp(SDL_Keycode keycode);

		void mouseMoveUpdate();
		void mouseButtonDown(Uint8 mouseCode);
		void mouseButtonUp(Uint8 mouseCode);
	};
}