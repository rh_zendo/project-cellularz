#include "AudioManager.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	AudioManager::AudioManager(ClientSDK* client) :
		m_client(client),
		m_music(nullptr)
	{}

	AudioManager::~AudioManager()
	{}
	#pragma endregion

	#pragma region Methods 
	void AudioManager::initializeAudio(AudioID id, std::string soundPath)
	{
		if (id == AudioID::BACKGROUND) {
			//Load music
			m_music = Mix_LoadMUS(soundPath.c_str());
			if (m_music == NULL) {
				m_client->fatalError("[AudioManager] Failed to load Mix_Music(music)\n\r" + std::string(Mix_GetError()));
			}
		} else {
			Mix_Chunk *effect = nullptr;
			// Load effect
			effect = Mix_LoadWAV(soundPath.c_str());
			if (effect == NULL) {
				m_client->fatalError("[AudioManager] Failed to load Mix_Chunk(sound effect)\n\r" + std::string(Mix_GetError()));
			}

			//Insert new effect file to mapping
			m_effects[id] = effect;
		}

	}

	Mix_Chunk* AudioManager::getChunk(AudioID id)
	{
		if (m_effects.find(id) == m_effects.end()) {
			return m_effects.at(AudioID::DEBUG);
		} else {
			return m_effects.at(id);
		}
	}

	void AudioManager::playIntro()
	{
		if (Mix_PlayingMusic() == 0) {
			Mix_PlayMusic(m_music, -1);
		}
	}

	void AudioManager::play(AudioID id)
	{
		Mix_PlayChannel(-1 , getChunk(id), 0);
	}

	void AudioManager::stop(AudioID id)
	{
		if (Mix_PlayingMusic() == 1) {
			Mix_HaltMusic();
		}
	}
	#pragma endregion
}