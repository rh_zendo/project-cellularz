#include "TextureManager.h"

#include <SDL_image.h>

namespace cellularz
{
	#pragma region Constructor / Destructor
	TextureManager::TextureManager(ClientSDK* client) :
		m_client(client)
	{}

	TextureManager::~TextureManager()
	{}
	#pragma endregion

	#pragma region Get / Set Methods
	SDL_Texture* TextureManager::getTexture(const TextureID& textureId)
	{
		if (m_textures.find(textureId) == m_textures.end()) {
			return m_textures.at(TextureID::DEBUG);
		} else {
			return m_textures.at(textureId);
		}
	}
	#pragma endregion

	#pragma region Methods
	void TextureManager::loadTexture(const TextureID& textureId, const std::string& filePath)
	{
		SDL_Texture* texture = nullptr;
		SDL_Surface* surface = loadPNG(filePath);

		// Create texture from surface pixels
		texture = SDL_CreateTextureFromSurface(m_client->getRenderEngine()->getSDLRenderer(), surface);
		if (texture == nullptr) {
			m_client->fatalError("[TextureManager] Failed to create texture\n\r" + std::string(SDL_GetError()));
		}

		// Clearing surface memory
		SDL_FreeSurface(surface);

		// Sets blend mode
		if(SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND) != 0) {
			Console::print("[Warning][TextureManager] Failed to enable blendmode on TextureID:" + std::to_string(textureId) + "\n\r" + std::string(SDL_GetError()));
		}

		// Insert new texture in to map
		m_textures[textureId] = texture;
	}
	#pragma endregion

	#pragma region Private Methods
	SDL_Surface* TextureManager::loadPNG(const std::string& filePath)
	{
		SDL_Surface* surface = nullptr;
		surface = IMG_Load(filePath.c_str());
		if (surface == nullptr) {
			m_client->fatalError("[TextureManager] Failed to create surface\n\r" + std::string(IMG_GetError()));
		}

		return surface;
	}
	#pragma endregion
}