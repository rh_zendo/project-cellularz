#pragma once

#include <string>
#include <map>
#include <SDL.h>

#include "ClientSDK.h"
#include "TextureID.h"

namespace cellularz
{
	// Forward declarations
	class ClientSDK;

	class TextureManager
	{
	public:
		TextureManager(ClientSDK* client);
		~TextureManager();

		// Get / Set Methods
		/// Gets a SDL texture pointer from the specified textureId
		SDL_Texture* getTexture(const TextureID& textureId);

		// Methods
		/// Loads a texture and binds is to the texture id
		void loadTexture(const TextureID& textureId, const std::string& filePath);

	private:
		// Members
		ClientSDK* m_client;
		std::map<TextureID, SDL_Texture*> m_textures;

		// Methods
		SDL_Surface* loadPNG(const std::string& filePath);
	};
}