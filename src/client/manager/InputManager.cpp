#include "InputManager.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	InputManager::InputManager(ClientSDK* client) :
		m_client(client)
	{}

	InputManager::~InputManager()
	{}
	#pragma endregion

	#pragma region Get / Set Methods
	bool const InputManager::isKeyDown(SDL_Keycode keycode)
	{
		if (m_keys.find(keycode) == m_keys.end()) {
			return false;
		} else {
			return m_keys.at(keycode);
		}
	}

	glm::ivec2 const InputManager::getMousePos() { return m_mousePos; }
	bool const InputManager::isMouseDown(Uint8 mouseCode)
	{
		if (m_mouseKeys.find(mouseCode) == m_mouseKeys.end()) {
			return false;
		} else {
			return m_mouseKeys.at(mouseCode);
		}
	}
	#pragma endregion

	#pragma region Methods
	void InputManager::update()
	{
		SDL_Event e;

		while (SDL_PollEvent(&e)) {
			switch (e.type) {
				// SDL Quit
				case SDL_QUIT: m_client->stopClient(); break;

				// Keyboard events
				case SDL_KEYUP: keyUp(e.key.keysym.sym); break;
				case SDL_KEYDOWN: keyDown(e.key.keysym.sym); break;

				// Mouse events
				case SDL_MOUSEMOTION: mouseMoveUpdate(); break;
				case SDL_MOUSEBUTTONDOWN: mouseButtonDown(e.button.button);	break;
				case SDL_MOUSEBUTTONUP: mouseButtonUp(e.button.button);	break;
			}
		}
	}
	#pragma endregion

	#pragma region Private Methods
	void InputManager::keyDown(SDL_Keycode keycode) { m_keys[keycode] = true; }
	void InputManager::keyUp(SDL_Keycode keycode) { m_keys[keycode] = false; }

	void InputManager::mouseMoveUpdate() { SDL_GetMouseState(&m_mousePos.x, &m_mousePos.y); }
	void InputManager::mouseButtonDown(Uint8 mouseCode) { m_mouseKeys[mouseCode] = true; }
	void InputManager::mouseButtonUp(Uint8 mouseCode) {	m_mouseKeys[mouseCode] = false; }
	#pragma endregion
}