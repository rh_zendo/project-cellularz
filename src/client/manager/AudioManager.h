#pragma once

#include <map>

#include <SDL.h>
#include <SDL_mixer.h>
#include <stdio.h>

#include "ClientSDK.h"
#include "AudioID.h"

namespace cellularz
{
	// Forward declaration
	class ClientSDK;

	class AudioManager
	{
	public:
		AudioManager(ClientSDK* client);
		~AudioManager();

		//Methods
		void initializeAudio(AudioID id, std::string soundPath);
		void play(AudioID id);
		void stop(AudioID id);
		void playIntro();
		Mix_Chunk* getChunk(AudioID id);

	private:
		// Members
		ClientSDK* m_client;
		Mix_Music* m_music;
		std::map<AudioID, Mix_Chunk*> m_effects;
	};
}