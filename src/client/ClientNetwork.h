#pragma once

#include <SDL_net.h>

#include "ClientSDK.h"
#include "TickLimiter.h"

#include "AckManager.h"

#include "PacketQueue.h"

namespace cellularz
{
	// Forward declaration
	class ClientSDK;
	class AckManager;

	class ClientNetwork
	{
	public:
		ClientNetwork(ClientSDK* client);
		~ClientNetwork();

		// Get / Set Methods
		/// Returns a pointer to the UDPsocket
		UDPsocket* getUDPsocket();
		/// Returns a pointer to the AckManager
		AckManager* getAckManager();

		// Initialization Methods
		/// Will initialize client network 
		void initialize();
		/// This will start the network thread 
		void startNetworking();

		// Methods
		/// Will try to connect to a server with the given hostname and hostport
		void connect(const std::string& hostname, const Uint16& hostport);

		std::vector<UDPpacket*> getInputPacketQueue();
		std::vector<UDPpacket*> getOutputPacketQueue();

		void addInputPacketToQueue(UDPpacket* packet);
		void addOutputPacketToQueue(UDPpacket* packet);

	private:
		// Members
		ClientSDK* m_client;
		TickLimiter m_tickLimiter;

		// Sockets
		UDPsocket m_socket;
		AckManager* m_ackManager;

		// Packet Buffers
		UDPpacket* m_packetIn;
		PacketQueue m_packetQueue;

		// Methods
		void loop();
		void receivedPacket(UDPpacket* packet);
		void sendPacket(UDPpacket* packet);

		// Thread
		SDL_Thread *m_thread;
		static int runner(void* ptr);
	};
}