#pragma once

#include "ClientSDK.h"
#include "ClientNetwork.h"

namespace cellularz
{
	// Forward declaration
	class ClientSDK;
	class ClientNetwork;

	class ClientPacketHandler
	{
	public:
		ClientPacketHandler(ClientSDK* client, ClientNetwork* clientNetwork);
		~ClientPacketHandler();

		// Methods
		void handelInputPackets();
		void handelOutputPackets();

	private:
		// Members
		ClientSDK* m_client;
		ClientNetwork* m_clientNetwork;

	};
}
