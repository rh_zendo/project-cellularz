#pragma once

#include "Console.h"
#include "TickLimiter.h"

#include "RenderEngine.h"
#include "Window.h"
#include "Camera.h"

#include "AudioManager.h"
#include "InputManager.h"
#include "TextureManager.h"

#include "ClientNetwork.h"
#include "ClientPacketHandler.h"

#include "World.h"
#include "Config.h"

#include "GameObject.h"

#include "AxisID.h"

namespace cellularz
{
	// Forward declaration
	class Console;

	class Window;
	class RenderEngine;
	
	class AudioManager;
	class InputManager;
	class TextureManager;
	class KeyBindingManager;

	class ClientNetwork;
	class ClientPacketHandler;

	class World;
	class Config;

	class GameObject;

	class ClientSDK
	{
	public:
		ClientSDK();
		~ClientSDK();

		// Get / Set
		bool isRunning();

		/// Gets the clients server channel id, return -1 if the clients is not connected
		const int& getServerChannelId();

		/// Sets the clients server channel id + triggers connection hook
		void setServerChannelId(const int& channelId);

		Window* getWindow();
		Camera* getCamara();
		RenderEngine* getRenderEngine();

		AudioManager* getAudioManager();
		InputManager* getInputManager();
		TextureManager* getTextureManager();
		KeyBindingManager* getKeyBindingmanager();

		Config* getConfig();

		ClientNetwork* getNetwork();

		World* getWorld();

		// Initialiation Methods
		void initialize();
		void startClient();
		void stopClient();

		// Methods
		void fatalError(const std::string& msg);

		// Initialiation Hooks
		virtual void onInitializeResources(AudioManager* audioManager, TextureManager* textureManager) {}
		virtual void onInitializeWorld(World* world, Config* config) {}

		// Hooks
		virtual void onTick(const double& deltaTime) {}
		virtual void onGameObjectCollision(GameObject* objectA, GameObject* objectB) {}
		virtual void onGameObjectCollisionResponse(GameObject* object, AxisID axis) {}
		virtual void onGameObjectOutOfWorld(GameObject* object) {}

		// Connection Hooks
		virtual void onServerConnect() {}
		virtual void onServerConnectChannelId(int channelId) {}
		virtual void onServerConnectFailed() {}
		virtual void onServerDisconnect() {}
		virtual void onServerUnresponsive() {}

		// Component Hooks
		virtual void onComponentAddedByNetworking(GameObject* object, ComponentID compId) {}
		virtual void onComponentRemovedByNetworking(GameObject* object, ComponentID compId) {}

	private:
		// Members
		bool m_isRunning;
		int m_serverChannelId;
		TickLimiter m_tickLimiter;

		RenderEngine* m_renderEngine;
		Window* m_window;
		Camera m_camera;

		AudioManager* m_audioManager;
		InputManager* m_inputManager;
		TextureManager* m_textureManager;
		KeyBindingManager* m_keyBindingManager;

		Config* m_config;

		ClientNetwork* m_clientNetwork;
		ClientPacketHandler* m_clientPacketHandler;

		World* m_world;

		// Methods
		void loopInitialize();
		void loop();

		// Thread
		SDL_Thread *m_thread;
		static int runner(void* ptr);
	};
}