#pragma once

#include <vector>
#include <SDL_net.h>

namespace cellularz
{
	class PacketQueue
	{
	public:
		PacketQueue();
		~PacketQueue();

		// Get / Set Methods
		/// Will get make a copy of the input buffer and then clear it
		std::vector<UDPpacket*> getInputBuffer();
		/// Will get make a copy of the output buffer and then clear it
		std::vector<UDPpacket*> getOutputBuffer();

		// Methods
		///  Will add a packet pointer to the input buffer
		void addInputPacket(UDPpacket* packet);
		/// Will add a packet pointer to the output buffer
		void addOutputPacket(UDPpacket* packet);

	private:
		// Members
		std::vector<UDPpacket*> m_inputBuffer;
		std::vector<UDPpacket*> m_outputBuffer;

		SDL_mutex* m_inputLock;
		SDL_mutex* m_outputLock;

		// Private Methods
		void removeInputPacket(UDPpacket* packet);
		void removeOutputPacket(UDPpacket* packet);
	};
}