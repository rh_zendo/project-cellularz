#pragma once

#include <string>
#include <SDL_net.h>

#include "PacketProtocalID.h"

namespace cellularz
{
	class PacketFactory
	{
	public:
		// Methods
		/// Creates a packet on the heap and allocates (size * PACKET_SIZE_HEADERS) bytes memory, sets channel to -1 and len to the allocation size
		static UDPpacket* const createPacket(const PacketProtocalID& protocalId, const int& size = 0);

		// Ack Packets
		static UDPpacket* const ack(const Uint16& ackId, const int& channelId = -1);

		// Connection Packets
		static UDPpacket* const connect(std::string username);
		static UDPpacket* const connect_failed(const IPaddress& ipAddress);

		static UDPpacket* const disconnect(const int& channelId = -1);
		static UDPpacket* const unresponsive(const int& channelId = -1);
		static UDPpacket* const reconnected(const int& channelId = -1);

		// Game Object Packets
		static UDPpacket* const gameobject_update(const Uint32& objectId, const Uint32& objectOwnerId, const Uint16& objectClassId, const Uint32& objectCompMask);
		static UDPpacket* const gameobject_kill(const Uint32& objectId);

		// Client
		static UDPpacket* const client_keepalive();
		static UDPpacket* const client_serverchannel(const Uint8& channelId);
		static UDPpacket* const client_inputevent(const Uint16& eventId, const int& size = 0);

		// Sound
		static UDPpacket* const sound_play(const Uint16& audioId);
	private:
	};

}