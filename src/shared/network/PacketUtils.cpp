#include "PacketUtils.h"

#include "Console.h"

#include "Settings.h"

namespace cellularz
{
	#pragma region Methods
	UDPpacket* const PacketUtils::clonePacket(UDPpacket* packet)
	{
		UDPpacket* result = SDLNet_AllocPacket(packet->len);
		result->address = packet->address;
		result->channel = packet->channel;
		std::memcpy(result->data, packet->data, packet->len);
		result->len = packet->len;
		result->status = packet->status;

		return result;
	}
	#pragma endregion

	#pragma region Packet Header Methods
	PacketProtocalID const PacketUtils::decodeHeaderProtocalId(UDPpacket* packet)
	{
		return (PacketProtocalID)SDLNet_Read16(packet->data + PACKET_POINTER_PROTOCALID);
	}

	void PacketUtils::encodeHeaderAckId(UDPpacket* packet, const Uint16& ackId)
	{
		SDLNet_Write16(ackId, packet->data + PACKET_POINTER_ACKID);
	}
	Uint16 const PacketUtils::decodeHeaderAckId(UDPpacket* packet)
	{
		return SDLNet_Read16(packet->data + PACKET_POINTER_ACKID);
	}
	#pragma endregion

	#pragma region Ack Packet Methods
	Uint16 const PacketUtils::decodeAckAckId(UDPpacket* packet)
	{
		return SDLNet_Read16(packet->data + PACKET_POINTER_DATA);
	}
	#pragma endregion

	#pragma region Connection Packet Methods
	std::string PacketUtils::decodeConnectUsername(UDPpacket* packet)
	{
		std::string username = std::string((const char*)(packet->data + PACKET_POINTER_DATA), (packet->len - PACKET_SIZE_HEADERS));
		return username;
	}
	#pragma endregion

	#pragma region Game Object Methods
	Uint32 const PacketUtils::decodeGameObjectPacketGetId(UDPpacket* packet)
	{
		return SDLNet_Read32(packet->data + PACKET_GAMEOBJECT_POINTER_ID);
	}
	Uint32 const PacketUtils::decodeGameObjectPacketGetOwnerId(UDPpacket* packet)
	{
		return SDLNet_Read32(packet->data + PACKET_GAMEOBJECT_POINTER_OWNERID);
	}
	Uint16 const PacketUtils::decodeGameObjectPacketGetClassId(UDPpacket* packet)
	{
		return SDLNet_Read16(packet->data + PACKET_GAMEOBJECT_POINTER_CLASSID);
	}

	Uint32 const PacketUtils::decodeGameObjectPacketGetCompMask(UDPpacket* packet)
	{
		return SDLNet_Read32(packet->data + PACKET_GAMEOBJECT_POINTER_COMPMASK);
	}
	#pragma endregion

	#pragma region Client Methods
	Uint8 const PacketUtils::decodeClientServerChannelId(UDPpacket* packet)
	{
		Uint8 channelId;

		std::memcpy(&channelId, packet->data + PACKET_CLIENT_SERVERCHANNEL_POINTER_CHANNELID, sizeof(Uint8));

		return channelId;
	}

	Uint16 const PacketUtils::decodeClientInputEventId(UDPpacket* packet)
	{
		return SDLNet_Read16(packet->data + PACKET_CLIENT_INPUTEVENT_POINTER_EVENTID);
	}
	#pragma endregion

	#pragma region Sound Methods
	Uint16 const PacketUtils::decodeSoundAudioId(UDPpacket* packet)
	{
		return SDLNet_Read16(packet->data + PACKET_SOUND_PLAY_POINTER_AUDIOID);
	}
	#pragma endregion
}