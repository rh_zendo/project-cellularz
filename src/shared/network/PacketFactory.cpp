#include "PacketFactory.h"

#include "PacketUtils.h"

#include "Settings.h"

namespace cellularz
{
	#pragma region Methods
	UDPpacket* const PacketFactory::createPacket(const PacketProtocalID& protocalId, const int& size)
	{
		// Allocates UDPpacket on heap and sets default settings
		UDPpacket* packet = SDLNet_AllocPacket(PACKET_SIZE_HEADERS + size);
		packet->channel = -1; // Sets channel to -1 by default
		packet->len = packet->maxlen; // Sets packet length, just in case some stupid programmer forget to set it when done writing

		// Writes ProtocalId header
		SDLNet_Write16(protocalId, packet->data + PACKET_POINTER_PROTOCALID);

		// Writes AckId to header, sets as disabled by default
		SDLNet_Write16(ACKMANAGER_DISABLE_ACKID, packet->data + PACKET_POINTER_ACKID);

		return packet;
	}
	#pragma endregion

	#pragma region Ack Packets
	UDPpacket* const PacketFactory::ack(const Uint16& ackId, const int& channelId) {
		UDPpacket* packet = createPacket(PacketProtocalID::ACK, PACKET_ACK_SIZE);
		packet->channel = channelId;

		// Writes ackId to packet data
		SDLNet_Write16(ackId, packet->data + PACKET_POINTER_DATA);

		return packet;
	}
	#pragma endregion

	#pragma region Connection Packets
	UDPpacket* const PacketFactory::connect(std::string username)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::CONNECT, (int)username.size());

		// Enables ack packet
		PacketUtils::encodeHeaderAckId(packet, ACKMANAGER_ENABLED_ACKID);

		// Writes username to packet data
		std::memcpy(packet->data + PACKET_POINTER_DATA, username.c_str(), username.size());

		return packet;
	}

	UDPpacket* const PacketFactory::connect_failed(const IPaddress& ipAddress)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::CONNECT_FAILED);
		packet->address = ipAddress;
		return packet;
	}

	UDPpacket* const PacketFactory::disconnect(const int& channelId)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::DISCONNECT);
		packet->channel = channelId;
		return packet;
	}
	UDPpacket* const PacketFactory::unresponsive(const int& channelId)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::UNRESPONSIVE);
		packet->channel = channelId;
		return packet;
	}
	UDPpacket* const PacketFactory::reconnected(const int& channelId)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::RECONNECTED);
		packet->channel = channelId;
		return packet;
	}

	#pragma endregion
	
	#pragma region Game Object Packets
	UDPpacket* const PacketFactory::gameobject_update(const Uint32& objectId, const Uint32& objectOwnerId, const Uint16& objectClassId, const Uint32& objectCompMask)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::GAMEOBJECT_UPDATE, PACKET_GAMEOBJECT_UPDATE_SIZE);
		packet->channel = CHANNELMANAGER_BROADCAST_CHANNEL; // Sets packet as a broadcast packet

		// Writes headers for the gameobject packet
		SDLNet_Write32(objectId, packet->data + PACKET_GAMEOBJECT_POINTER_ID);
		SDLNet_Write32(objectOwnerId, packet->data + PACKET_GAMEOBJECT_POINTER_OWNERID);
		SDLNet_Write16(objectClassId, packet->data + PACKET_GAMEOBJECT_POINTER_CLASSID);
		SDLNet_Write32(objectCompMask, packet->data + PACKET_GAMEOBJECT_POINTER_COMPMASK);

		return packet;
	}

	UDPpacket* const PacketFactory::gameobject_kill(const Uint32& objectId)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::GAMEOBJECT_KILL, PACKET_GAMEOBJECT_KILL_SIZE);
		packet->channel = CHANNELMANAGER_BROADCAST_CHANNEL; // Sets packet as a broadcast packet

		// Enables ack packet
		PacketUtils::encodeHeaderAckId(packet, ACKMANAGER_ENABLED_ACKID);

		// Writes headers for the gameobject packet
		SDLNet_Write32(objectId, packet->data + PACKET_GAMEOBJECT_POINTER_ID);

		return packet;
	}
	#pragma endregion


	#pragma region Client Packets
	UDPpacket* const PacketFactory::client_keepalive()
	{
		UDPpacket* packet = createPacket(PacketProtocalID::CLIENT_KEEPALIVE);
		return packet;
	}

	UDPpacket* const PacketFactory::client_serverchannel(const Uint8& channelId)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::CLIENT_SERVERCHANNEL, PACKET_CLIENT_SERVERCHANNEL_SIZE);
		packet->channel = channelId;

		// Enables ack packet
		PacketUtils::encodeHeaderAckId(packet, ACKMANAGER_ENABLED_ACKID);

		// Added channelId to packet data
		std::memcpy(packet->data + PACKET_CLIENT_SERVERCHANNEL_POINTER_CHANNELID, &channelId, sizeof(Uint8));

		return packet;
	}

	UDPpacket* const PacketFactory::client_inputevent(const Uint16& eventId, const int& size)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::CLIENT_INPUTEVENT, PACKET_CLIENT_INPUTEVENT_SIZE + size);

		// Writes EventID
		SDLNet_Write16(eventId, packet->data + PACKET_CLIENT_INPUTEVENT_POINTER_EVENTID);

		return packet;
	}
	#pragma endregion

	#pragma region Audio Packets
	UDPpacket* const PacketFactory::sound_play(const Uint16& audioId)
	{
		UDPpacket* packet = createPacket(PacketProtocalID::CLIENT_INPUTEVENT, PACKET_SOUND_PLAY_SIZE);

		// Writes AudioId
		SDLNet_Write16(audioId, packet->data + PACKET_SOUND_PLAY_POINTER_AUDIOID);

		return packet;
	}
	#pragma endregion
}