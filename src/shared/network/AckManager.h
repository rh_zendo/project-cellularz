#pragma once

#include <map>
#include <bitset>

#include <SDL.h>
#include <SDL_net.h>

#include "Settings.h"

#ifdef _BUILD_CLIENT
#include "ClientNetwork.h"
#endif

#ifdef _BUILD_SERVER
#include "ServerNetwork.h"
#endif

namespace cellularz
{
	// Forward declaration
	#ifdef _BUILD_CLIENT
	class ClientNetwork;
	#endif

	#ifdef _BUILD_SERVER
	class ServerNetwork;
	using ChannelAckBitMask = std::bitset<CHANNELMANAGER_LIMIT>;
	#endif

	/// Used by the AckManager to hold all the data need for the ack system
	struct AckManagerDataStruct
	{
		Uint32 sendAt = SDL_GetTicks(); /**< At what tick the packet was send */
		Uint32 resendAt; /**< At what tick the packet should resend */
		Uint8 resendCount = 0; /**< How many times the packet has been resend */

		UDPpacket* packet; /**< Pointer to the packet */

		#ifdef _BUILD_SERVER
		ChannelAckBitMask channelAckMask = { false }; /**< Used to see what channels hasnt acknowledge getting the packet */
		#endif
	};

	class AckManager
	{
	public:
		#ifdef _BUILD_CLIENT
		AckManager(ClientNetwork* clientNetwork);
		#endif

		#ifdef _BUILD_SERVER
		AckManager(ServerNetwork* serverNetwork);
		#endif

		~AckManager();

		// Methods
		/// Will encode the ackId in the packet header data, also add it to the AckManager, return false if the packet shouldnt be handeld.
		bool encodeAckId(UDPpacket* packet);

		/// Will acknowledge a packet by AckId, and remove the packet from the AckManager, also sends the back back to the inputmanger.
		void acknowledgePacketByAckId(const Uint16& ackId, const int& channelId = -1);

		/// Will run through ackData map to check if any packets should be resend again or it should be removed.
		void update();

	private:
		// Members
		Uint16 m_ackId;
		std::map<Uint16, AckManagerDataStruct> m_ackData;

		#ifdef _BUILD_CLIENT
		ClientNetwork* m_clientNetwork;
		#endif

		#ifdef _BUILD_SERVER
		ServerNetwork* m_serverNetwork;
		#endif

		// Methods
		/// Gets the next ackId
		const Uint16& getNextAckId();

		/// Added a ackData and it's id to the AckManager.
		void addAckData(const Uint16&, const AckManagerDataStruct& data);

		/// Gets a AckData iterator, if it's the same value as m_ackData.end() then the ackId dosnt exists.
		std::map<Uint16, AckManagerDataStruct>::iterator getAckDataIteratorById(const Uint16& managerRemove);

		/// Removes AckData by AckId
		void removeAckDataByAckId(const Uint16& ackId, bool force = false);

		/// Removes AckData by AckData iterator, also put the packet in the input buffer
		std::map<Uint16, AckManagerDataStruct>::iterator removeAckDataByIterator(std::map<Uint16, AckManagerDataStruct>::iterator it, bool managerRemove);
	};
}