#pragma once

#include <string>
#include <SDL_net.h>

#include "PacketProtocalID.h"

namespace cellularz
{
	class PacketUtils
	{
	public:
		// Methods
		static UDPpacket* const clonePacket(UDPpacket* packet);

		// Packet Header Methods
		static PacketProtocalID const decodeHeaderProtocalId(UDPpacket* packet);

		static void encodeHeaderAckId(UDPpacket* packet, const Uint16& ackId);
		static Uint16 const decodeHeaderAckId(UDPpacket* packet);

		// Ack Packets
		static Uint16 const decodeAckAckId(UDPpacket* packet);

		// Connection Packet Methods
		static std::string decodeConnectUsername(UDPpacket* packet);

		// Game Object Methods
		static Uint32 const decodeGameObjectPacketGetId(UDPpacket* packet);
		static Uint32 const decodeGameObjectPacketGetOwnerId(UDPpacket* packet);
		static Uint16 const decodeGameObjectPacketGetClassId(UDPpacket* packet);
		static Uint32 const decodeGameObjectPacketGetCompMask(UDPpacket* packet);

		// Client Methods
		static Uint8 const decodeClientServerChannelId(UDPpacket* packet);
		static Uint16 const decodeClientInputEventId(UDPpacket* packet);

		// Sound Methods
		static Uint16 const decodeSoundAudioId(UDPpacket* packet);

	private:

	};
	
}