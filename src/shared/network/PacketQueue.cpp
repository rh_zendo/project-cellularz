#include "PacketQueue.h"

#include <algorithm>
#include "Console.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	PacketQueue::PacketQueue() :
		m_inputLock(SDL_CreateMutex()),
		m_outputLock(SDL_CreateMutex())
	{
		SDL_UnlockMutex(m_inputLock);
		SDL_UnlockMutex(m_outputLock);
	}

	PacketQueue::~PacketQueue()
	{
		// Clearing Input Buffer
		for (std::size_t i = 0; i < m_inputBuffer.size(); ++i) {
			SDLNet_FreePacket(m_inputBuffer[i]);
		}
		m_inputBuffer.clear();

		// Clearing Output Buffer
		for (std::size_t i = 0; i < m_outputBuffer.size(); ++i) {
			SDLNet_FreePacket(m_outputBuffer[i]);
		}
		m_outputBuffer.clear();
	}
	#pragma endregion

	#pragma region Get / Set Methods
	std::vector<UDPpacket*> PacketQueue::getInputBuffer()
	{
		std::vector<UDPpacket*> result;

		if (SDL_LockMutex(m_inputLock) == 0) {
			// Checks if empty
			if (!m_inputBuffer.empty()) {
				// Copies vector and clears the queue buffer
				result = m_inputBuffer;
				m_inputBuffer.clear();
			}
			SDL_UnlockMutex(m_inputLock);
		}

		return result;
	}

	std::vector<UDPpacket*> PacketQueue::getOutputBuffer()
	{
		std::vector<UDPpacket*> result;

		if (SDL_LockMutex(m_outputLock) == 0) {
			// Checks if empty
			if (!m_outputBuffer.empty()) {
				// Copies vector and clears the queue buffer
				result = m_outputBuffer;
				m_outputBuffer.clear();
			}
			SDL_UnlockMutex(m_outputLock);
		}

		return result;
	}
	#pragma endregion

	#pragma region Methods
	void PacketQueue::addInputPacket(UDPpacket* packet)
	{
		if (SDL_LockMutex(m_inputLock) == 0) {
			m_inputBuffer.push_back(packet);
			SDL_UnlockMutex(m_inputLock);
		}
	}

	void PacketQueue::addOutputPacket(UDPpacket* packet)
	{
		if (SDL_LockMutex(m_outputLock) == 0) {
			m_outputBuffer.push_back(packet);
			SDL_UnlockMutex(m_outputLock);
		}
	}
	#pragma endregion

	#pragma region Private Methods
	void PacketQueue::removeInputPacket(UDPpacket* packet)
	{
		m_inputBuffer.erase(std::remove(m_inputBuffer.begin(), m_inputBuffer.end(), packet), m_inputBuffer.end());
	}

	void PacketQueue::removeOutputPacket(UDPpacket* packet)
	{
		m_outputBuffer.erase(std::remove(m_outputBuffer.begin(), m_outputBuffer.end(), packet), m_outputBuffer.end());
	}
	#pragma endregion


}
