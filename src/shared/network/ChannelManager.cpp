#ifdef _BUILD_SERVER
#include "ChannelManager.h"

#include "Console.h"
#include "PacketFactory.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	ChannelManager::ChannelManager(ServerNetwork* serverNetwork) :
		m_serverNetwork(serverNetwork)
	{}

	ChannelManager::~ChannelManager()
	{}
	#pragma endregion

	#pragma region Get / Set Methods
	bool ChannelManager::isChannelInUse(int channelId) { return m_channelsInUse[channelId]; }

	bool ChannelManager::isChannelUnresponsive(int channelId) { return m_channelsUnresponsive[channelId]; }

	IPaddress* ChannelManager::getChannelIP(int channelId)
	{
		IPaddress* address = SDLNet_UDP_GetPeerAddress(*m_serverNetwork->getUDPsocket() , channelId);
		if (address == nullptr) {
			Console::print("[ChannelManager] Failed to find channels IPaddress:\n\r" + std::string(SDLNet_GetError()));
		}

		return address;
	}

	const ChannelPoolBitMask& ChannelManager::getChannelBitMask() { return m_channelsInUse; }
	#pragma endregion

	#pragma region Methods
	int ChannelManager::connectWithIPaddress(IPaddress* address)
	{
		// Checks for free channels
		int channelId = getFreeChannel();
		if (channelId == -1) {
			Console::print("[ChannelManager] No free channels");
			return -1;
		}

		// Binds ip address to the free channel
		channelId = SDLNet_UDP_Bind(*m_serverNetwork->getUDPsocket(), channelId, address);
		if (channelId == -1) {
			Console::print("[ChannelManager] Failed to bind channel to socket:\n\r" + std::string(SDLNet_GetError()));
			return -1;
		}

		// Sets channel as inUse in the bitmask, updates the last response time
		m_channelsInUse[channelId] = true;
		m_channelsUnresponsive[channelId] = false;

		// Sends channel id to the client
		m_serverNetwork->addOutputPacketToQueue(PacketFactory::client_serverchannel(channelId));

		return channelId;
	}
	void ChannelManager::disconnectChannel(const int& channelId)
	{
		// Unbinds the channel
		SDLNet_UDP_Unbind(*m_serverNetwork->getUDPsocket(), channelId);

		// Send message to server to trigger onClientDisconnected
		if (!isChannelUnresponsive(channelId)) {
			m_serverNetwork->addInputPacketToQueue(PacketFactory::unresponsive(channelId));
		}

		// Send message to server to trigger onClientDisconnect
		if(isChannelInUse(channelId)) {
			m_serverNetwork->addInputPacketToQueue(PacketFactory::disconnect(channelId));
			m_channelsInUse[channelId] = false;
		}
	}
	void ChannelManager::disconnectAllChannels()
	{
		for (int i = 0; i < CHANNELMANAGER_LIMIT; i++) {
			disconnectChannel(i);
		}
	}

	void ChannelManager::broadcastUDPPacket(UDPpacket* packet, const ChannelPoolBitMask& sendMask)
	{
		// And's channelMask + sendMask
		ChannelPoolBitMask sendCheckMask = m_channelsInUse & sendMask;

		for (int i = 0; i < CHANNELMANAGER_LIMIT; i++) {
			// Checks if a channel is in use
			if (!sendCheckMask[i]) { continue; }

			// Sends UDPpacket to the channel
			SDLNet_UDP_Send(*m_serverNetwork->getUDPsocket(), i, packet);
		}
	}

	void ChannelManager::update()
	{
		for (int i = 0; i < CHANNELMANAGER_LIMIT; i++) {
			// Checks if a channel is in use
			if (!isChannelInUse(i)) { continue; }

			// Cals the last response time in ms
			Uint32 lastResponse = SDL_GetTicks() - m_channelLastResponse[i];

			// Checks if channel has "timed out"
			if (lastResponse >= CHANNELMANAGER_DISCONNECT_TIME) {
				disconnectChannel(i);
				continue;
			}

			// Checks if channel has is beginning to "timed out"
			if (lastResponse >= CHANNELMANAGER_DISCONNECTED_TIME) {
				// Send message to server to trigger onClientDisconnected
				if(!m_channelsUnresponsive[i]) {
					m_serverNetwork->addInputPacketToQueue(PacketFactory::unresponsive(i));
				}

				// Sets channel as unresponsive
				m_channelsUnresponsive[i] = true;
			} else if (m_channelsUnresponsive[i]) {
				// Resets the channels unresponsive state
				m_channelsUnresponsive[i] = false;
				m_serverNetwork->addInputPacketToQueue(PacketFactory::reconnected(i));
			}
		}
	}
	void ChannelManager::updateChannelLastResponse(const int& channelId) { m_channelLastResponse[channelId] = SDL_GetTicks(); }
	#pragma endregion

	#pragma region Private Methods
	int const ChannelManager::getFreeChannel() {
		for (int i = 0; i < CHANNELMANAGER_LIMIT; i++) {
			if (!isChannelInUse(i)) { return i; }
		}

		return -1;
	}
	#pragma endregion
}
#endif