#pragma once
#ifdef _BUILD_SERVER
#include <SDL_net.h>
#include <bitset>

#include "ServerNetwork.h"

#include "Settings.h"

namespace cellularz
{
	// Forward declaration
	class ServerNetwork;

	using ChannelPoolBitMask = std::bitset<CHANNELMANAGER_LIMIT>;

	class ChannelManager
	{
	public:
		ChannelManager(ServerNetwork* serverNetwork);
		~ChannelManager();

		// Get / Set Methods
		/// Returns if a channelId is in use
		bool isChannelInUse(int channelId);

		/// Returns if a channelId is in the disconnected state
		bool isChannelUnresponsive(int channelId);

		/// Gets a channelId's IPaddress, returns a nullptr if the channel has no IPaddress
		IPaddress* getChannelIP(int channelId);

		/// Gets the ChannelManager's channel bitmask
		const ChannelPoolBitMask& getChannelBitMask();

		// Methods
		/// Tries to connect an IPaddress with a free channel, returns -1 if it failes to connect
		int connectWithIPaddress(IPaddress* address);

		/// Disconnects the channelId, will send a DISCONNECT packet if the channel is in use
		void disconnectChannel(const int& channelId);
		/// Disconnects all channels
		void disconnectAllChannels();
		
		/// Will send the UDPpacket to open all channels, use a bit mask what channels to send to
		void broadcastUDPPacket(UDPpacket* packet, const ChannelPoolBitMask& sendMask = 0xFFFF);

		/// Check if any channels are "disconnected"
		void update();
		/// Updates the channels last response time
		void updateChannelLastResponse(const int& channelId);

	private:
		// Members
		ServerNetwork* m_serverNetwork;

		ChannelPoolBitMask m_channelsInUse = { false };
		ChannelPoolBitMask m_channelsUnresponsive = { false };
		Uint32 m_channelLastResponse[CHANNELMANAGER_LIMIT];

		// Methods
		int const getFreeChannel(); /**< Finds a free channelId, return -1 if there is no free channel */
	};
}
#endif