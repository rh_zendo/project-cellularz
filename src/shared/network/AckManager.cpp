#include "AckManager.h"

#include "PacketUtils.h"
#include "PacketFactory.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	#ifdef _BUILD_CLIENT
	AckManager::AckManager(ClientNetwork* clientNetwork) :
		m_ackId(0),
		m_clientNetwork(clientNetwork)
	{}
	#endif

	#ifdef _BUILD_SERVER
	AckManager::AckManager(ServerNetwork* serverNetwork) :
		m_ackId(0),
		m_serverNetwork(serverNetwork)
	{}
	#endif

	AckManager::~AckManager()
	{}
	#pragma endregion

	#pragma region Methods
	bool AckManager::encodeAckId(UDPpacket* packet)
	{
		// Creates data struct for AckData
		AckManagerDataStruct ackData;

		#ifdef _BUILD_SERVER
		// Checks if its a broadcast packet, and added a channelAckMask to the dataStruct
		if (packet->channel == CHANNELMANAGER_BROADCAST_CHANNEL) {
			ChannelAckBitMask mask = m_serverNetwork->getChannelManager()->getChannelBitMask();

			// Check the channelMask for connectet clients
			if (mask == 0) { return false; }

			// Sets channelAckMask
			ackData.channelAckMask = mask;
		}
		#endif

		// Sets the packet pointer
		ackData.packet = packet;

		// Sets when to resend
		ackData.resendAt = SDL_GetTicks() + ACKMANAGER_RESEND_DELAY;

		// Gets the next ackId and encodes it into the packet header
		Uint16 ackId = getNextAckId();
		PacketUtils::encodeHeaderAckId(packet, ackId);

		// Addes to ackData to the map
		addAckData(ackId, ackData);

		return true;
	}

	void AckManager::acknowledgePacketByAckId(const Uint16& ackId, const int& channelId)
	{
		// Checks if the manager has the ackId
		if (getAckDataIteratorById(ackId) == m_ackData.end()) { return; }

		#ifdef _BUILD_CLIENT
		removeAckDataByAckId(ackId);
		#endif

		#ifdef _BUILD_SERVER
		if (channelId == -1) {
			removeAckDataByAckId(ackId);
		} else {
			// Sets channel at acknowledge
			m_ackData[ackId].channelAckMask[channelId] = false; 

			// Checks if all channels has acknowledge
			if (m_ackData[ackId].channelAckMask == 0) {
				removeAckDataByAckId(ackId);
			}
		}
		#endif
	}

	void AckManager::update()
	{
		std::map<Uint16, AckManagerDataStruct>::iterator it = m_ackData.begin();
		while (it != m_ackData.end()) {
			// Check if the resend limit count has been hit
			if (it->second.resendCount >= ACKMANAGER_RESEND_LIMIT) {
				it = removeAckDataByIterator(it, true);
				continue;
			}

			if (it->second.resendAt < SDL_GetTicks()) {
				// Counts up resend counter
				it->second.resendCount++;

				// Sets when to resend next
				it->second.resendAt = SDL_GetTicks() + ACKMANAGER_RESEND_DELAY;

				#ifdef _BUILD_CLIENT
				SDLNet_UDP_Send(*m_clientNetwork->getUDPsocket(), it->second.packet->channel, it->second.packet);
				#endif

				#ifdef _BUILD_SERVER
				// Checks if it's a broadcast packet
				if (it->second.packet->channel == CHANNELMANAGER_BROADCAST_CHANNEL)	{
					m_serverNetwork->getChannelManager()->broadcastUDPPacket(it->second.packet, it->second.channelAckMask);
				} else {
					SDLNet_UDP_Send(*m_serverNetwork->getUDPsocket(), it->second.packet->channel, it->second.packet);
				}
				#endif
			}

			++it;
		}
	}
	#pragma endregion

	#pragma region Private Methods
	const Uint16& AckManager::getNextAckId()
	{
		m_ackId++;

		// Resets the ackId if ACKMANAGER_ENABLED_ACKID or ACKMANAGER_DISABLE_ACKID ackId's are hit
		if (m_ackId == ACKMANAGER_ENABLED_ACKID || m_ackId == ACKMANAGER_DISABLE_ACKID) { m_ackId = ACKMANAGER_START_ACKID; }

		return m_ackId;
	}

	void AckManager::addAckData(const Uint16& ackId, const AckManagerDataStruct& data)
	{
		auto it = m_ackData.begin();
		m_ackData.insert(it, std::pair<Uint16, AckManagerDataStruct>(ackId, data));  // max efficiency inserting
	}

	std::map<Uint16, AckManagerDataStruct>::iterator AckManager::getAckDataIteratorById(const Uint16& ackId)
	{
		std::map<Uint16, AckManagerDataStruct>::iterator it = m_ackData.find(ackId);
		return it;
	}

	void AckManager::removeAckDataByAckId(const Uint16& ackId, bool managerRemove)
	{
		// Gets Iterator and check if it exists
		auto it = getAckDataIteratorById(ackId);
		if (it != m_ackData.end()) {
			removeAckDataByIterator(it, managerRemove);
		}
	}

	std::map<Uint16, AckManagerDataStruct>::iterator AckManager::removeAckDataByIterator(std::map<Uint16, AckManagerDataStruct>::iterator it, bool managerRemove)
	{
		#ifdef _BUILD_CLIENT
		if (managerRemove) {
			switch (PacketUtils::decodeHeaderProtocalId(it->second.packet)) {
				case PacketProtocalID::CONNECT:
					IPaddress ip;
					ip.host = 0;
					ip.port = 0;
					m_clientNetwork->addInputPacketToQueue(PacketFactory::connect_failed(ip));
				break;
			}
			SDLNet_FreePacket(it->second.packet);
		} else {
			m_clientNetwork->addInputPacketToQueue(it->second.packet);
		}
		#endif

		#ifdef _BUILD_SERVER
		m_serverNetwork->addInputPacketToQueue(it->second.packet);
		#endif

		// Removes data from map and returns new iterator
		return m_ackData.erase(it);
	}
	#pragma endregion
}