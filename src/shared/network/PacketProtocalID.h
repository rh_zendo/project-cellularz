#pragma once

#include <SDL.h>

namespace cellularz
{
	enum PacketProtocalID : Uint16
	{
		_ZERO = 0x0000, /**< Dont use this !!!! */

		// Ack
		ACK = 0x0001,

		// Connection
		CONNECT = 0x0010,
		CONNECT_FAILED = 0x0011,
		DISCONNECT = 0x0012,
		UNRESPONSIVE = 0x0013,
		RECONNECTED = 0x0014,

		// Game Object
		GAMEOBJECT_UPDATE = 0x0020,
		GAMEOBJECT_KILL = 0x0021,

		// Client
		CLIENT_KEEPALIVE = 0x0030,
		CLIENT_SERVERCHANNEL = 0x0031,
		CLIENT_INPUTEVENT = 0x0032,

		// Sound
		SOUND_PLAY = 0x0040,

		UNKNOWN = 0xFFFF
	};
}