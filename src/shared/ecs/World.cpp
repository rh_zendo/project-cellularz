#include "World.h"

#include <SDL.h>
#include <algorithm>

#include "Utils.h"

#include "PacketUtils.h"
#include "PacketFactory.h"

// Component
#include "ComponentID.h"
#include "CTransform.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	World::World(void* sdkPtr) :
		m_sdkPtr(sdkPtr),
		m_objectId(ECS_WORLD_START_ID)
	{}
	World::~World() {}
	#pragma endregion

	#pragma region Get / Set Methods
	const glm::vec2& World::getSize() { return m_size; }
	void World::setSize(const float& width, const float& height)
	{
		m_size.x = width;
		m_size.y = height;
	}
	void World::setSize(const glm::vec2& size) { m_size = size; }

	#ifdef _BUILD_CLIENT
	ClientSDK* World::getClient() { return (ClientSDK*)m_sdkPtr; }
	#endif

	#ifdef _BUILD_SERVER
	ServerSDK* World::getServer() { return (ServerSDK*)m_sdkPtr; }
	#endif
	#pragma endregion

	#pragma region Game Object Methods
	GameObject* World::getGameObjectById(Uint32 objectId)
	{
		try {
			return m_gameObjects.at(objectId);
		} catch (const std::out_of_range&) {
			return nullptr;
		}
	}
	GameObject* World::createGameObject()
	{
		GameObject* object = new GameObject(this, m_objectId);
		m_gameObjects[m_objectId] = object;
		m_objectId++;

		return object;
	}
	GameObject* World::createGameObject(Uint32 objectId)
	{
		GameObject* object = new GameObject(this, objectId);
		m_gameObjects[objectId] = object;

		if (m_objectId >= objectId) { m_objectId = objectId + 1; }

		return object;
	}

	void World::killGameObject(GameObject* object) { m_deadGameObject.push_back(object->getId()); }
	void World::killGameObject(const Uint32& objectId) { m_deadGameObject.push_back(objectId); }

	std::size_t World::getGameObjectCount() { return m_gameObjects.size(); }
	const std::map<Uint32, GameObject*>& World::getGameObjects() { return m_gameObjects; }
	#pragma endregion

	#pragma region System Methods
	System* World::getSystem(const Uint8& id) { return m_systems[id]; }
	bool World::hasSystem(const Uint8& id) { return m_systems[id] != nullptr; }
	void World::removeSystem(const Uint8& id) { delete m_systems[id]; }
	#pragma endregion

	#pragma region Update Methods
	void World::update(const double& deltaTime)
	{
		// Loops through system updates
		for (int i = 0; i < ComponentID::_TOTAL_ComponentID; i++) {
			// Checks if system is enabled
			if (m_systems[i] == nullptr) { continue; }

			// Runs updates from the system
			m_systems[i]->update(deltaTime);
		}

		// Checks if objects are Out Of World
		GameObject* object;
		CTransform* cTransform;
		for (auto it = m_gameObjects.begin(); it != m_gameObjects.end(); it++) {
			// Sets pointers
			object = it->second;
			cTransform = object->getComponent<CTransform>(ComponentID::CTRANSFORM);

			if(!Utils::collisionAABB(cTransform->getPosition(), cTransform->getPosition() + cTransform->getSize(), glm::vec2(0.0f, 0.0f), m_size)) {
				#ifdef _BUILD_CLIENT
				getClient()->onGameObjectOutOfWorld(object);
				#endif

				#ifdef _BUILD_SERVER
				getServer()->onGameObjectOutOfWorld(object);
				#endif
			}
		}


		// Remove dead game objects
		Uint32 objectId;
		for (std::size_t i = 0; i < m_deadGameObject.size(); i++) {
			objectId = m_deadGameObject[i];
			removeGameObjectById(objectId);

			#ifdef _BUILD_SERVER
			UDPpacket* packet = PacketFactory::gameobject_kill(objectId);
			getServer()->getServerNetwork()->addOutputPacketToQueue(packet);
			#endif
		}
		m_deadGameObject.clear();
	}

	#ifdef _BUILD_CLIENT
	void World::render(RenderEngine* renderEngine)
	{
		// Loops through system rendering
		for (int i = 0; i < ComponentID::_TOTAL_ComponentID; i++) {
			// Checks if system is enabled
			if (m_systems[i] == nullptr) { continue; }

			// Runs updates from the system
			m_systems[i]->render(renderEngine);
		}
	}

	void World::handelUpdatePacket(UDPpacket* packet)
	{
		Uint32 objectId = PacketUtils::decodeGameObjectPacketGetId(packet);
		GameObject* object;

		// Checks if the id exists in the world
		auto it = m_gameObjects.find(objectId);
		if(it != m_gameObjects.end()) {
			object = it->second;
		} else {
			// Creates new game object
			object = createGameObject(objectId);
		}

		object->decodeUpdatePacket(packet);
	}
	#endif

	#ifdef _BUILD_SERVER
	void World::transmitWorldState(ServerNetwork* serverNetwork)
	{
		for (auto it = m_gameObjects.begin(); it != m_gameObjects.end(); ++it) {
			// Checks if the gameObject should send an updated
			if (it->second->getNextNetworkUpdate() >= SDL_GetTicks()) { continue; }

			// Encodes and sends packet
			UDPpacket* packet = it->second->encodeUpdatePacket();
			serverNetwork->addOutputPacketToQueue(packet);
		}
	}
	#endif
	#pragma endregion

	#pragma region Private Methods
	void World::removeGameObjectById(const Uint32& objectId)
	{
		delete m_gameObjects[objectId];
		m_gameObjects.erase(objectId);
	}
	#pragma endregion
}