#pragma once

#include <vector>
#include <map>

#include <glm/glm.hpp>

#include "System.h"
#include "GameObject.h"
#include "ComponentID.h"

#ifdef _BUILD_CLIENT
#include "ClientSDK.h"
#include "RenderEngine.h"
#endif

#ifdef _BUILD_SERVER
#include "ServerSDK.h"
#include "ServerNetwork.h"
#endif

#include "Settings.h"

namespace cellularz
{
	// Forward declaration
	class GameObject;
	class System;

	#ifdef _BUILD_CLIENT
	class ClientSDK;
	#endif

	#ifdef _BUILD_SERVER
	class ServerSDK;
	class ServerNetwork;
	#endif

	using GameObjectMap = std::map<Uint32, GameObject*>;

	class World
	{
	public:
		// Constructor / Destructor
		World(void* sdkPtr);
		~World();

		// Get / Set Methods
		/// Gets the size of the world
		const glm::vec2& getSize();
		void setSize(const float& width, const float& height);
		void setSize(const glm::vec2& size);

		#ifdef _BUILD_CLIENT
		/// Gets a pointer to the ClientSDK
		ClientSDK* getClient();
		#endif

		#ifdef _BUILD_SERVER
		/// Gets a pointer to the ServerSDK
		ServerSDK* getServer();
		#endif

		// GameObject Methods
		GameObject* getGameObjectById(Uint32 objectId);
		GameObject* createGameObject();
		GameObject* createGameObject(Uint32 objectId);

		/// Markes an GameObject as dead, it will be removed at the end of the update frame
		void killGameObject(GameObject* object);
		/// Markes an GameObject as dead, it will be removed at the end of the update frame
		void killGameObject(const Uint32& objectId);

		/// Gets the count of how many GameObject's are in the world
		std::size_t getGameObjectCount();
		/// Gets a all GameObjects inside the world
		const std::map<Uint32, GameObject*>& getGameObjects();

		// System Methods
		/// Gets the system pointer for the specified systemId
		System* getSystem(const Uint8& id);
		/// Checks if the world has specified systemId
		bool hasSystem(const Uint8& id);
		/// Removes the specified systemId from the world
		void removeSystem(const Uint8& id);

		/// Adds a system to the world, and returns a casted pointer to the system
		template <typename T, typename... TArgs>
		void addSystem(TArgs&&... args);
		/// Gets a castet system pointer, return nullptr if the system dosnt exists in the world
		template <typename T>
		T* const getSystem(const Uint8& id);

		// Update Methods
		/// Will update all systems and remove dead objects from the world
		void update(const double& deltaTime);

		#ifdef _BUILD_CLIENT
		void render(RenderEngine* renderEngine);
		void handelUpdatePacket(UDPpacket* packet);
		#endif

		#ifdef _BUILD_SERVER
		void transmitWorldState(ServerNetwork* serverNetwork);
		#endif

	private:
		// Members
		void* m_sdkPtr; /**< Used to point at the ClientSDK or ServerSDK*/

		glm::vec2 m_size;

		Uint32 m_objectId;
		std::vector<Uint32> m_deadGameObject;
		GameObjectMap m_gameObjects;

		System* m_systems[ECS_POOL_SIZE_COMPONENT_SYSTEM] = { nullptr };

		// Private Methods
		/// Removes a GameObject by id
		void removeGameObjectById(const Uint32& objectId);
	};

	template <typename T, typename... TArgs>
	void World::addSystem(TArgs&&... args)
	{
		// Checks if T is inherit System
		static_assert(std::is_base_of<System, T>(), "Template argument does not inherit from System");

		// Creates the system
		T* system(new T(std::forward<TArgs>(args)...));

		// Sets system pointer in array
		m_systems[system->getId()] = system;

		// Sets the systems world pointer
		system->setWorld(this);
	}

	template <typename T>
	T* const World::getSystem(const Uint8& id)
	{
		static_assert(std::is_base_of<System, T>(), "Template argument does not inherit from Component");
		return static_cast<T*>(getSystem(id));
	}
}