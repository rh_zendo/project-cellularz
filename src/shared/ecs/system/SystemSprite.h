#pragma once

#ifdef _BUILD_CLIENT

#include "System.h"

namespace cellularz
{
	class SystemSprite : public System
	{
	public:
		SystemSprite();

		// Inherited Methods
		virtual void render(RenderEngine* renderEngine) override;

	private:
		// Inherited Private Methods
		virtual void initialize() override;
		virtual void onObjectAdded(GameObject* object) override;
		virtual void onObjectRemoved(GameObject* object) override;
	};
}
#endif