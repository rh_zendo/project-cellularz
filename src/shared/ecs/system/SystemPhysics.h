#pragma once

#include "System.h"

// Componets
#include "CPhysics.h"
#include "CTransform.h"

namespace cellularz
{
	class SystemPhysics : public System
	{
	public:
		SystemPhysics();

		// Inherited Methods
		virtual void update(const double& deltaTime) override;

	private:
		// Private Methods
		/// Updates the GameObject's gravity
		void updateGravity(CPhysics* cPhysics, const double& deltaTime);
		/// Updates the GameObject's Velocity
		void updateVelocity(CPhysics* cPhysics, CTransform* cTransform, const double& deltaTime);

		// Inherited Private Methods
		virtual void initialize() override;
		virtual void onObjectAdded(GameObject* object) override;
		virtual void onObjectRemoved(GameObject* object) override;
	};
}