#include "SystemPlayer.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	SystemPlayer::SystemPlayer() :
		System(ComponentID::CPLAYER)
	{}

	SystemPlayer::~SystemPlayer()
	{}
	#pragma endregion

	#pragma region Methods
	GameObject* SystemPlayer::getGameObjectByChannelId(const Uint8& channelId)
	{
		GameObject* object;
		CPlayer* cPlayer;

		// Looping though game objects
		for (std::size_t i = 0; i < getGameObjects().size(); ++i) {
			// Sets object pointers
			object = getGameObjects()[i];
			cPlayer = object->getComponent<CPlayer>(ComponentID::CPLAYER);

			// Compares the channelId's, and return object pointer if they are the same
			if (cPlayer->getChannelId() == channelId) { return object; }
		}

		return nullptr;
	}
	#pragma endregion

	#pragma region Inherited Methods
	void SystemPlayer::update(const double& deltaTime)
	{
		GameObject* object;
		CPlayer* cPlayer;
		CTransform* cTransform;

		// Looping though game objects
		for (std::size_t i = 0; i < getGameObjects().size(); ++i) {
			// Getting object and need components
			object = getGameObjects()[i];
			cPlayer = object->getComponent<CPlayer>(ComponentID::CPLAYER);
			cTransform = object->getComponent<CTransform>(ComponentID::CTRANSFORM);

			#ifdef _BUILD_CLIENT
			// Checks if cam lock and updates cam if it is
			if (cPlayer->isCameraLocked()) { updateCamera(cPlayer, cTransform); }
			#endif
		}
	}
	#pragma endregion

	#pragma region Private Methods
	#ifdef _BUILD_CLIENT
	void SystemPlayer::updateCamera(CPlayer* cPlayer, CTransform* cTransform)
	{
		// Checks if the camera pointer is set
		if (cPlayer->getCamera() == nullptr) { return; }

		// Updates the camera's center position
		cPlayer->getCamera()->setCenterPosition(cTransform->getCenterPosition());
	}
	#endif
	#pragma endregion 

	#pragma region Inherited Private Methods
	void SystemPlayer::initialize()
	{
		Console::print("[SystemPlayer] initializing");
		Console::print("[SystemPlayer] initialized");
	}

	void SystemPlayer::onObjectAdded(GameObject* object)
	{}

	void SystemPlayer::onObjectRemoved(GameObject* object)
	{}
	#pragma endregion
}