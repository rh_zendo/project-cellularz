#pragma once

#include "System.h"

#include "QuadTree.h"
#include "CCollision.h"
#include "GameObject.h"

namespace cellularz
{
	class SystemCollision : public System
	{
	public:
		SystemCollision();
		virtual ~SystemCollision() override;

		// Inherited Methods
		virtual void update(const double& deltaTime) override;
		
		#ifdef _BUILD_CLIENT
		virtual void render(RenderEngine* renderEngine) override;
		#endif
	private:
		// Members
		QuadTree* m_quadTree;

		// Private Methods
		/// Checks if two collisionGroupBitMask have any matching collisionGroups
		const bool doesCollisionGroupsMatch(const CollisionGroupBitMask& collisionGroupsA, const CollisionGroupBitMask& collisionGroupsB);

		// Inherited Private Methods
		virtual void initialize() override;
		virtual void onObjectAdded(GameObject* object) override;
		virtual void onObjectRemoved(GameObject* object) override;
	};
}