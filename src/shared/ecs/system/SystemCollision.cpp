#include "SystemCollision.h"

#include "ComponentID.h"

// Components
#include "CTransform.h"
#include "CCollision.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	SystemCollision::SystemCollision() :
		System(ComponentID::CCOLLISON)
	{}

	SystemCollision::~SystemCollision()
	{
		delete m_quadTree;
	}
	#pragma endregion

	#pragma region Inherited Methods
	void SystemCollision::update(const double& deltaTime)
	{
		// PerfData
		#ifdef DEBUG_PREF_COUNTERS
		Utils::pref_AABBChecksReset();
		#endif

		// Resets the quadtree
		m_quadTree->reset();

		// Starting inserting GameObjects
		for (std::size_t i = 0; i < getGameObjects().size(); ++i) {
			m_quadTree->insertGameObject(getGameObjects()[i]);
		}

		// Makes pointers ready
		size_t nodeSize;
		QuadTreeNode* node;

		GameObject* objectA;
		CTransform* cTransformA;
		CCollision* cCollisionA;

		GameObject* objectB;
		CTransform* cTransformB;
		CCollision* cCollisionB;

		// Makes Collision Checks
		for (std::size_t i = 0; i < m_quadTree->getActiveNodes().size(); ++i) {
			// Sets node pointer and size
			node = m_quadTree->getActiveNodes()[i];
			nodeSize = node->getGameObjectCount();

			for (std::size_t iA = 0; iA < nodeSize; ++iA) {
				// Sets ObjectA pointers
				objectA = node->getGameObjects()[iA];
				cCollisionA = objectA->getComponent<CCollision>(ComponentID::CCOLLISON);
				cTransformA = objectA->getComponent<CTransform>(ComponentID::CTRANSFORM);

				for (std::size_t iB = 0; iB < nodeSize; ++iB) {
					// Skips the objectA
					if (iA == iB) { continue; }

					// Sets ObjectB pointers
					objectB = node->getGameObjects()[iB];
					cCollisionB = objectB->getComponent<CCollision>(ComponentID::CCOLLISON);
					cTransformB = objectB->getComponent<CTransform>(ComponentID::CTRANSFORM);

					// Checks if objectA and objectB share any Collision Groups, otherwise skips it
					if (!doesCollisionGroupsMatch(cCollisionA->getCollisionGroups() , cCollisionB->getCollisionGroups())) {	continue; }

					// Makes AABB check for the objects
					if (Utils::collisionAABB(
						cTransformA->getPosition(), cTransformA->getPosition() + cTransformA->getSize(),
						cTransformB->getPosition(), cTransformB->getPosition() + cTransformB->getSize())
						) {

						// Triggers onGameObjectCollision
						#ifdef _BUILD_CLIENT
						getWorld()->getClient()->onGameObjectCollision(objectA, objectB);

						#ifdef DEBUG_AABB_RENDER
						getWorld()->getClient()->getRenderEngine()->setDrawColor(255, 0, 0, 255);
						getWorld()->getClient()->getRenderEngine()->drawRect(cTransformA->getPosition(), cTransformA->getSize());

						getWorld()->getClient()->getRenderEngine()->setDrawColor(255, 0, 0, 255);
						getWorld()->getClient()->getRenderEngine()->drawRect(cTransformB->getPosition(), cTransformB->getSize());
						#endif
						#endif

						#ifdef _BUILD_SERVER
						getWorld()->getServer()->onGameObjectCollision(objectA, objectB);
						#endif

						// CHecks if dead and if in the same groups
						if (objectA->isAlive() && (cCollisionA->getCollisionResponseGroups() & cCollisionB->getCollisionGroups()) != 0x0000) {
							glm::vec2 posA = cTransformA->getCenterPosition();
							glm::vec2 posB = cTransformB->getCenterPosition();
							glm::vec2 sizeHalf = (cTransformA->getSize() + cTransformB->getSize()) / 2.0f;

							float dx = posA.x - posB.x;
							float px = (sizeHalf.x) - abs(dx);

							float dy = posA.y - posB.y;
							float py = (sizeHalf.y) - abs(dy);

							AxisID axis;

							if (px < py) {
								float sx = std::copysign<float, float>(1.0f, dx);
								cTransformA->addPosition(px * sx, 0.0f);
								axis = AxisID::X;
							} else {
								float sy = std::copysign<float, float>(1.0f, dy);
								cTransformA->addPosition(0.0f, py * sy);
								axis = AxisID::Y;
							}

							#ifdef _BUILD_CLIENT
							getWorld()->getClient()->onGameObjectCollisionResponse(objectA, axis);
							#endif

							#ifdef _BUILD_SERVER
							getWorld()->getServer()->onGameObjectCollisionResponse(objectA, axis);
							#endif
						}
					}
				}
			}
		}

		#ifdef DEBUG_QUADTREE_INFO
		Console::print("Nodes: " + std::to_string(m_quadTree->getNodes().size()) +
			"	Active: " + std::to_string(m_quadTree->getActiveNodes().size()) +
			"	Inactive: " + std::to_string(m_quadTree->getInactiveNodes().size())
			#ifdef DEBUG_PREF_COUNTERS
			+ "	AABB: " + std::to_string(Utils::pref_AABBChecksCount())
			#endif
			);
		#endif
	}

	#ifdef _BUILD_CLIENT
	void SystemCollision::render(RenderEngine* renderEngine)
	{
		#ifdef DEBUG_QUADTREE_RENDERNODES
		QuadTreeNode* node;
		Color color;
		if (!m_quadTree->getInactiveNodes().empty()) {
			for (std::size_t i = m_quadTree->getInactiveNodes().size() - 1; i > 0; --i) {
				node = m_quadTree->getInactiveNodes()[i];

				// Cals / Sets draw color
				color.r = (255 / QUADTREE_DEPTH) * node->getDepth();
				color.g = (255 / QUADTREE_DEPTH) - ((255 / QUADTREE_DEPTH) * node->getDepth());
				renderEngine->setDrawColor(color);

				renderEngine->drawRect(node->getBoundaryA(), node->getBoundaryB() - node->getBoundaryA());
			}
		}

		if(!m_quadTree->getActiveNodes().empty()) {
			for (std::size_t i = m_quadTree->getActiveNodes().size() - 1; i > 0; --i) {
				node = m_quadTree->getActiveNodes()[i];

				// Cals / Sets draw color
				color.r = (255 / QUADTREE_DEPTH) * node->getDepth();
				color.g = (255 / QUADTREE_DEPTH) - ((255 / QUADTREE_DEPTH) * node->getDepth());
				renderEngine->setDrawColor(color);

				renderEngine->drawRect(node->getBoundaryA(), node->getBoundaryB() - node->getBoundaryA());
			}
		}
		#endif
	}
	#endif
	#pragma endregion

	#pragma region Private Methods
	const bool SystemCollision::doesCollisionGroupsMatch(const CollisionGroupBitMask& collisionGroupsA, const CollisionGroupBitMask& collisionGroupsB)
	{
		CollisionGroupBitMask checkMask = collisionGroupsA & collisionGroupsB;

		// Returns true if bitmask has a true value anywhere
		return checkMask != 0;
	}
	#pragma endregion

	#pragma region Inherited Private Methods
	void SystemCollision::initialize()
	{
		Console::print("[SystemCollision] initializing");

		m_quadTree = new QuadTree(getWorld());
		m_quadTree->initialize();

		Console::print("[SystemCollision] initialized");
	}

	void SystemCollision::onObjectAdded(GameObject* object)
	{}
	void SystemCollision::onObjectRemoved(GameObject* object)
	{}
	#pragma endregion
}