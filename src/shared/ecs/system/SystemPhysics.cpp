#include "SystemPhysics.h"

namespace cellularz
{
	SystemPhysics::SystemPhysics() :
		System(ComponentID::CPHYSICS)
	{}

	#pragma region Inherited Methods
	void SystemPhysics::update(const double& deltaTime)
	{
		GameObject* object;
		CPhysics* cPhysics;
		CTransform* cTransform;

		// Looping though GameObject's
		for (std::size_t i = 0; i < getGameObjects().size(); ++i) {
			// Sets pointers
			object = getGameObjects()[i];
			cPhysics = object->getComponent<CPhysics>(ComponentID::CPHYSICS);
			cTransform = object->getComponent<CTransform>(ComponentID::CTRANSFORM);

			// Check if Gravity and then runs Gravity update
			if (cPhysics->hasGravity()) { updateGravity(cPhysics, deltaTime); }

			// Runs Velocity update
			updateVelocity(cPhysics, cTransform, deltaTime);
		}
	}
	#pragma endregion

	#pragma region Private Methods
	void SystemPhysics::updateGravity(CPhysics* cPhysics, const double& deltaTime)
	{
		// Cals acceleration
		double acceleration = (9.8f * cPhysics->getWeight()) * deltaTime;

		// Adds acceleration to velocity
		cPhysics->addVelocity(0.0f, (float)acceleration);
	}
	void SystemPhysics::updateVelocity(CPhysics* cPhysics, CTransform* cTransform, const double& deltaTime)
	{
		glm::vec2 tmpVelocity;
		tmpVelocity.x = (float)(cPhysics->getVelocity().x * deltaTime);
		tmpVelocity.y = (float)(cPhysics->getVelocity().y * deltaTime);

		// Sets new position
		cTransform->addPosition(tmpVelocity);
	}
	#pragma endregion

	#pragma region Inherited Private Methods
	void SystemPhysics::initialize()
	{
		Console::print("[SystemPhysics] initializing");
		Console::print("[SystemPhysics] initialized");
	}

	void SystemPhysics::onObjectAdded(GameObject* object)
	{}

	void SystemPhysics::onObjectRemoved(GameObject* object)
	{}
	#pragma endregion
}