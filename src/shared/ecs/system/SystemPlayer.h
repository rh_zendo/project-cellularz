#pragma once

#include "System.h"

// Componets
#include "CPlayer.h"
#include "CTransform.h"

namespace cellularz
{
	class SystemPlayer : public System
	{
	public:
		SystemPlayer();
		virtual ~SystemPlayer();

		// Methods
		/// Trys to find the object pointer by channelId, returns a nullptr is nothing is found
		GameObject* getGameObjectByChannelId(const Uint8& channelId);

		// Inherited Methods
		virtual void update(const double& deltaTime) override;

	private:
		// Private Methods
		#ifdef _BUILD_CLIENT
		/// Updates the camera's center positon
		void updateCamera(CPlayer* cPlayer, CTransform* cTransform);
		#endif

		// Inherited Private Methods
		virtual void initialize() override;
		virtual void onObjectAdded(GameObject* object) override;
		virtual void onObjectRemoved(GameObject* object) override;
	};
}