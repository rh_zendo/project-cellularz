#ifdef _BUILD_CLIENT

#include "SystemSprite.h"

#include "Utils.h"

// Components
#include "CSprite.h"
#include "CTransform.h"

namespace cellularz
{
	SystemSprite::SystemSprite() :
		System(ComponentID::CSPRITE)
	{}

	#pragma region Inherited Methods
	void SystemSprite::render(RenderEngine* renderEngine)
	{
		GameObject* object;
		CSprite* cSprite;
		CTransform* cTransform;

		// Looping though GameObject's
		for (std::size_t i = 0; i < getGameObjects().size(); ++i) {
			// Sets object pointers
			object = getGameObjects()[i];
			cSprite = object->getComponent<CSprite>(ComponentID::CSPRITE);
			cTransform = object->getComponent<CTransform>(ComponentID::CTRANSFORM);

			// Checks if texture is not a nullptr
			if (cSprite->getTexture() == nullptr) { continue; }

			// Renders sprite to client
			renderEngine->drawSprite(cSprite->getTexture(), cTransform->getPosition(), cTransform->getSize(), cTransform->getRotation());
		}
	}
	#pragma endregion

	#pragma region Inherited Private Methods
	void SystemSprite::initialize()
	{
		Console::print("[SystemSprite] initializing");
		Console::print("[SystemSprite] initialized");
	}

	void SystemSprite::onObjectAdded(GameObject* object)
	{}

	void SystemSprite::onObjectRemoved(GameObject* object)
	{}
	#pragma endregion

}
#endif