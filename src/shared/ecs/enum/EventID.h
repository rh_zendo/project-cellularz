#pragma once

#include <SDL.h>

namespace cellularz
{
	enum EventID : Uint16
	{
		// Movement
		PLAYER_MOVE_UP,
		PLAYER_MOVE_DOWN,
		PLAYER_MOVE_LEFT,
		PLAYER_MOVE_RIGHT,

		// Mouse
		PLAYER_MOUSE_LEFT,

		_TOTAL_EventID
	};
}