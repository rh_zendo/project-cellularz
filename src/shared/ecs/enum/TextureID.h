#pragma once

#include <SDL.h>

namespace cellularz
{
	enum TextureID : Uint16
	{
		DEBUG = 0x0000,

		// GameDemo
		ROCKET = 0x1000,
		WALL = 0x1001,
		CAREPACKAGE = 0x1002,
		BULLET = 0x1003,

		TOTAL_TextureID
	};
}