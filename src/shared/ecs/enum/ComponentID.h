#pragma once

#include <SDL.h>

namespace cellularz
{
	enum ComponentID : Uint8
	{
		// Container components
		CTRANSFORM,

		// System components
		CPLAYER,
		CPHYSICS,
		CSPRITE,
		CCOLLISON,

		_TOTAL_ComponentID
	};
}