#pragma once

#include <SDL.h>

namespace cellularz 
{
	enum class AxisID : Uint8
	{
		X,
		Y
	};
}