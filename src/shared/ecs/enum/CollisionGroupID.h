#pragma once

#include <SDL.h>

namespace cellularz
{
	enum CollisionGroupID : Uint8
	{
		WORLD,
		MAP,
		PLAYER,
		PROJECTILE
	};
}