#pragma once

#include <SDL.h>

namespace cellularz 
{
	enum class AudioID : Uint16
	{
		DEBUG = 0x0000,
		BACKGROUND = 0x0001,
		SHOOTING = 0x0002,
		
		_TOTAL_AudioID
	};
}