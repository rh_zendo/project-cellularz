#pragma once

#include <SDL.h>

namespace cellularz
{
	enum KeyEvent : Uint16
	{
		MOVE_UP,
		MOVE_DOWN,
		MOVE_LEFT,
		MOVE_RIGHT,
		SHOOT,
		HEIGHT,
		WIDTH,

		_SIZE
	};
}