#include "System.h"

#include <algorithm>

namespace cellularz
{
	#pragma region Constructor / Destructor
	System::System(ComponentID id) :
		m_id(id),
		m_world(nullptr)
	{}
	#pragma endregion

	#pragma region Get / Set Methods
	const ComponentID& System::getId() { return m_id; }
	
	World* System::getWorld() { return m_world; }
	void System::setWorld(World* world)
	{
		m_world = world;
		initialize();
	}

	const std::vector<GameObject*>& System::getGameObjects() { return m_gameObjects; }
	#pragma endregion

	#pragma region Methods
	void System::addGameObject(GameObject* object)
	{
		m_gameObjects.push_back(object);
		onObjectAdded(object);
	}
	void System::removeGameObject(GameObject* object)
	{
		m_gameObjects.erase(std::remove(m_gameObjects.begin(), m_gameObjects.end(), object), m_gameObjects.end());
		onObjectRemoved(object);
	}
	#pragma endregion
}