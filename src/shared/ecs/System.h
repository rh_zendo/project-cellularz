#pragma once

#include <vector>
#include <iostream>

#include "World.h"
#include "GameObject.h"

#include "ComponentID.h"

#ifdef _BUILD_CLIENT
#include "RenderEngine.h"
#endif

namespace cellularz
{
	// Forward declaration
	class World;
	class GameObject;
	class RenderEngine;

	class System
	{
	public:
		// Constructor / Destructor
		virtual ~System() {};

		// Get / Set Methods
		const ComponentID& getId();

		/// Gets a pointer to the world
		World* getWorld();
		/// Sets what world the system is in.
		void setWorld(World* world);

		/// Gets a refrence to the GameObjects in the system
		const std::vector<GameObject*>& getGameObjects();

		// Methods
		/// Adds a GameObject to the system
		void addGameObject(GameObject* object);
		/// Removes a GameObject from the system
		void removeGameObject(GameObject* object);

		// Abstract Methods
		virtual void update(const double& deltaTime) {}

		#ifdef _BUILD_CLIENT
		virtual void render(RenderEngine* renderEngine) {}
		#endif

	protected:
		System(ComponentID id);

		// Abstract Methods
		virtual void initialize() = 0;

	private:
		// Memebers
		ComponentID m_id;
		World* m_world;
		std::vector<GameObject*> m_gameObjects;

		// Abstract Methods
		virtual void onObjectAdded(GameObject* entity) {}
		virtual void onObjectRemoved(GameObject* entity) {}
	};
}