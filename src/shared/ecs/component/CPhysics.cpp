#include "CPhysics.h"

#include <stdio.h>
#include <cmath>

#include <iostream>

namespace cellularz
{
	#pragma region Constructor / Destructor
	CPhysics::CPhysics() :
		Component(ComponentID::CPHYSICS),
		m_weight(0.0f),
		m_velocity(0.0f, 0.0f),
		m_gravity(true)
	{}
	#pragma endregion

	#pragma region Get / Set Methods
	const float& CPhysics::getWeight() { return m_weight; }
	void CPhysics::setWeight(const float& weigth) { m_weight = weigth; }

	const glm::vec2& CPhysics::getVelocity() { return m_velocity; }
	void CPhysics::setVelocity(const float& x, const float& y) { m_velocity.x = x; m_velocity.y = y; }
	void CPhysics::setVelocity(const glm::vec2& velocity) { m_velocity = velocity; }
	void CPhysics::addVelocity(const float& x, const float& y) { m_velocity.x += x; m_velocity.y += y; }
	void CPhysics::addVelocity(const glm::vec2& velocity) { m_velocity += velocity; }

	const bool& CPhysics::hasGravity() { return m_gravity; }
	void CPhysics::enableGravity() { m_gravity = true; }
	void CPhysics::disableGravity() { m_gravity = false; }
	#pragma endregion

	#pragma region Methods
	void CPhysics::applyCenterForce(const float& force, const float& angel)
	{
		float radians = angel * M_PI / 180.0f;
		float x = std::cos(radians);
		float y = std::sin(radians);

		addVelocity(x * force, y * force);
	}
	void CPhysics::applyCenterForce(const float& force, const float& angel, const float& forceLimit)
	{
		float radians = angel * M_PI / 180.0f;
		float x = std::cos(radians);
		float y = std::sin(radians);

		if (glm::length(m_velocity) > forceLimit) {
			addVelocity(x * force, y * force);
			glm::vec2 norm = glm::normalize(getVelocity());
			setVelocity(norm.x * forceLimit, norm.y * forceLimit);
		} else {
			addVelocity(x * force, y * force);
		}
	}
	#pragma endregion

	#pragma region Encode / Decode Methods
	void CPhysics::encodeData(Uint8* dataPtr, int& addressPtr)
	{
		std::memcpy(dataPtr + addressPtr, &m_weight, sizeof(float));
		addressPtr += sizeof(float);

		std::memcpy(dataPtr + addressPtr, &m_velocity.x, sizeof(float));
		addressPtr += sizeof(float);
		std::memcpy(dataPtr + addressPtr, &m_velocity.y, sizeof(float));
		addressPtr += sizeof(float);

		memcpy(dataPtr + addressPtr, &m_gravity, sizeof(bool));
		addressPtr += sizeof(bool);
	}

	void CPhysics::decodeData(Uint8* dataPtr, int& addressPtr)
	{
		std::memcpy(&m_weight, dataPtr + addressPtr, sizeof(float));
		addressPtr += sizeof(float);

		std::memcpy(&m_velocity.x, dataPtr + addressPtr, sizeof(float));
		addressPtr += sizeof(float);
		std::memcpy(&m_velocity.y, dataPtr + addressPtr, sizeof(float));
		addressPtr += sizeof(float);

		std::memcpy(&m_gravity, dataPtr + addressPtr, sizeof(bool));
		addressPtr += sizeof(bool);
	}
	#pragma endregion
}