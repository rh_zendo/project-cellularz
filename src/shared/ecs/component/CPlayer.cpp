#include "CPlayer.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	CPlayer::CPlayer() :
		#ifdef _BUILD_CLIENT
		m_camera(nullptr),
		#endif
		Component(ComponentID::CPLAYER),
		m_channelId(0),
		m_cameraLocked(false)
	{}

	#ifdef _BUILD_SERVER
	CPlayer::CPlayer(Uint8 channelId) :
		Component(ComponentID::CPLAYER),
		m_channelId(channelId),
		m_cameraLocked(false)
	{}
	#endif
	#pragma endregion


	#pragma region Get / Set Methods
	const Uint8& CPlayer::getChannelId() { return m_channelId; }

	const bool& CPlayer::isCameraLocked() { return m_cameraLocked; }
	void CPlayer::enableCameraLock() { m_cameraLocked = true; }
	void CPlayer::disableCameraLock() { m_cameraLocked = false; }

	#ifdef _BUILD_CLIENT
	Camera* CPlayer::getCamera() { return m_camera; }
	void CPlayer::setCamera(Camera* camera)
	{
		if (camera == nullptr) { disableCameraLock(); }
		m_camera = camera;
	}
	#endif
	#pragma endregion

	#pragma region Encode / Decode Methods
	void CPlayer::encodeData(Uint8* dataPtr, int& addressPtr)
	{
		memcpy(dataPtr + addressPtr, &m_channelId, sizeof(Uint8));
		addressPtr += sizeof(Uint8);

		memcpy(dataPtr + addressPtr, &m_cameraLocked, sizeof(bool));
		addressPtr += sizeof(bool);
	}

	void CPlayer::decodeData(Uint8* dataPtr, int& addressPtr)
	{
		memcpy(&m_channelId, dataPtr + addressPtr, sizeof(Uint8));
		addressPtr += sizeof(Uint8);

		memcpy(&m_cameraLocked, dataPtr + addressPtr, sizeof(bool));
		addressPtr += sizeof(bool);
	}
	#pragma endregion
}