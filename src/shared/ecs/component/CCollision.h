#pragma once

#include <SDL.h>

#include "Component.h"
#include "CollisionGroupID.h"

#include "Settings.h"

namespace cellularz
{
	using CollisionGroupBitMask = std::bitset<ECS_POOL_SIZE_COMPONENT_SYSTEM>;

	class CCollision : public Component
	{
	public:
		CCollision();

		// Get / Set Methods
		/// Gets the CollisionGroup mask form the component
		const CollisionGroupBitMask& getCollisionGroups();
		/// Checks if the object has the CollisionGroupID
		bool hasCollisionGroup(const CollisionGroupID& groupId);
		/// Enables a collision group
		void enableCollisionGroup(const CollisionGroupID& groupId);
		/// disables a collision group
		void disableCollisionGroup(const CollisionGroupID& groupId);

		const CollisionGroupBitMask& getCollisionResponseGroups();
		bool hasCollisionResponseGroup(const CollisionGroupID& groupId);
		void enableCollisionResponseGroup(const CollisionGroupID& groupId);
		void disableCollisionResponseGroup(const CollisionGroupID& groupId);

		// Encode / Decode Methods
		virtual void encodeData(Uint8* dataPtr, int& addressPtr) override;
		virtual void decodeData(Uint8* dataPtr, int& addressPtr) override;

	private:
		// Members
		CollisionGroupBitMask m_collisionGroups = { false };
		CollisionGroupBitMask m_collisionResponseGroups = { false };
	};
}