#include "CTransform.h"
#include "CTransform.h"
#include "CTransform.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	CTransform::CTransform() :
		Component(ComponentID::CTRANSFORM),
		m_position(0.0f, 0.0f),
		m_size(0.0f, 0.0f),
		m_rotation(0.0f)
	{}
	#pragma endregion

	#pragma region Get / Set Methods
	const glm::vec2& CTransform::getSize() { return m_size; }
	void CTransform::setSize(const float& width, const float& height)
	{
		m_size.x = width;
		m_size.y = height;
	}
	void CTransform::setSize(const glm::vec2& size) { m_size = size; }

	const glm::vec2& CTransform::getPosition() { return m_position; }
	void CTransform::setPosition(const float& x, const float& y)
	{
		m_position.x = x;
		m_position.y = y;
	}
	void CTransform::setPosition(const glm::vec2& position) { m_position = position; }
	void CTransform::addPosition(const float& x, const float& y)
	{
		m_position.x += x;
		m_position.y += y;
	}
	void CTransform::addPosition(const glm::vec2& position) { m_position += position; }

	glm::vec2 CTransform::getCenterPosition() { return m_position + (m_size / 2.0f); }
	void CTransform::setCenterPosition(const float& x, const float& y)
	{
		m_position.x = x - (m_size.x / 2.0f);
		m_position.y = y - (m_size.y / 2.0f);
	}
	void CTransform::setCenterPosition(const glm::vec2& position) 
	{
		m_position = position - (m_size / 2.0f);
	}

	const float& CTransform::getRotation() { return m_rotation; }
	void CTransform::setRotation(const float& angel) { m_rotation = angel; }
	void CTransform::addRotation(const float & angel) { m_rotation += angel; }
	#pragma endregion

	#pragma region Encode / Decode Methods
	void CTransform::encodeData(Uint8* dataPtr, int& addressPtr)
	{
		std::memcpy(dataPtr + addressPtr, &m_size.x, sizeof(float));
		addressPtr += sizeof(float);
		std::memcpy(dataPtr + addressPtr, &m_size.y, sizeof(float));
		addressPtr += sizeof(float);

		std::memcpy(dataPtr + addressPtr, &m_position.x, sizeof(float));
		addressPtr += sizeof(float);
		std::memcpy(dataPtr + addressPtr, &m_position.y, sizeof(float));
		addressPtr += sizeof(float);

		std::memcpy(dataPtr + addressPtr, &m_rotation, sizeof(float));
		addressPtr += sizeof(float);
	}

	void CTransform::decodeData(Uint8* dataPtr, int& addressPtr)
	{
		std::memcpy(&m_size.x, dataPtr + addressPtr, sizeof(float));
		addressPtr += sizeof(float);
		std::memcpy(&m_size.y, dataPtr + addressPtr, sizeof(float));
		addressPtr += sizeof(float);
		
		std::memcpy(&m_position.x, dataPtr + addressPtr, sizeof(float));
		addressPtr += sizeof(float);
		std::memcpy(&m_position.y, dataPtr + addressPtr, sizeof(float));
		addressPtr += sizeof(float);

		std::memcpy(&m_rotation, dataPtr + addressPtr, sizeof(float));
		addressPtr += sizeof(float);
	}
	#pragma endregion
}