#pragma once

#include <SDL.h>

#include "Component.h"

#ifdef _BUILD_CLIENT
#include "Camera.h"
#endif

namespace cellularz
{
	class CPlayer : public Component
	{
	public:
		CPlayer();

		#ifdef _BUILD_SERVER
		CPlayer(Uint8 channelId);
		#endif

		// Get / Set Methods
		/// Gets get channelId
		const Uint8& getChannelId();

		/// Checks if the camera is locked or not
		const bool& isCameraLocked();
		/// Enables the cam lock 
		void enableCameraLock();
		/// Disables the cam lock
		void disableCameraLock();

		#ifdef _BUILD_CLIENT
		/// Gets a pointer to the Camera 
		Camera* getCamera();
		/// Sets what camera the component should lock to, if set to nullptr it automaticly disables the cam lock
		void setCamera(Camera* camera);
		#endif


		// Encode / Decode Methods
		virtual void encodeData(Uint8* dataPtr, int& addressPtr) override;
		virtual void decodeData(Uint8* dataPtr, int& addressPtr) override;

	private:
		// Members
		Uint8 m_channelId;
		bool m_cameraLocked;

		#ifdef _BUILD_CLIENT
		Camera* m_camera;
		#endif

	};
}