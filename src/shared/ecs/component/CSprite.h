#pragma once

#include <SDL.h>

#include "Component.h"
#include "TextureID.h"

namespace cellularz
{
	class CSprite : public Component
	{
	public:
		CSprite();

		#ifdef _BUILD_CLIENT
		CSprite(SDL_Texture* texture);
		#endif

		// Get / Set Methods
		const TextureID& getTextureId();
		void setTextureId(const TextureID& textureId);

		#ifdef _BUILD_CLIENT
		SDL_Texture* getTexture();
		void setTexture(SDL_Texture* texture);
		#endif

		// Encode / Decode Methods
		virtual void encodeData(Uint8* dataPtr, int& addressPtr) override;
		virtual void decodeData(Uint8* dataPtr, int& addressPtr) override;
		virtual void postDecode(World* world) override;

	private:
		// Members
		TextureID m_textureId;

		#ifdef _BUILD_CLIENT
		SDL_Texture* m_texture;
		bool m_textureReload;
		#endif
	};
}