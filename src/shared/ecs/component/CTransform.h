#pragma once

#include <glm/glm.hpp>

#include "Component.h"

namespace cellularz
{
	class CTransform : public Component
	{
	public:
		CTransform();

		// Get / Set Methods
		const glm::vec2& getSize();
		void setSize(const float& width, const float& height);
		void setSize(const glm::vec2& size);

		const glm::vec2& getPosition();
		void setPosition(const float& x, const float& y);
		void setPosition(const glm::vec2& position);
		void addPosition(const float& x, const float& y);
		void addPosition(const glm::vec2& position);

		glm::vec2 getCenterPosition();
		void setCenterPosition(const float& x, const float& y);
		void setCenterPosition(const glm::vec2& position);

		const float& getRotation();
		void setRotation(const float& angel);
		void addRotation(const float& angel);

		// Encode / Decode Methods
		virtual void encodeData(Uint8* dataPtr, int& addressPtr) override;
		virtual void decodeData(Uint8* dataPtr, int& addressPtr) override;

	private:
		// Members
		glm::vec2 m_size;
		glm::vec2 m_position;
		float m_rotation;
	};
}