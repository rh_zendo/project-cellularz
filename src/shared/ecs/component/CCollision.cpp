#include "CCollision.h"

#include "ComponentID.h"

#include <SDL_net.h>

namespace cellularz
{
	#pragma region Constructor / Destructor
	CCollision::CCollision() :
		Component(ComponentID::CCOLLISON)
	{}
	#pragma endregion

	#pragma region Get / Set Methods
	const CollisionGroupBitMask& CCollision::getCollisionGroups() { return m_collisionGroups; }
	bool CCollision::hasCollisionGroup(const CollisionGroupID& groupId) { return m_collisionGroups[groupId]; }
	void CCollision::enableCollisionGroup(const CollisionGroupID& groupId) { m_collisionGroups[groupId] = true; }
	void CCollision::disableCollisionGroup(const CollisionGroupID& groupId) { m_collisionGroups[groupId] = false; }

	const CollisionGroupBitMask& CCollision::getCollisionResponseGroups() { return m_collisionResponseGroups; }
	bool CCollision::hasCollisionResponseGroup(const CollisionGroupID& groupId) { return m_collisionResponseGroups[groupId]; }
	void CCollision::enableCollisionResponseGroup(const CollisionGroupID& groupId) { m_collisionResponseGroups[groupId] = true; }
	void CCollision::disableCollisionResponseGroup(const CollisionGroupID& groupId) { m_collisionResponseGroups[groupId] = false; }
	#pragma endregion

	#pragma region Encode / Decode Methods
	void CCollision::encodeData(Uint8* dataPtr, int& addressPtr)
	{
		SDLNet_Write32((Uint32)m_collisionGroups.to_ulong(), dataPtr + addressPtr);
		addressPtr += sizeof(Uint32);

		SDLNet_Write32((Uint32)m_collisionResponseGroups.to_ulong(), dataPtr + addressPtr);
		addressPtr += sizeof(Uint32);
	}

	void CCollision::decodeData(Uint8* dataPtr, int& addressPtr)
	{
		m_collisionGroups = (CollisionGroupBitMask)SDLNet_Read32(dataPtr + addressPtr);
		addressPtr += sizeof(CollisionGroupBitMask);

		m_collisionResponseGroups = (CollisionGroupBitMask)SDLNet_Read32(dataPtr + addressPtr);
		addressPtr += sizeof(CollisionGroupBitMask);
	}
	#pragma endregion
}