#include "CSprite.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	CSprite::CSprite() :
	#ifdef _BUILD_CLIENT
		m_texture(nullptr),
		m_textureReload(true),
	#endif
		Component(ComponentID::CSPRITE)
	{}

	#ifdef _BUILD_CLIENT
	CSprite::CSprite(SDL_Texture* texture) :
		Component(ComponentID::CSPRITE),
		m_texture(texture),
		m_textureReload(true)
	{}
	#endif
	#pragma endregion

	#pragma region Get / Set Methods
	const TextureID& CSprite::getTextureId() { return m_textureId; }
	void CSprite::setTextureId(const TextureID& textureId) { m_textureId = textureId; }

	#ifdef _BUILD_CLIENT
	SDL_Texture* CSprite::getTexture() { return m_texture; }
	void CSprite::setTexture(SDL_Texture* texture) { m_texture = texture; }
	#endif
	#pragma endregion

	#pragma region Encode / Decode Methods
	void CSprite::encodeData(Uint8* dataPtr, int& addressPtr)
	{
		SDLNet_Write16((Uint16)m_textureId, dataPtr + addressPtr);
		addressPtr += sizeof(Uint16);
	}

	void CSprite::decodeData(Uint8* dataPtr, int& addressPtr)
	{
		TextureID tempId = (TextureID)SDLNet_Read16(dataPtr + addressPtr);
		addressPtr += sizeof(TextureID);

		#ifdef _BUILD_CLIENT
		// Checks if the texture has changed
		if (tempId != m_textureId) { m_textureReload = true; }
		#endif

		m_textureId = tempId;
	}

	void CSprite::postDecode(World* world)
	{
		#ifdef _BUILD_CLIENT
		// Checks if it should reload the texture
		if (m_textureReload) {
			m_texture = world->getClient()->getTextureManager()->getTexture(m_textureId);
		}
		#endif
	}

	#pragma endregion
}