#pragma once

#include <SDL.h>

#include <glm/glm.hpp>

#include "Component.h"

namespace cellularz
{
	class CPhysics : public Component
	{
	public:
		CPhysics();

		// Get / Set Methods
		const float& getWeight();
		void setWeight(const float& weigth);
		
		const glm::vec2& getVelocity();
		void setVelocity(const float& x, const float& y);
		void setVelocity(const glm::vec2& velocity);
		void addVelocity(const float& x, const float& y);
		void addVelocity(const glm::vec2& velocity);

		const bool& hasGravity();
		void enableGravity();
		void disableGravity();

		// Methods
		void applyCenterForce(const float& force, const float& angel);
		void applyCenterForce(const float& force, const float& angel, const float& forceLimit);

		// Encode / Decode Methods
		virtual void encodeData(Uint8* dataPtr, int& addressPtr) override;
		virtual void decodeData(Uint8* dataPtr, int& addressPtr) override;

	private:
		// Members
		float m_weight;
		glm::vec2 m_velocity;
		bool m_gravity;
	};
}