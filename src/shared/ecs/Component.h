#pragma once

#include <SDL.h>

#include "ComponentID.h"

#include "World.h"

namespace cellularz
{
	// Forward declaration
	class World;

	class Component
	{
	public:
		Component(const ComponentID& id) :
			m_id(id)
		{}
		virtual ~Component() {}

		// Methods
		const ComponentID& getId() { return m_id; }

		virtual void encodeData(Uint8* dataPtr, int& addressPtr) {}
		virtual void decodeData(Uint8* dataPtr, int& addressPtr) {}
		virtual void postDecode(World* world) {}

	private:
		// Members
		ComponentID m_id;
	};
}