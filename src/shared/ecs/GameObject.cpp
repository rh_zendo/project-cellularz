#include "GameObject.h"

#include "PacketUtils.h"
#include "PacketFactory.h"

// Components
#include "CCollision.h"
#include "CPhysics.h"
#include "CPlayer.h"
#include "CSprite.h"
#include "CTransform.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	GameObject::GameObject(World* world, const Uint32& id) :
		m_id(id),
		m_ownerId(0),
		m_classId(0),
		m_world(world),
		m_alive(true),
		m_nextNetworkUpdate(SDL_GetTicks())
	{}
	GameObject::~GameObject()
	{
		removeAllComponent();
	}
	#pragma endregion

	#pragma region Comparison operator
	bool GameObject::operator==(const GameObject &object) const
	{
		return m_id == object.m_id && object.m_world == m_world;
	}
	#pragma endregion

	#pragma region Get / Set Methods
	const Uint32& GameObject::getId() { return m_id; }

	const Uint32& GameObject::getOwnerId() { return m_ownerId; }
	void GameObject::setOwnerId(const Uint32& ownerId) { m_ownerId = ownerId; }

	const Uint16& GameObject::getClassId() { return m_classId; }
	void GameObject::setClassId(const Uint16& classId) { m_classId = classId; }

	const bool& GameObject::isAlive() { return m_alive; }
	void GameObject::kill()
	{
		m_alive = false;
		m_world->killGameObject(this);
	}

	const Uint32& GameObject::getNextNetworkUpdate() { return m_nextNetworkUpdate; }
	void GameObject::setNextNetworkUpdate(const Uint32& waitTime) { m_nextNetworkUpdate = waitTime + SDL_GetTicks(); }

	World* GameObject::getWorld() { return m_world; }
	#pragma endregion

	#pragma region Methods
	#ifdef _BUILD_CLIENT
	void GameObject::decodeUpdatePacket(UDPpacket* packet)
	{
		// Sets start address for the component data
		int addressPtr = PACKET_GAMEOBJECT_POINTER_COMPDATA;

		// Reads packet data
		m_ownerId = PacketUtils::decodeGameObjectPacketGetOwnerId(packet);
		m_classId = PacketUtils::decodeGameObjectPacketGetClassId(packet);
		ComponentBitMask compMask = PacketUtils::decodeGameObjectPacketGetCompMask(packet);

		// Checks if compmask has changed
		bool maskChanged = false;
		if (compMask != m_componentMask) { maskChanged = true; }

		// Looping through the comp mask 
		for (Uint8 i = 0; i < ComponentID::_TOTAL_ComponentID; i++) {

			bool compAdded = false;

			// Checks if the mask has changed
			if(maskChanged) {
				// Check if the component should be added or removed
				if (compMask[i] && !hasComponent(i)) {
					switch (i) {
						case ComponentID::CCOLLISON: addComponent<CCollision>(); break;
						case ComponentID::CPHYSICS: addComponent<CPhysics>(); break;
						case ComponentID::CPLAYER: addComponent<CPlayer>();	break;
						case ComponentID::CSPRITE: addComponent<CSprite>(); break;
						case ComponentID::CTRANSFORM: addComponent<CTransform>(); break;
					}
					compAdded = true;
				} else if(hasComponent(i)) {
					// Removes component
					removeComponent(i);

					// Triggers hook
					getWorld()->getClient()->onComponentRemovedByNetworking(this, (ComponentID)i);
				}
			}

			// Decodes data
			if (hasComponent(i)) {
				m_components[i]->decodeData(packet->data, addressPtr);
				m_components[i]->postDecode(getWorld());
			}

			// Triggers hook
			if (compAdded) {
				getWorld()->getClient()->onComponentAddedByNetworking(this, (ComponentID)i);
			}

		}

		// Sets component mask
		m_componentMask = compMask;
	}
	#endif

	#ifdef _BUILD_SERVER
	UDPpacket* GameObject::encodeUpdatePacket()
	{
		// Creates Packet
		UDPpacket* packet = PacketFactory::gameobject_update(m_id, m_ownerId, m_classId, m_componentMask.to_ulong());

		// Sets start address for the component data
		int addressPtr = PACKET_GAMEOBJECT_POINTER_COMPDATA;

		// Writes component data
		for (Uint8 i = 0; i < ComponentID::_TOTAL_ComponentID; i++) {
			if (!hasComponent(i)) { continue; }
			m_components[i]->encodeData(packet->data, addressPtr);
		}

		// Sets packet size to optimize for disabled components
		packet->len = addressPtr;

		// Sets when to send update next
		if(!hasComponent(ComponentID::CPLAYER)) {
			setNextNetworkUpdate(SERVER_GAMEOBJECT_NETUPDATE_DELAY);
		}

		return packet;
	}
	#endif
	#pragma endregion

	#pragma region Component Methods
	bool GameObject::hasComponent(const Uint8& id) { return m_componentMask[id]; }
	void GameObject::removeComponent(const Uint8& id)
	{
		// Checks if the system has the system, and then removes the GameObject from it
		if (m_world->hasSystem(id)) {
			m_world->getSystem(id)->removeGameObject(this);
		}

		// Removes the the component mask
		m_componentMask[id] = false;

		// Frees the memory from the component
		delete m_components[id];
	}

	void GameObject::removeAllComponent()
	{
		// Loops througe all components
		for (Uint8 i = 0; i < ComponentID::_TOTAL_ComponentID; i++) {
			if (hasComponent(i))
				removeComponent(i);
		}
	}
	#pragma endregion
}