#include "QuadTree.h"

#include <algorithm>

#include "CTransform.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	QuadTree::QuadTree(World* world)
	{
		m_treeRoot = new QuadTreeNode(this, glm::vec2(0, 0), world->getSize());
	}

	QuadTree::~QuadTree()
	{
		delete m_treeRoot;
	}
	#pragma endregion

	#pragma region Methods
	void QuadTree::initialize() { m_treeRoot->initialize(); }

	bool QuadTree::insertGameObject(GameObject* object)
	{
		CTransform* cTransform = object->getComponent<CTransform>(ComponentID::CTRANSFORM);
		return m_treeRoot->insertGameObject(object, cTransform);
	}

	void QuadTree::reset() {
		// Resets active nodes
		for (size_t i = 0; i < m_activeNodes.size(); ++i) {
			m_activeNodes[i]->reset();
		}

		// Resets inactive nodes
		for (size_t i = 0; i < m_inactiveNodes.size(); ++i) {
			m_inactiveNodes[i]->reset();
		}

		// Clear Active node cache
		m_activeNodes.clear();
		m_inactiveNodes.clear();
	}

	const std::vector<QuadTreeNode*>& QuadTree::getNodes() { return m_nodes; }
	void QuadTree::addNode(QuadTreeNode* node) { m_nodes.push_back(node); }
	void QuadTree::removeNode(QuadTreeNode* node) {	m_nodes.erase(std::remove(m_nodes.begin(), m_nodes.end(), node), m_nodes.end()); }

	const std::vector<QuadTreeNode*>& QuadTree::getActiveNodes() { return m_activeNodes; }
	void QuadTree::addActiveNode(QuadTreeNode* node) { m_activeNodes.push_back(node);}
	void QuadTree::removeActiveNode(QuadTreeNode* node) { m_activeNodes.erase(std::remove(m_activeNodes.begin(), m_activeNodes.end(), node), m_activeNodes.end()); }

	const std::vector<QuadTreeNode*>& QuadTree::getInactiveNodes() { return m_inactiveNodes; }
	void QuadTree::addInactiveNode(QuadTreeNode* node) { m_inactiveNodes.push_back(node); }
	void QuadTree::removeInactiveNode(QuadTreeNode* node) { m_inactiveNodes.erase(std::remove(m_inactiveNodes.begin(), m_inactiveNodes.end(), node), m_inactiveNodes.end()); }
	#pragma endregion
}