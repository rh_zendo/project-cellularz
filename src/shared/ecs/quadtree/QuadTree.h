#pragma once

#include <vector>

#include "QuadTreeNode.h"
#include "GameObject.h"
#include "World.h"

namespace cellularz
{	
	// Forward declaration
	class QuadTreeNode;

	class QuadTree
	{
	public:
		QuadTree(World* world);
		~QuadTree();

		// Methods
		/// Will initialize by generating the whole quadtree
		void initialize();

		/// Will try to add a game object to the quadtree
		bool insertGameObject(GameObject* object);

		/// Will reset all active and inactive nodes
		void reset();

		const std::vector<QuadTreeNode*>& getNodes();
		void addNode(QuadTreeNode* node);
		void removeNode(QuadTreeNode* node);

		const std::vector<QuadTreeNode*>& getActiveNodes();
		void addActiveNode(QuadTreeNode* node);
		void removeActiveNode(QuadTreeNode* node);

		const std::vector<QuadTreeNode*>& getInactiveNodes();
		void addInactiveNode(QuadTreeNode* node);
		void removeInactiveNode(QuadTreeNode* node);


	private:
		// Memebers
		QuadTreeNode* m_treeRoot;

		std::vector<QuadTreeNode*> m_nodes;
		std::vector<QuadTreeNode*> m_activeNodes;
		std::vector<QuadTreeNode*> m_inactiveNodes;
	};
}