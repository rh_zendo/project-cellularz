#pragma once

#include <glm/glm.hpp>
#include <vector>

#include "QuadTree.h"
#include "GameObject.h"

#include "CTransform.h"

namespace cellularz
{	
	// Forward declaration
	class QuadTree;

	class QuadTreeNode
	{
	public:
		QuadTreeNode(QuadTree* treeRoot, const glm::vec2& boundaryA, const glm::vec2& boundaryB, const Uint8& depth = 0);
		~QuadTreeNode();

		// Get / Set
		/// Gets the depth of the node
		const Uint8& getDepth();

		/// Checks if the node has divided
		const bool& hasDivided();

		const glm::vec2& getBoundaryA();
		const glm::vec2& getBoundaryB();

		/// Gets how many GameObject's the node contains
		const Uint16& getGameObjectCount();
		/// Gets all GameObject's the node contains
		const std::vector<GameObject*>& getGameObjects();

		// Methods
		/// Will initialize by generating sub nodes until the maximum depth has been reached 
		void initialize();

		/// Will try to add the GameObject to the node
		bool insertGameObject(GameObject* object, CTransform* cTransform);

		/// Will reset the node
		void reset();


	private:
		// Memebers
		Uint8 m_depth;
		bool m_divided;

		glm::vec2 m_boundaryA;
		glm::vec2 m_boundaryB;

		QuadTree* m_treeRoot;
		QuadTreeNode* m_nodeTopLeft;
		QuadTreeNode* m_nodeTopRight;
		QuadTreeNode* m_nodeBottomLeft;
		QuadTreeNode* m_nodeBottomRight;

		Uint16 m_gameObjectCount;
		std::vector<GameObject*> m_gameObjects;

		// Methods
		/// Checks if a CTransform is inside the nodes boundaries
		bool insideNode(CTransform* cTransform);

		/// Will insert the GameObject to sub nodes
		void insertGameObjectToSubNodes(GameObject* object, CTransform* cTransform);

		/// Will make sub nodes
		void growRoots();

		/// Will divide the node
		void divideNode();
	};
}