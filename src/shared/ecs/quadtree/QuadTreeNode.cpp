#include "QuadTreeNode.h"

#include "Utils.h"
#include "CTransform.h"

#include "Console.h"
#include "Settings.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	QuadTreeNode::QuadTreeNode(QuadTree* treeRoot, const glm::vec2& boundaryA, const glm::vec2& boundaryB, const Uint8& depth) :
		m_treeRoot(treeRoot),
		m_depth(depth),
		m_divided(false),
		m_boundaryA(boundaryA),
		m_boundaryB(boundaryB),
		m_nodeTopLeft(nullptr),
		m_nodeTopRight(nullptr),
		m_nodeBottomLeft(nullptr),
		m_nodeBottomRight(nullptr),
		m_gameObjectCount(0)
	{
		m_treeRoot->addNode(this);
	}

	QuadTreeNode::~QuadTreeNode()
	{
		// Cleaing heap memory
		delete m_nodeTopLeft;
		delete m_nodeTopRight;
		delete m_nodeBottomLeft;
		delete m_nodeBottomRight;
	}
	#pragma endregion

	#pragma region Get / Set Methods
	const Uint8& QuadTreeNode::getDepth() { return m_depth; }

	const bool& QuadTreeNode::hasDivided() { return m_divided; }

	const glm::vec2& QuadTreeNode::getBoundaryA() { return m_boundaryA; }
	const glm::vec2& QuadTreeNode::getBoundaryB() { return m_boundaryB; }

	const Uint16& QuadTreeNode::getGameObjectCount() { return m_gameObjectCount; }
	const std::vector<GameObject*>& QuadTreeNode::getGameObjects() { return m_gameObjects; }
	#pragma endregion

	#pragma region Methods
	void QuadTreeNode::initialize()
	{
		// Checks if tree has reached the maximum depth
		if (m_depth >= QUADTREE_DEPTH) { return; }

		// Grows roots
		growRoots();

		// Tells roots to initialize
		m_nodeTopLeft->initialize();
		m_nodeTopRight->initialize();
		m_nodeBottomLeft->initialize();
		m_nodeBottomRight->initialize();
	}

	bool QuadTreeNode::insertGameObject(GameObject* object, CTransform* cTransform)
	{
		// Making  AABB check
		if (!insideNode(cTransform)) { return false; }

		// Checks if node should divide
		if (!m_divided && m_depth < QUADTREE_DEPTH && m_gameObjectCount >= QUADTREE_DIVIDE_COUNT) {
			divideNode();
		}

		// Checks if node has divided
		if (!m_divided) {
			// Checks if node is active and activates it if it isnt
			if (!m_gameObjectCount) { m_treeRoot->addActiveNode(this); }

			m_gameObjectCount++;
			m_gameObjects.push_back(object);
			return true;
		}

		// Addes object to sub nodes
		insertGameObjectToSubNodes(object, cTransform);

		return true;
	}

	void QuadTreeNode::reset()
	{
		m_divided = false;
		m_gameObjectCount = 0;
		m_gameObjects.clear();
	}
	#pragma endregion

	#pragma region Private Methods
	bool QuadTreeNode::insideNode(CTransform* cTransform)
	{
		return Utils::collisionAABB(cTransform->getPosition(), cTransform->getPosition() + cTransform->getSize(), m_boundaryA, m_boundaryB);
	}

	void QuadTreeNode::insertGameObjectToSubNodes(GameObject* object, CTransform* cTransform)
	{
		m_nodeTopLeft->insertGameObject(object, cTransform);
		m_nodeTopRight->insertGameObject(object, cTransform);
		m_nodeBottomLeft->insertGameObject(object, cTransform);
		m_nodeBottomRight->insertGameObject(object, cTransform);
	}

	void QuadTreeNode::growRoots()
	{
		// Checks that roots arnt growen
		if (m_nodeTopLeft != nullptr) { return; }

		// Cals new roots depth
		int depth = m_depth + 1;

		// Cals the middel point of x and y
		float midX = (m_boundaryA.x + m_boundaryB.x) / 2.0f;
		float midY = (m_boundaryA.y + m_boundaryB.y) / 2.0f;

		// Grows new roots
		m_nodeTopLeft = new QuadTreeNode(m_treeRoot, m_boundaryA, glm::vec2(midX, midY), depth);
		m_nodeTopRight = new QuadTreeNode(m_treeRoot, glm::vec2(midX, m_boundaryA.y), glm::vec2(m_boundaryB.x, midY), depth);
		m_nodeBottomLeft = new QuadTreeNode(m_treeRoot, glm::vec2(m_boundaryA.x, midY), glm::vec2(midX, m_boundaryB.y), depth);
		m_nodeBottomRight = new QuadTreeNode(m_treeRoot, glm::vec2(midX, midY), m_boundaryB, depth);
	}

	void QuadTreeNode::divideNode()
	{
		// Sets node as inactive
		m_treeRoot->removeActiveNode(this);
		m_treeRoot->addInactiveNode(this);

		// Addes current objects to the sub nodes
		GameObject* object;
		CTransform* cTransform;
		for (std::size_t i = 0; i < m_gameObjects.size(); ++i) {
			object = m_gameObjects[i];
			cTransform = object->getComponent<CTransform>(ComponentID::CTRANSFORM);
			insertGameObjectToSubNodes(object, cTransform);
		}

		// Cleaning node up
		m_divided = true;
		m_gameObjects.clear();
	}
	#pragma endregion
}