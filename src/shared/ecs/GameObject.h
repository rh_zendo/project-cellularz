#pragma once

#include <vector>
#include <bitset>

#include <SDL_net.h>

#include "World.h"
#include "Component.h"

#include "Settings.h"

namespace cellularz
{
	// Forward declaration
	class World;
	class Component;

	using ComponentBitMask = std::bitset<ECS_POOL_SIZE_COMPONENT_SYSTEM>;

	class GameObject
	{
	public:
		// Constructor / Destructor
		GameObject(World* world, const Uint32& id);
		~GameObject();

		// Default copy/move ctors and assignment operators
		GameObject(const GameObject&) = default;
		GameObject(GameObject&&) = default;
		GameObject& operator=(const GameObject&) = default;
		GameObject& operator=(GameObject&&) = default;

		// Comparison operator
		bool operator==(const GameObject& object) const;
		bool operator!=(const GameObject& object) const { return !operator==(object); }

		// Get / Set Methods
		/// Gets the GameObject's id */
		const Uint32& getId();

		/// Gets the ownerId the object represents
		const Uint32& getOwnerId();
		/// Sets the ownerId for the gameObject */
		void setOwnerId(const Uint32& ownerId);

		/// Gets the classId the object represents
		const Uint16& getClassId();
		/// Sets the classId for the gameObject */
		void setClassId(const Uint16& classId);

		/// Is the GameObject is dead or alive
		const bool& isAlive();
		/// Will kill the GameObject, and set it as dead in the connected world
		void kill();

		/// Gets at what tick the next network update should be
		const Uint32& getNextNetworkUpdate();
		/// Sets the wait time before the next network update should be in ms
		void setNextNetworkUpdate(const Uint32& waitTime);

		/// Gets a pointer to the GameObject's connected world
		World* getWorld();

		// Methods
		#ifdef _BUILD_CLIENT
		void decodeUpdatePacket(UDPpacket* packet);
		#endif

		#ifdef _BUILD_SERVER
		UDPpacket* encodeUpdatePacket();
		#endif

		// Component Methods
		/// Checks if the GameObject has the specified ComponentID
		bool hasComponent(const Uint8& id);
		/// Removes the specified ComponentID from the GameObject
		void removeComponent(const Uint8& id);
		/// Removes all Component's from GameObject
		void removeAllComponent();

		/// Adds a Component and returns a casted pointer for the Component
		template <typename T, typename... TArgs>
		T* const addComponent(TArgs&&... args);
		/// Gets a casted Component pointer for specified ComponentID
		template <typename T>
		T* const getComponent(const Uint8& id);

	private:
		// Memebers
		Uint32 m_id;
		Uint32 m_ownerId;
		Uint16 m_classId;
		bool m_alive;

		Uint32 m_nextNetworkUpdate;

		Component* m_components[ECS_POOL_SIZE_COMPONENT_SYSTEM] = { nullptr };
		ComponentBitMask m_componentMask = { false };

		World* m_world;
	};

	template <typename T, typename... TArgs>
	T* const GameObject::addComponent(TArgs&&... args)
	{
		// Checks if the T is a Component and creates the component
		static_assert(std::is_base_of<Component, T>(), "Template argument does not inherit from Component");
		T* component(new T(std::forward<TArgs>(args)...));
		m_components[component->getId()] = component;
		m_componentMask[component->getId()] = true;
		
		// Checks if the word has the system
		if(m_world->hasSystem(component->getId())) {
			m_world->getSystem(component->getId())->addGameObject(this);
		}

		// Returns the new component
		return component;
	}

	template <typename T>
	T* const GameObject::getComponent(const Uint8& id)
	{
		static_assert(std::is_base_of<Component, T>(), "Template argument does not inherit from Component");
		return static_cast<T*>(m_components[id]);
	}
}