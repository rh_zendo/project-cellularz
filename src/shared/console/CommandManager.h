#pragma once

#include <vector>

#include "ICommand.h"
#include "Utils.h"
#include "Console.h"

namespace cellularz
{
	// Forward declaration
	class Console;

	class CommandManager
	{
	public:
		CommandManager();
		~CommandManager();

		// Methods
		template <typename T, typename... TArgs>
		void addCommand(TArgs&&... args);

		bool handel(std::string cmdStr);
		void displayHelp();

	private:
		// Members
		std::vector<ICommand*> m_commands;

	};

	template <typename T, typename... TArgs>
	void CommandManager::addCommand(TArgs&&... args) {
		T* command(new T(std::forward<TArgs>(args)...));
		m_commands.push_back(command);
	}
}