#include "Console.h"

#include <iostream>
#include <conio.h>

// Commands
#include "CommandShutdown.h"
#include "CommandStatus.h"

namespace cellularz
{
	#pragma region Singelton
	Console::Console() :
		m_initialized(false)
	{}
	#pragma endregion

	#pragma region Methods
	void Console::initialize(void* sdkPtr)
	{
		m_initialized = true;
		writeMessage("[Console] Initializing Server Console");

		// Creating command manager
		m_commandManager = new CommandManager();
		m_commandManager->addCommand<CommandShutdown>(sdkPtr);
		m_commandManager->addCommand<CommandStatus>(sdkPtr);

		// Starting Loop
		loop();
	}
	#pragma endregion

	#pragma region Private Methods
	void Console::loop()
	{
		char input;

		// Waiting for keypress
		while (input = _getch()) {
			// Check if hit enter
			if (input == '\r') {
				std::string msg = m_msgBuffer;
				m_msgBuffer.clear();

				handelCommand(msg);
				continue;
			}

			// Writes to console
			if (input == '\b') {
				// Removes a char from the command string
				if (m_msgBuffer.length() > 0) {
					printf("\b \b");
					m_msgBuffer.pop_back();
				}
			} else {
				// Adds a char to the command string
				printf("%c", input);
				m_msgBuffer += input;
			}
		}
	}

	void Console::writeMessage(std::string msg)
	{
		// Check if string is 0
		if (msg.length() <= 0) { return; }

		// Overwrite current command
		int clearLen = (int)m_msgBuffer.length() + 2 - (int)msg.length();
		if (clearLen > 0) {
			std::string clean(clearLen, ' ');
			msg.append(clean);
		}

		// Printing command
		if (m_initialized) {
			printf("\r%s\n\r$ %s", msg.c_str(), m_msgBuffer.c_str());
		}
		else {
			printf("%s\n\r", msg.c_str());
		}
	}

	void Console::handelCommand(std::string msg) {
		// Show Help
		if (msg == "help") { m_commandManager->displayHelp(); return; }

		// Handel Command
		if (m_commandManager->handel(msg)) {
			writeMessage("$ ");
		} else {
			writeMessage("[Console] Command not found, write \"help\" to get all commands");
		}
	}
	#pragma endregion

	#pragma region Static Methods
	void Console::print(std::string msg) { Console::getInstance().writeMessage(msg); }
	#pragma endregion
}