#include "CommandManager.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	CommandManager::CommandManager()
	{}

	CommandManager::~CommandManager()
	{}
	#pragma endregion


	#pragma region Methods
	bool CommandManager::handel(std::string cmdStr) {
		std::vector<std::string> cmdArgv = Utils::splitString(cmdStr, ' ');

		if (cmdArgv.size() <= 0) { return false; }

		for each (ICommand* command in m_commands) {
			if (command->GetName() == cmdArgv.at(0)) {
				command->Execute(cmdStr, cmdArgv);
				return true;
			}
		}

		return false;
	}

	void CommandManager::displayHelp() {
		Console::print("[Help] Here are all known commands and there function");

		for each (ICommand* cmd in m_commands) {
			if (cmd == nullptr) { continue; }
			Console::print("[Help] " + cmd->GetName() + ":	" + cmd->GetHelp());
		}
	}
	#pragma endregion
}