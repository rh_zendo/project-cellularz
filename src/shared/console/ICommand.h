#pragma once

#include <string>
#include <vector>

namespace cellularz
{
class ICommand
{
	public:
		ICommand(std::string cmdName, std::string cmdHelp) :
			m_cmdName(cmdName),
			m_cmdHelp(cmdHelp)
		{}
		virtual ~ICommand() {}

		// Abstract Methods
		virtual void Execute(std::string cmdStr, std::vector<std::string> cmdArgv) = 0;
		virtual std::string GetName() { return m_cmdName; }
		virtual std::string GetHelp() { return m_cmdHelp; }

	private:
		std::string m_cmdName;
		std::string m_cmdHelp;

	};
}