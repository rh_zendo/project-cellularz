#pragma once

#include "ICommand.h"

#ifdef _BUILD_CLIENT
#include "ClientSDK.h"
#endif

#ifdef _BUILD_SERVER
#include "ServerSDK.h"
#endif



namespace cellularz
{
	class CommandStatus : public ICommand
	{
	public:
		CommandStatus(void* sdkPtr) :
			ICommand("status", "Will diplay the status of the current system"),
			m_sdkPtr(sdkPtr)
		{};

		void Execute(std::string cmdStr, std::vector<std::string> cmdArgv)
		{

		}

	private:
		void* m_sdkPtr;
	};
}