#pragma once

#include "ICommand.h"

#ifdef _BUILD_CLIENT
#include "ClientSDK.h"
#endif

#ifdef _BUILD_SERVER
#include "ServerSDK.h"
#endif

namespace cellularz
{
	class CommandShutdown : public ICommand
	{
	public:
		CommandShutdown(void* sdkPtr) :
			ICommand("exit", "Will shutdown the system"),
			m_sdkPtr(sdkPtr)
		{};

		void Execute(std::string cmdStr, std::vector<std::string> cmdArgv)
		{
			if (m_sdkPtr != nullptr) {
				#ifdef _BUILD_CLIENT
				((ClientSDK*)m_sdkPtr)->stopClient();
				#endif

				#ifdef _BUILD_SERVER
				((ServerSDK*)m_sdkPtr)->stopServer();
				#endif
			}
		}

	private:
		void* m_sdkPtr;
	};
}