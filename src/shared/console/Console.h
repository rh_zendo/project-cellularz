#pragma once

#include <string>

#include "CommandManager.h"

namespace cellularz
{
	// Forward declaration
	class CommandManager;

	class Console
	{
	public:
		// Singelton
		static Console& getInstance() {
			static Console instance;
			return instance;
		}

		// Methods
		void initialize(void* sdkPtr);

		// Static Methods
		static void print(std::string msg);

	private:
		// Singelton
		Console();
		Console(Console const&) = delete;
		void operator=(Console const&) = delete;

		// Members
		bool m_initialized;
		std::string m_msgBuffer;

		CommandManager* m_commandManager;

		// Methods
		void loop();
		void writeMessage(std::string msg);
		void handelCommand(std::string msg);
	};
}