#pragma once
/*
	Debugging
*/
//#define DEBUG_PREF_COUNTERS

//#define DEBUG_QUADTREE_INFO
//#define DEBUG_QUADTREE_RENDERNODES

#define DEBUG_AABB_RENDER

/*
	Shared
*/
	// General
	#define BUILD_VERSION "v0.2.2 - pre alpha"

	#define TICKLIMITER_SAMPLECOUNT 10

	// GameObject Component System
	#define ECS_WORLD_START_ID 0 /**< What number the game object should start at */
	#define ECS_POOL_SIZE_COMPONENT_SYSTEM 32 /**< The maximum componets the engine can handel (Limited to 4 bytes atm) */

		// QuadTree
		#define QUADTREE_DEPTH 7 /**< The limit of how deep a quad tree can sub divide */
		#define QUADTREE_DIVIDE_COUNT 4 /**< The capacity limit of a quad tree node */

	// Networking
		// Ack Manager
		#define ACKMANAGER_ENABLED_ACKID 0x0000
		#define ACKMANAGER_DISABLE_ACKID 0xFFFF

		#define ACKMANAGER_START_ACKID 0x0001

		#define ACKMANAGER_RESEND_LIMIT 8 /**< How many times a packet will be resend before removing it */
		#define ACKMANAGER_RESEND_DELAY (Uint32)(1000.0f / 5.0f) /**< How long in ms a packet will wait in the ack buffer before resending */

		// Channel Manager
		#define CHANNELMANAGER_BROADCAST_CHANNEL 255 /**< Sets what channelId to use as a "broadcast channel" */
		#define CHANNELMANAGER_DISCONNECTED_TIME 2 * 1000 /**< How many ms the ChannelPool will wait before declaying a channel for disconnected */
		#define CHANNELMANAGER_DISCONNECT_TIME 30 * 1000 /**< How many ms the ChannelPool will wait before disconnecting a channel */
		#define CHANNELMANAGER_LIMIT SDLNET_MAX_UDPCHANNELS /**< How many channels the ChannelPool can contain */

		// Packet Header
		#define PACKET_SIZE_PROTOCALID (unsigned int)(sizeof(Uint16)) /**< This is size of the protocalId field in the packet header */
		#define PACKET_SIZE_ACKID (unsigned int)(sizeof(Uint16)) /**< This is size of the ackId field in the packet header */
		#define PACKET_SIZE_DATA (unsigned int)(512 - PACKET_SIZE_PROTOCALID - PACKET_SIZE_ACKID) /**< This is size of the data field in the header */

		#define PACKET_SIZE_HEADERS (unsigned int)(PACKET_SIZE_PROTOCALID + PACKET_SIZE_ACKID) /**< This is the total size of the header */
		#define PACKET_SIZE (unsigned int)(PACKET_SIZE_HEADERS + PACKET_SIZE_DATA) /**< This is the maximum size a packet can be */

		#define PACKET_POINTER_PROTOCALID 0 /**< This is where the pointer should start to read the protocalId from a packet */
		#define PACKET_POINTER_ACKID PACKET_POINTER_PROTOCALID + PACKET_SIZE_PROTOCALID /**< This is where the pointer should start to read the ackId from a packet */
		#define PACKET_POINTER_DATA PACKET_POINTER_ACKID + PACKET_SIZE_ACKID /**< This is where the pointer should start to read the data from a packet */

		// Ack Packets
		#define PACKET_ACK_SIZE (unsigned int)(sizeof(Uint16))

		// Game Object Packets
			// Components Sizes
			#define COMPONENT_SIZE_CCOLLISION (unsigned int)(sizeof(Uint32) * 2)
			#define COMPONENT_SIZE_CPLAYER (unsigned int)(sizeof(Uint8) + sizeof(bool))
			#define COMPONENT_SIZE_CPHYSICS (unsigned int)(sizeof(float) * 3 + sizeof(bool))
			#define COMPONENT_SIZE_CSPRITE (unsigned int)(sizeof(Uint16))
			#define COMPONENT_SIZE_CTRANSFORM (unsigned int)(sizeof(float) * 5)
			#define COMPONENT_SIZE_LIMIT (unsigned int)(COMPONENT_SIZE_CCOLLISION + COMPONENT_SIZE_CPLAYER + COMPONENT_SIZE_CPHYSICS + COMPONENT_SIZE_CSPRITE + COMPONENT_SIZE_CTRANSFORM)

			// Sizes
			#define PACKET_GAMEOBJECT_SIZE_ID (unsigned int)(sizeof(Uint32))
			#define PACKET_GAMEOBJECT_SIZE_OWNERID (unsigned int)(sizeof(Uint32))
			#define PACKET_GAMEOBJECT_SIZE_CLASSID (unsigned int)(sizeof(Uint16))
			#define PACKET_GAMEOBJECT_SIZE_COMPMASK (unsigned int)(sizeof(Uint32))

			// Packet Size / Packet Structurccc
			#define PACKET_GAMEOBJECT_UPDATE_SIZE (unsigned int)(PACKET_GAMEOBJECT_SIZE_ID + PACKET_GAMEOBJECT_SIZE_OWNERID + PACKET_GAMEOBJECT_SIZE_CLASSID + PACKET_GAMEOBJECT_SIZE_COMPMASK + COMPONENT_SIZE_LIMIT)
			#define PACKET_GAMEOBJECT_KILL_SIZE (unsigned int)(PACKET_GAMEOBJECT_SIZE_ID)

			// Pointers
			#define PACKET_GAMEOBJECT_POINTER_ID PACKET_POINTER_DATA
			#define PACKET_GAMEOBJECT_POINTER_OWNERID PACKET_GAMEOBJECT_POINTER_ID + PACKET_GAMEOBJECT_SIZE_ID
			#define PACKET_GAMEOBJECT_POINTER_CLASSID PACKET_GAMEOBJECT_POINTER_OWNERID + PACKET_GAMEOBJECT_SIZE_OWNERID
			#define PACKET_GAMEOBJECT_POINTER_COMPMASK PACKET_GAMEOBJECT_POINTER_CLASSID + PACKET_GAMEOBJECT_SIZE_CLASSID
			#define PACKET_GAMEOBJECT_POINTER_COMPDATA PACKET_GAMEOBJECT_POINTER_COMPMASK + PACKET_GAMEOBJECT_SIZE_COMPMASK

		// Client Packets
		#define PACKET_CLIENT_SERVERCHANNEL_SIZE_CHANNELID (unsigned int)(sizeof(Uint8))
		#define PACKET_CLIENT_SERVERCHANNEL_SIZE (unsigned int)(PACKET_CLIENT_SERVERCHANNEL_SIZE_CHANNELID)
		#define PACKET_CLIENT_SERVERCHANNEL_POINTER_CHANNELID PACKET_POINTER_DATA

		#define PACKET_CLIENT_INPUTEVENT_SIZE_EVENTID (unsigned int)(sizeof(Uint16))
		#define PACKET_CLIENT_INPUTEVENT_SIZE (unsigned int)(PACKET_CLIENT_INPUTEVENT_SIZE_EVENTID)
		#define PACKET_CLIENT_INPUTEVENT_POINTER_EVENTID PACKET_POINTER_DATA
		#define PACKET_CLIENT_INPUTEVENT_POINTER_DATA PACKET_CLIENT_INPUTEVENT_POINTER_EVENTID + PACKET_CLIENT_INPUTEVENT_SIZE_EVENTID

		// Sound Packets
		#define PACKET_SOUND_PLAY_SIZE_AUDIOID (unsigned int)(sizeof(Uint16))
		#define PACKET_SOUND_PLAY_SIZE (unsigned int)(PACKET_SOUND_PLAY_SIZE_AUDIOID)
		#define PACKET_SOUND_PLAY_POINTER_AUDIOID PACKET_POINTER_DATA
	

/*
	Client
*/
	// Tickrate
	#define CLIENT_TICKRATE 128.0f /**< Will set how fast the client will try to update */
	#define CLIENTNETWORK_TICKRATE 66.0f /**< Will set how fast the clients network thread will try to update */

	// Window
	#define WINDOW_WIDTH 1366
	#define WINDOW_HEIGHT 768

	// Config
	#define CONFIG_COMPANY "CellularZ"
	#define CONFIG_GAME "CellularZ Engine"
/*
	Server
*/
	// Tickrate
	#define SERVER_TICKRATE 128.0f /**< Will set how fast the server will try to update */
	#define SERVERNETWORK_TICKRATE CLIENTNETWORK_TICKRATE /**< Will set how fast the servers network thread will try to update */

	// Game Object
	#define SERVER_GAMEOBJECT_NETUPDATE_DELAY (Uint32)(1000.0f / 5.0f) /**< Will set a delay in ms, that the gameobjet will wait before sending a new update packet */
