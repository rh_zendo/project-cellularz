#pragma once

#include <vector>
#include <string>
#include <glm/glm.hpp>

#include "Settings.h"

namespace cellularz
{
	class Utils
	{
	public:
		// Time
		static long long getCurrentMill();
		static long long getCurrentMicro();

		// Random
		static float randomFloat(float a, float b);

		// String
		std::vector<std::string> static splitString(const std::string& str, const char& ch);

		// Convert
		template <typename T>
		static T bytesToInt(unsigned char* bytes, unsigned char size, unsigned int startPtr = 0, bool flip = false);

		static std::string bytesToString(unsigned char* dataPtr, unsigned int startPtr, unsigned int size);

		// Collision Detection
		#ifdef DEBUG_PREF_COUNTERS
		static void pref_AABBChecksReset();
		static int const pref_AABBChecksCount();
		#endif

		static bool collisionAABB(const glm::vec2& A1, const glm::vec2& A2, const glm::vec2& B1, const glm::vec2& B2);
	};

	#pragma region Convert
	template <typename T>
	static T Utils::bytesToInt(unsigned char* bytes, unsigned char size, unsigned int startPtr, bool flip)
	{
		T result = 0;

		if (!flip) {
			unsigned int bitCount = size - 1;
			for (unsigned int i = startPtr; i < size; i++) {
				result += bytes[i] << (8 * bitCount);
				bitCount--;
			}
		} else {
			for (unsigned int i = startPtr; i > size; i++) {
				result += bytes[i] << (8 * i);
			}
		}

		return result;
	}
	#pragma endregion
}