#include "Config.h"
#include "SDL.h"
#include "Utils.h"
#include "CommandManager.h"
#include <iostream>
#include <sstream>

namespace cellularz
{
	std::string pref_path;
	std::string fileContent;

#pragma region Public methods
	void Config::initializePrefPath()
	{		
		char *base_path = SDL_GetPrefPath(CONFIG_COMPANY, CONFIG_GAME);
		if (base_path) {
			pref_path = std::string(base_path) += "config.conf";
		}
		else {

		}
	}

	void Config::loadConfig()
	{
		// Get the file of the preffered path
		SDL_RWops *file = SDL_RWFromFile(pref_path.c_str(), "r+");

		// If there is no file we create one with default values
		if (file == NULL) {
			file = SDL_RWFromFile(pref_path.c_str(), "w+");

			std::string str = "WIDTH=1366\r\n";
			str += "HEIGHT=768\r\n";

			// Default key bindings
			str += "MOVE_UP=1073741906\r\n";
			str += "MOVE_DOWN=1073741905\r\n";
			str += "MOVE_LEFT=1073741904\r\n";
			str += "MOVE_RIGHT=1073741903\r\n";

			size_t len = SDL_strlen(str.c_str());

			if (SDL_RWwrite(file, str.c_str(), 1, len) != len) {
				Console::print("[Error] Couldn't write to file");
			}
			else {
				// Close and save the file, then open it out again
				SDL_RWclose(file);
				SDL_RWops *file = SDL_RWFromFile(pref_path.c_str(), "r+");
			}
		}
		
		// We now read the size of the file and get out each char into a string locally
		Sint64 res_size = SDL_RWsize(file);
		char* res = (char*)malloc(res_size + 1);

		Sint64 nb_read_total = 0, nb_read = 1;
		char* buf = res;
		while (nb_read_total < res_size && nb_read != 0) {
			nb_read = SDL_RWread(file, buf, 1, (res_size - nb_read_total));
			nb_read_total += nb_read;
			buf += nb_read;
		}
		SDL_RWclose(file);
		if (nb_read_total != res_size) {
			free(res);
		}

		res[nb_read_total] = '\0';

		fileContent = res;
	}

	SDL_Keycode Config::getKeyValue(std::string key)
	{
		if (!isKeyExist(key))
			return NULL;

		std::istringstream ss(fileContent);
		std::string line;

		// Run through each line of the file (Each \r\n)
		while (std::getline(ss, line))
		{
			std::istringstream is_line(line);
			std::string confKey;
			// Get the key in the file and check if it fits with the key we want
			if (std::getline(is_line, confKey, '='))
			{
				if (confKey != key)
					continue;

				// Get the value of the found key and return it without line break
				std::string value;
				std::getline(is_line, value);
				if (!value.empty() && value[value.size() - 1] == '\r')
					value.erase(value.size() - 1);

				return intToSDLKey(std::stoi(value));
			}
		}

		return NULL;
	}

	void Config::updateConfig(std::string key, std::string value)
	{
		if (!isKeyExist(key)) {
			fileContent += key + "=" + value + "\r\n";
			SDL_RWops *file = SDL_RWFromFile(pref_path.c_str(), "w+");
			if (file != NULL) {
				size_t len = SDL_strlen(fileContent.c_str());
				if (SDL_RWwrite(file, fileContent.c_str(), 1, len) != len) {
					Console::print("[Config] Couldn't fully write string\n");
				}

				SDL_RWclose(file);

				loadConfig();
			}
		}
		else {
			std::istringstream ss(fileContent);
			std::string line;
			// Run through each line of the file (Each \r\n)
			while (std::getline(ss, line))
			{
				std::istringstream is_line(line);
				std::string confKey;
				// Get the key in the file and check if it fits with the key we want
				if (std::getline(is_line, confKey, '='))
				{
					if (confKey == key) {
						
						size_t pos = fileContent.find(line);
						fileContent.replace(pos, SDL_strlen(line.c_str()), key + "=" + value + "\r\n");

						SDL_RWops *file = SDL_RWFromFile(pref_path.c_str(), "w+");
						size_t len = SDL_strlen(fileContent.c_str());

						if (SDL_RWwrite(file, fileContent.c_str(), 1, len) != len) {
							Console::print("[Config] Couldn't fully write string\n");
						}

						SDL_RWclose(file);
					}
				}
			}
		}
	}
#pragma endregion
#pragma region Private methods
	bool Config::isKeyExist(std::string key)
	{
		if (fileContent.empty())
			return false;

		std::size_t found = fileContent.find(key);
		if (found != std::string::npos)
			return true;

		return false;
	}

	SDL_Keycode Config::intToSDLKey(int value)
	{
		SDL_Keycode key = value;
		return key;
	}
#pragma endregion
}