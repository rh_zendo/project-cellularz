#pragma once

#include "KeyEvent.h"
#include <string>
namespace cellularz
{
	class Config
	{
	public:
		void initializePrefPath();
		void loadConfig();
		
		SDL_Keycode getKeyValue(std::string key);
		void updateConfig(std::string key, std::string value);
	private:
		bool isKeyExist(std::string key);
		SDL_Keycode intToSDLKey(int value);
	};

}