#pragma once

#include <SDL.h>

#include "Settings.h"

namespace cellularz
{
	class TickLimiter
	{
	public:
		TickLimiter(const float& maxTPS);

		// Get / Set Methods
		const float& getCurrentTPS();

		// Methods
		void setMaxTPS(const float& maxTPS);

		void begin();
		void end(bool delayThread = true);

	private:
		// Calculates the current FPS
		void calculateFPS();

		// Members
		float m_tps;
		float m_maxTPS;
		float m_frameTime;
		unsigned int m_startTicks;

		float m_frameTimes[TICKLIMITER_SAMPLECOUNT];
		int m_currentFrame;
		Uint32 m_prevTicks;
	};
}