#include "TickLimiter.h"

namespace cellularz
{
	#pragma region Constructor / Destructor
	TickLimiter::TickLimiter(const float& maxTPS) :
		m_maxTPS(maxTPS),
		m_currentFrame(0),
		m_prevTicks(SDL_GetTicks())
	{}
	#pragma endregion

	#pragma region Get / Set Methods
	const float& TickLimiter::getCurrentTPS() { return m_tps; }
	#pragma endregion

	#pragma region Methods
	void TickLimiter::setMaxTPS(const float& maxTPS) {	m_maxTPS = maxTPS; }

	void TickLimiter::begin() {	m_startTicks = SDL_GetTicks(); }
	void TickLimiter::end(bool delayThread) {
		calculateFPS();

		if (!delayThread) { return; }

		// Checks if the thead should be delayed
		float frameTicks = (float)(SDL_GetTicks() - m_startTicks);
		if (1000.0f / m_maxTPS > frameTicks) {
			SDL_Delay((Uint32)(1000.0f / m_maxTPS - frameTicks));
		}
	}

	void TickLimiter::calculateFPS() {
		// Ticks for the current frame
		Uint32 currentTicks = SDL_GetTicks();

		// Calculate the number of ticks (ms) for this frame
		m_frameTime = (float)(currentTicks - m_prevTicks);
		m_frameTimes[m_currentFrame % TICKLIMITER_SAMPLECOUNT] = m_frameTime;

		// Current ticks is now previous ticks
		m_prevTicks = currentTicks;

		// The number of frames to average
		int count;

		m_currentFrame++;
		if (m_currentFrame < TICKLIMITER_SAMPLECOUNT) {
			count = m_currentFrame;
		} else {
			count = TICKLIMITER_SAMPLECOUNT;
		}

		// Average all the frame times
		float frameTimeAverage = 0;
		for (int i = 0; i < count; i++) {
			frameTimeAverage += m_frameTimes[i];
		}
		frameTimeAverage /= count;

		// Calculate FPS
		if (frameTimeAverage > 0) {
			m_tps = 1000.0f / frameTimeAverage;
		} else {
			m_tps = 60.0f;
		}
	}
	#pragma endregion
}