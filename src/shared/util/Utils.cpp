#include "Utils.h"

#include <iostream>
#include <chrono>

#include <sstream>

namespace cellularz
{
	#pragma region Time
	long long Utils::getCurrentMill()
	{
		long long ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();	// Black magic, enough Said
		return ms;
	}

	long long Utils::getCurrentMicro()
	{
		long long mc = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();	// Black magic, enough Said
		return mc;
	}
	#pragma endregion

	#pragma region Random
	float Utils::randomFloat(float a, float b) {
		float random = ((float)rand()) / (float)RAND_MAX;
		float diff = b - a;
		float r = random * diff;
		return a + r;
	}
	#pragma endregion


	#pragma region String
	std::vector<std::string> Utils::splitString(const std::string& str, const char& ch)
	{
		std::string next;
		std::vector<std::string> result;

		// For each character in the string
		for (std::string::const_iterator it = str.begin(); it != str.end(); it++) {
			// If we've hit the terminal character
			if (*it == ch) {
				// If we have some characters accumulated
				if (!next.empty()) {
					// Add them to the result vector
					result.push_back(next);
					next.clear();
				}
			}
			else {
				// Accumulate the next character into the sequence
				next += *it;
			}
		}

		if (!next.empty())
			result.push_back(next);

		return result;
	}
	#pragma endregion

	#pragma region Convert
	std::string Utils::bytesToString(unsigned char* dataPtr, unsigned int startPtr, unsigned int size)
	{
		std::string result;

		for (unsigned int i = startPtr; i < (startPtr + size); i++) {
			result.push_back(dataPtr[i]);
		}

		return result;
	}
	#pragma endregion

	#pragma region Collision Detection

	#ifdef DEBUG_PREF_COUNTERS
	static int pref_AABBChecks;
	void Utils::pref_AABBChecksReset() { pref_AABBChecks = 0; }
	int const Utils::pref_AABBChecksCount() { return pref_AABBChecks; }
	#endif

	// Checks if object that starts at A1 and ends at A2 is at least partially inside object that starts at B1 and ends at B2
	bool Utils::collisionAABB(const glm::vec2& A1, const glm::vec2& A2, const glm::vec2& B1, const glm::vec2& B2)
	{
		#ifdef DEBUG_PREF_COUNTERS
		pref_AABBChecks++;
		#endif

		if (A1.y >= B2.y)
			return false;
		if (A1.x >= B2.x)
			return false;
		if (A2.y <= B1.y)
			return false;
		if (A2.x <= B1.x)
			return false;

		return true;
	}
	#pragma endregion

}