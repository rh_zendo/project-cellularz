#pragma once

#include "world.h"

#include "EventID.h"

using namespace cellularz;

class GameUtils
{
	public:
		static void createWorldBorder(World* world);

		// Create specific
		static void createPlayerObject(World* world, const Uint8& channelId);
		static GameObject* createCarePackage(World* world, float positionX, float positionY);
		static void createBullet(World* world, GameObject* playerObject, glm::vec2 mouseLocation);
		static float getTrueVelocity(float xVelocity, float yVelocity);
		static float getTrueVelocity(glm::vec2 velocity);

		static void createPlayerObject(World* world, const int& channelId);
	private:
};