#ifdef _BUILD_CLIENT
#include "GameDemoClient.h"

#include "PacketFactory.h"
#include "EventID.h"
#include "ObjectClassID.h"


// Systems
#include "SystemCollision.h"
#include "SystemPlayer.h"
#include "SystemPhysics.h"
#include "SystemSprite.h"

// Components
#include "CCollision.h"
#include "CPlayer.h"
#include "CPhysics.h"
#include "CSprite.h"
#include "CTransform.h"

#pragma region Constructor / Destructor
GameDemoClient::GameDemoClient() :
	m_playerGameObject(nullptr)
{}

GameDemoClient::~GameDemoClient()
{}
#pragma endregion

#pragma region Initialiation Hooks
void GameDemoClient::onInitializeResources(AudioManager* audioManager, TextureManager* textureManager)
{
	// Textures
	textureManager->loadTexture(TextureID::DEBUG, "../../../resources/textures/debug.png");
	textureManager->loadTexture(TextureID::ROCKET, "../../../resources/textures/rocket_v1.png");
	textureManager->loadTexture(TextureID::WALL, "../../../resources/textures/wall.png");
	textureManager->loadTexture(TextureID::CAREPACKAGE, "../../../resources/textures/ammo.png");
	textureManager->loadTexture(TextureID::BULLET, "../../../resources/textures/debug.png");

	// Audio
	audioManager->initializeAudio(AudioID::BACKGROUND, "../../../resources/audio/intro.wav");
	audioManager->initializeAudio(AudioID::SHOOTING, "../../../resources/audio/projectile_shoot.wav");
}

void GameDemoClient::onInitializeWorld(World* world, Config* config)
{
	// SYSTEMS ONLY cant load textures
	world->setSize(2048, 2048);
	world->addSystem<SystemCollision>();
	world->addSystem<SystemPlayer>();
	world->addSystem<SystemPhysics>();
	world->addSystem<SystemSprite>();

	// Debug Config system
	config->initializePrefPath();
	config->loadConfig();
}
#pragma endregion

#pragma region Hooks
void GameDemoClient::onTick(const double& deltaTime)
{
	// Player Events
	if (getInputManager()->isKeyDown(getConfig()->getKeyValue("MOVE_UP")))
		getNetwork()->addOutputPacketToQueue(PacketFactory::client_inputevent(EventID::PLAYER_MOVE_UP));
	if (getInputManager()->isKeyDown(getConfig()->getKeyValue("MOVE_DOWN")))
		getNetwork()->addOutputPacketToQueue(PacketFactory::client_inputevent(EventID::PLAYER_MOVE_DOWN));
	if (getInputManager()->isKeyDown(getConfig()->getKeyValue("MOVE_RIGHT")))
		getNetwork()->addOutputPacketToQueue(PacketFactory::client_inputevent(EventID::PLAYER_MOVE_RIGHT));
	if (getInputManager()->isKeyDown(getConfig()->getKeyValue("MOVE_LEFT")))
		getNetwork()->addOutputPacketToQueue(PacketFactory::client_inputevent(EventID::PLAYER_MOVE_LEFT));

	if (getInputManager()->isMouseDown(SDL_BUTTON_LEFT)) {
		// Converts screen pos to world pos
		glm::vec2 worldPos = getCamara()->convertScreenToWorld(getInputManager()->getMousePos());

		// Creates Packet
		int addressPtr = PACKET_CLIENT_INPUTEVENT_POINTER_DATA;
		UDPpacket* packet = PacketFactory::client_inputevent(EventID::PLAYER_MOUSE_LEFT, sizeof(float) * 2);

		// Adds world pos to packet
		std::memcpy(packet->data + addressPtr, &worldPos.x, sizeof(float));
		addressPtr += sizeof(float);
		std::memcpy(packet->data + addressPtr, &worldPos.y, sizeof(float));
		addressPtr += sizeof(float);

		// Adds packet to output quere
		getNetwork()->addOutputPacketToQueue(packet);
	}
}

void GameDemoClient::onGameObjectCollision(GameObject* objectA, GameObject* objectB)
{}

void GameDemoClient::onGameObjectCollisionResponse(GameObject* object, AxisID axis)
{
	CPhysics* cPhysics = object->getComponent<CPhysics>(ComponentID::CPHYSICS);

	if(axis == AxisID::X) {
		cPhysics->setVelocity(-cPhysics->getVelocity().x, cPhysics->getVelocity().y);
	}

	if(axis == AxisID::Y) {
		cPhysics->setVelocity(cPhysics->getVelocity().x, -cPhysics->getVelocity().y);
	}
}

void GameDemoClient::onGameObjectOutOfWorld(GameObject* object)
{
	// Checks if not player component
	if (!object->hasComponent(ComponentID::CPLAYER))
		object->kill();
}
#pragma endregion

#pragma region Connection Hooks
void GameDemoClient::onServerConnect()
{

	Console::print("Client has connected to the server on channel: " + std::to_string(getServerChannelId()));
}

void GameDemoClient::onServerConnectChannelId(int channelId)
{
	SystemPlayer* systemPlayer = getWorld()->getSystem<SystemPlayer>(ComponentID::CPLAYER);
	GameObject* object = systemPlayer->getGameObjectByChannelId(channelId);
	if (object == nullptr) { return; }

	Console::print("OwnerId:" + std::to_string(object->getOwnerId()));

	CPlayer* cPLayer = object->getComponent<CPlayer>(ComponentID::CPLAYER);
	if (cPLayer->isCameraLocked() && getServerChannelId() == cPLayer->getChannelId()) {
		cPLayer->setCamera(getCamara());
	}

}

void GameDemoClient::onServerConnectFailed()
{
	Console::print("Client failed to connect to the server");
}
#pragma endregion

#pragma region Component Hooks
void GameDemoClient::onComponentAddedByNetworking(GameObject* object, ComponentID compId)
{
	switch (compId) {
		// Adds camera pointer
		case ComponentID::CPLAYER:
			CPlayer* cPLayer = object->getComponent<CPlayer>(ComponentID::CPLAYER);
			if (cPLayer->isCameraLocked() && getServerChannelId() == cPLayer->getChannelId()) {
				cPLayer->setCamera(getCamara());
			}
		break;
	}

	//Console::print("[onComponentAddedByNetworking] blop");
}

void GameDemoClient::onComponentRemovedByNetworking(GameObject* object, ComponentID compId)
{
	//Console::print("[onComponentRemovedByNetworking] blip");
}
#pragma endregion
#endif