#pragma once

#include "GameObject.h"

#include "EventID.h"

using namespace cellularz;

struct PlayerProperties {
	Uint16 shots;
	bool recentlyFired;
};

class GameWorldObjects
{
public:
	GameWorldObjects::GameWorldObjects();

	// Player
	void addPlayer(const Uint8& channelId);
	void addPlayerShot(const Uint8& playerId);
	bool takePlayerShot(const Uint8& playerId);
	void deletePlayerProperties(const Uint8& playerId);

	// World
	std::vector<Uint32>& getCarePackages();
private:
	// Variables
	std::vector<Uint32> m_carePackages;
	std::map<Uint8, PlayerProperties*> m_playerProperties;

	// Methods
	static Uint32 allowPlayerFire(Uint32 interval, void* playerId);
};