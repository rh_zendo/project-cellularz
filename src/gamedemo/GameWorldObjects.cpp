#include "GameWorldObjects.h"

#include "GameSettings.h"

// enums
#include "EventID.h"

GameWorldObjects::GameWorldObjects()
{
	m_carePackages = std::vector<Uint32>();
	m_playerProperties = std::map<Uint8, PlayerProperties*>();
}

void GameWorldObjects::addPlayer(const Uint8& channelId)
{
	PlayerProperties* newPlayerPropterties =  new PlayerProperties();
	newPlayerPropterties->shots = PLAYER_INITIAL_SHOTS;
	newPlayerPropterties->recentlyFired = false;

	m_playerProperties.insert(std::pair<Uint8, PlayerProperties*>(channelId, newPlayerPropterties));
}

std::vector<Uint32>& GameWorldObjects::getCarePackages() { return m_carePackages; }

void GameWorldObjects::addPlayerShot(const Uint8& playerId)
{
	m_playerProperties.find(playerId)->second->shots++;
}
bool GameWorldObjects::takePlayerShot(const Uint8& playerId)
{
	PlayerProperties* playerProperties = m_playerProperties.find(playerId)->second;

	if (playerProperties->recentlyFired == true) { return false; }
	if (playerProperties->shots)
	{
		playerProperties->recentlyFired = true;
		playerProperties->shots--;
		
		SDL_AddTimer((Uint32)TIME_BETWEEN_SHOTS, allowPlayerFire, playerProperties);

		return true;
	}
	return false;
}

void GameWorldObjects::deletePlayerProperties(const Uint8& playerId)
{
	auto iterator = m_playerProperties.find(playerId);
	PlayerProperties* playerProperties = iterator->second;

	m_playerProperties.erase(iterator);
	delete playerProperties;
}

Uint32 GameWorldObjects::allowPlayerFire(Uint32 interval, void* playerPropertiesParam)
{
	PlayerProperties* playerProperties = (PlayerProperties*)playerPropertiesParam;
	playerProperties->recentlyFired = false;

	return 0;
}