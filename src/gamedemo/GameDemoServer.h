#pragma once
#ifdef _BUILD_SERVER

#include "ServerSDK.h"
#include "GameWorldObjects.h"

using namespace cellularz;

class GameDemoServer : public ServerSDK
{
public:
	GameDemoServer();
	~GameDemoServer();

	// Initialiation Hooks
	virtual void onInitializeWorld(World* world) override;

	// Hooks
	virtual void onTick(const double& deltaTime);
	virtual void onGameObjectCollision(GameObject* objectA, GameObject* objectB) override;
	virtual void onGameObjectCollisionResponse(GameObject* object, AxisID axis) override;
	virtual void onGameObjectOutOfWorld(GameObject* object) override;

	// Connnection Hooks
	virtual void onClientConnected(const Uint8& channelId, const IPaddress& clientIp) override;
	virtual void onClientDisconnect(const Uint8& channelId) override;
	virtual void onClientUnresponsive(const Uint8& channelId) override;
	virtual void onClientReconnected(const Uint8& channelId) override;
	
	// Client Hooks
	virtual void onClientInputEvent(const Uint8& channelId, const Uint16& eventId, Uint8* dataPointer) override;

private:
	GameWorldObjects* m_gameWorldObjects;
};
#endif