#pragma once

#include <SDL.h>

namespace cellularz
{
	enum ObjectClassID : Uint16
	{
		OCPLAYER = 0x0000,
		OCWALL = 0x0001,
		OCCAREPACKAGE = 0x0002,
		OCBULLET = 0x0003,

		_TOTAL_ObjectClassID
	};
}