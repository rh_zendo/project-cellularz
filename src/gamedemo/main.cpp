#include "Console.h"

#ifdef _BUILD_CLIENT
#include "GameDemoClient.h"
#endif

#ifdef _BUILD_SERVER
#include "GameDemoServer.h"
#endif

using namespace cellularz;

int main(int argc, char **argv) {
	#ifdef _BUILD_CLIENT
	GameDemoClient client;
	client.initialize();
	client.startClient();

	// Initializing console
	Console::getInstance().initialize(&client);
	#endif

	#ifdef _BUILD_SERVER
	GameDemoServer server;
	server.initialize(25025);
	server.startServer();

	// Initializing console
	Console::getInstance().initialize(&server);
	#endif
	return 0;
}