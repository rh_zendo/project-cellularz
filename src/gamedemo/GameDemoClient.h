#pragma once
#ifdef _BUILD_CLIENT

#include "ClientSDK.h"

using namespace cellularz;

class GameDemoClient : public ClientSDK
{
public:
	GameDemoClient();
	~GameDemoClient();

	// Initialiation Hooks
	virtual void onInitializeResources(AudioManager* audioManager, TextureManager* textureManager) override;
	virtual void onInitializeWorld(World* world, Config* config) override;

	// Hooks
	virtual void onTick(const double& deltaTime) override;
	virtual void onGameObjectCollision(GameObject* objectA, GameObject* objectB) override;
	virtual void onGameObjectCollisionResponse(GameObject* object, AxisID axis) override;
	virtual void onGameObjectOutOfWorld(GameObject* object) override;

	// Connection Hooks
	virtual void onServerConnect() override;
	virtual void onServerConnectChannelId(int channelId) override;
	virtual void onServerConnectFailed() override;

	// Component Hooks
	virtual void onComponentAddedByNetworking(GameObject* object, ComponentID compId) override;
	virtual void onComponentRemovedByNetworking(GameObject* object, ComponentID compId) override;

private:
	// Members
	GameObject* m_playerGameObject;
};
#endif