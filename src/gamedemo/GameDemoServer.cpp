#ifdef _BUILD_SERVER
#include <algorithm>

#include "GameDemoServer.h"
#include "time.h"
#include "GameSettings.h"
#include "GameUtils.h"
#include "GameWorldObjects.h"

// Systems
#include "SystemCollision.h"
#include "SystemPlayer.h"
#include "SystemPhysics.h"

// Components
#include "CCollision.h"
#include "CPlayer.h"
#include "CPhysics.h"
#include "CSprite.h"
#include "CTransform.h"

// Enums
#include "ObjectClassID.h"
#include "EventID.h"

#pragma region Constructor / Destructor
GameDemoServer::GameDemoServer()
{}

GameDemoServer::~GameDemoServer()
{
	delete m_gameWorldObjects;
}
#pragma endregion

#pragma region Initialiation Hooks
void GameDemoServer::onInitializeWorld(World* world)
{
	srand(time(NULL));

	world->setSize(WORLD_SIZE_X, WORLD_SIZE_Y);
	world->addSystem<SystemCollision>();
	world->addSystem<SystemPlayer>();
	world->addSystem<SystemPhysics>();

	m_gameWorldObjects = new GameWorldObjects();

	// Create world boundaries
	GameUtils::createWorldBorder(world);
}
#pragma endregion

#pragma region Hooks
void GameDemoServer::onTick(const double& deltaTime)
{
	// Create carePackages
	std::vector<Uint32>& carePackages = m_gameWorldObjects->getCarePackages();
	while (carePackages.size() < MAX_CAREPACKAGES)
	{
		int size = carePackages.size();

		float positionX = Utils::randomFloat(32.0f, getWorld()->getSize().x - 64.0f);
		float positionY = Utils::randomFloat(32.0f, getWorld()->getSize().y - 64.0f);

		GameObject* newCarePackage = GameUtils::createCarePackage(getWorld(), positionX, positionY);

		carePackages.push_back(newCarePackage->getId());
	}
}

void GameDemoServer::onGameObjectCollision(GameObject* objectA, GameObject* objectB)
{
	// Handle player picks up carePackage
	if (objectA->getClassId() == ObjectClassID::OCPLAYER && objectB->getClassId() == ObjectClassID::OCCAREPACKAGE)
	{
		// Delete carePackage
		std::vector<Uint32>& carePackages = m_gameWorldObjects->getCarePackages();
		carePackages.erase(std::remove(carePackages.begin(), carePackages.end(), objectB->getId()), carePackages.end());
		getWorld()->killGameObject(objectB->getId());

		// Grant extra bullet for player
		CPlayer* cPlayer = objectA->getComponent<CPlayer>(ComponentID::CPLAYER);
		m_gameWorldObjects->addPlayerShot(cPlayer->getChannelId());
	}
	else if (objectA->getClassId() == ObjectClassID::OCPLAYER && objectB->getClassId() == ObjectClassID::OCBULLET)
	{
		if (objectB->getOwnerId() == objectA->getId()) { return; }
		CTransform* cTransform = objectA->getComponent<CTransform>(ComponentID::CTRANSFORM);
		cTransform->setPosition(100.0f, 100.0f);

		getWorld()->killGameObject(objectB);
	}
}

void GameDemoServer::onGameObjectCollisionResponse(GameObject* object, AxisID axis)
{
	CPhysics* cPhysics = object->getComponent<CPhysics>(ComponentID::CPHYSICS);

	if (axis == AxisID::X) {
		cPhysics->setVelocity(-cPhysics->getVelocity().x, cPhysics->getVelocity().y);
	}

	if (axis == AxisID::Y) {
		cPhysics->setVelocity(cPhysics->getVelocity().x, -cPhysics->getVelocity().y);
	}
}

void GameDemoServer::onGameObjectOutOfWorld(GameObject* object)
{
	// Checks if not player component
	if (!object->hasComponent(ComponentID::CPLAYER))
		object->kill();
}
#pragma endregion

#pragma region Connnection Hooks
void GameDemoServer::onClientConnected(const Uint8& channelId, const IPaddress& clientIp)
{
	Console::print("Client has connected on Channel: " + std::to_string(channelId));

	GameUtils::createPlayerObject(getWorld(), channelId);
	m_gameWorldObjects->addPlayer(channelId);
}

void GameDemoServer::onClientDisconnect(const Uint8& channelId)
{
	Console::print("onClientDisconnect Channel: " + std::to_string(channelId));

	// Removes player GameObject
	SystemPlayer* systemPlayer = getWorld()->getSystem<SystemPlayer>(ComponentID::CPLAYER);
	m_gameWorldObjects->deletePlayerProperties(channelId);

	GameObject* object = systemPlayer->getGameObjectByChannelId(channelId);
	if (object != nullptr) {
		object->kill();
	}
}

void GameDemoServer::onClientUnresponsive(const Uint8& channelId)
{
	Console::print("onClientUnresponsive: " + std::to_string(channelId));
}

void GameDemoServer::onClientReconnected(const Uint8& channelId)
{
	Console::print("onClientReconnected: " + std::to_string(channelId));
}
#pragma endregion


#pragma region Client Hooks
void GameDemoServer::onClientInputEvent(const Uint8& channelId, const Uint16& eventId, Uint8* dataPointer)
{
	SystemPlayer* systemPlayer = getWorld()->getSystem<SystemPlayer>(ComponentID::CPLAYER);

	GameObject* object = systemPlayer->getGameObjectByChannelId(channelId);
	if (object == nullptr) { return; }

	CPhysics* cPhysics = object->getComponent<CPhysics>(ComponentID::CPHYSICS);

	switch (eventId) {
	case EventID::PLAYER_MOVE_UP:
		cPhysics->applyCenterForce(PLAYER_SPEED_INCREMENT_AMOUNT, 270, PLAYER_MAX_SPEED);
	break;

	case EventID::PLAYER_MOVE_DOWN:
		cPhysics->applyCenterForce(PLAYER_SPEED_INCREMENT_AMOUNT, 90, PLAYER_MAX_SPEED);
	break;

	case EventID::PLAYER_MOVE_LEFT:
		cPhysics->applyCenterForce(PLAYER_SPEED_INCREMENT_AMOUNT, 180, PLAYER_MAX_SPEED);
	break;

	case EventID::PLAYER_MOVE_RIGHT:
		cPhysics->applyCenterForce(PLAYER_SPEED_INCREMENT_AMOUNT, 0, PLAYER_MAX_SPEED);
	break;

	case EventID::PLAYER_MOUSE_LEFT:
		if (!m_gameWorldObjects->takePlayerShot(channelId)) { return; }

		glm::vec2 mousePosition = glm::vec2();

		int addressPtr = 0;
		std::memcpy(&mousePosition.x, dataPointer + addressPtr, sizeof(float));
		addressPtr += sizeof(float);
		std::memcpy(&mousePosition.y, dataPointer + addressPtr, sizeof(float));
		addressPtr += sizeof(float);

		GameUtils::createBullet(getWorld(), object, mousePosition);
		break;
	}
}
#pragma endregion
#endif