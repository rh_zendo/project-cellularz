#pragma once

// Player
#define PLAYER_MAX_SPEED 700.0f
#define PLAYER_SPEED_INCREMENT_AMOUNT 20.0f
#define PLAYER_INITIAL_SHOTS 0
#define TIME_BETWEEN_SHOTS 500
#define PLAYER_SIZE_X 64.0f
#define PLAYER_SIZE_Y 128.0f

// World
#define WORLD_SIZE_X 2048.0f
#define WORLD_SIZE_Y 2048.0f
#define WALL_SIZE 32.0f
#define MAX_CAREPACKAGES 10

// Bullets
#define BULLET_SIZE 10.0f
#define BULLET_SPEED 1000.0f

// Other
#define CAREPACKAGE_SIZE 32.0f