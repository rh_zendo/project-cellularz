#include <cmath>

#include "GameUtils.h"
#include "World.h"
#include "GameSettings.h"

// Components
#include "CTransform.h"
#include "CCollision.h"
#include "CSprite.h"
#include "CPhysics.h"
#include "CPlayer.h"

// Enums
#include "ObjectClassID.h"

void GameUtils::createWorldBorder(World* world)
{
	// Prepare variables
	glm::vec2 worldSize = world->getSize();
	float objectsToCreateX = worldSize.x / WALL_SIZE;
	float objectsToCreateY = worldSize.y / WALL_SIZE;

	// Create counter simulating X axis
	for (size_t i1 = 0; i1 < objectsToCreateX; i1++)
	{
		// Create counter simulating Y axis
		for (size_t i2 = 0; i2 < objectsToCreateY; i2++)
		{
			// Only create objects at edges of X or Y axis
			if (i1 == 0 ||
				i1 == objectsToCreateX - 1 ||
				i2 == 0 ||
				i2 == objectsToCreateY - 1)
			{
				GameObject* wall = world->createGameObject();
				wall->setClassId(ObjectClassID::OCWALL);

				CTransform* cTransform = wall->addComponent<CTransform>();
				cTransform->setPosition((float)i1 * WALL_SIZE, (float)i2 *WALL_SIZE);
				cTransform->setSize((float)WALL_SIZE, (float)WALL_SIZE);

				CCollision* cCollision = wall->addComponent<CCollision>();
				cCollision->enableCollisionGroup(CollisionGroupID::MAP);

				CSprite* cSprite = wall->addComponent<CSprite>();
				cSprite->setTextureId(TextureID::WALL);
			}
		}
	}
}

void GameUtils::createPlayerObject(World* world, const Uint8& channelId)
{
	GameObject* object = world->createGameObject();
	object->setOwnerId(object->getId());
	object->setClassId(ObjectClassID::OCPLAYER);

	CTransform* cTransform = object->addComponent<CTransform>();
	cTransform->setPosition((float)world->getSize().x / 2, (float)world->getSize().y / 2);
	cTransform->setSize(PLAYER_SIZE_X, PLAYER_SIZE_Y);

	object->addComponent<CPhysics>();

	CCollision* cCollision = object->addComponent<CCollision>();
	cCollision->enableCollisionGroup(CollisionGroupID::MAP);
	cCollision->enableCollisionGroup(CollisionGroupID::PLAYER);
	cCollision->enableCollisionResponseGroup(CollisionGroupID::MAP);

	#ifdef _BUILD_SERVER
	CPlayer* cPlayer = object->addComponent<CPlayer>(channelId);
	#else
	CPlayer* cPlayer = object->addComponent<CPlayer>();
	#endif
	cPlayer->enableCameraLock();

	CSprite* cSprite = object->addComponent<CSprite>();
	cSprite->setTextureId(TextureID::ROCKET);
}

GameObject* GameUtils::createCarePackage(World* world, float positionX, float positionY)
{
	// Create object and components
	GameObject* carePackage = world->createGameObject();
	carePackage->setClassId(ObjectClassID::OCCAREPACKAGE);

	CTransform* cTransform = carePackage->addComponent<CTransform>();
	cTransform->setPosition(positionX, positionY);
	cTransform->setSize(CAREPACKAGE_SIZE, CAREPACKAGE_SIZE);

	CCollision* cCollision = carePackage->addComponent<CCollision>();
	cCollision->enableCollisionGroup(CollisionGroupID::PLAYER);

	CSprite* cSprite = carePackage->addComponent<CSprite>();
	cSprite->setTextureId(TextureID::CAREPACKAGE);

	return carePackage;
}

void GameUtils::createBullet(World* world, GameObject* playerObject, glm::vec2 mouseLocation)
{
	CTransform* playerCTransform = playerObject->getComponent<CTransform>(ComponentID::CTRANSFORM);
	Uint32 playerObjectId = playerObject->getId();

	GameObject* bullet = world->createGameObject();
	bullet->setClassId(ObjectClassID::OCBULLET);
	bullet->setOwnerId(playerObjectId);

	CTransform* cTransform = bullet->addComponent<CTransform>();
	cTransform->setSize(BULLET_SIZE, BULLET_SIZE);

	// Calculate angle
	glm::vec2 triangularValues = mouseLocation - playerCTransform->getCenterPosition();
	float angle = atan2(triangularValues.y, triangularValues.x);
	float xCos = cos(angle);
	float ySin = sin(angle);

	// Set location
	glm::vec2 playerSize = (playerCTransform->getSize() / 2.0f + BULLET_SIZE);
	glm::vec2 playerPos = playerCTransform->getCenterPosition();
	cTransform->setCenterPosition((playerSize.x * xCos) + playerPos.x, (playerSize.y * ySin) + playerPos.y);

	CPhysics* cPhysics = bullet->addComponent<CPhysics>();

	// Calculate velocity
	float speed = BULLET_SPEED;
	cPhysics->setVelocity(speed * xCos, speed * ySin);

	CCollision* cCollision = bullet->addComponent<CCollision>();
	cCollision->enableCollisionGroup(CollisionGroupID::PLAYER);

	CSprite* cSprite = bullet->addComponent<CSprite>();
	cSprite->setTextureId(TextureID::BULLET);
}