set buildDir="../../buildServer"

rmdir /S /Q %buildDir%
IF not exist %buildDir% (mkdir %buildDir%)

cd %buildDir%

cmake -G "Visual Studio 14 Win64" -DBUILD_CLIENT=OFF -DBUILD_SERVER=ON -DUNITEST_DEBUG=ON ../