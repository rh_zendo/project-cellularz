set buildDir="../../buildClient"

rmdir /S /Q %buildDir%
IF not exist %buildDir% (mkdir %buildDir%)

cd %buildDir%

cmake -G "Visual Studio 14 Win64" -DBUILD_CLIENT=ON -DBUILD_SERVER=OFF -DUNITEST_DEBUG=ON ../